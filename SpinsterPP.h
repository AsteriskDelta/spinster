#ifndef SPINSTERPP_H
#define SPINSTERPP_H


//Thank you, anonymous stackoverflow user, for the foreach() macro code
#ifndef PP_NARG
#define PP_NARG(...)    PP_NARG_(__VA_ARGS__,PP_RSEQ_N())
#define PP_NARG_(...)   PP_ARG_N(__VA_ARGS__)

#define PP_ARG_N( \
        _1, _2, _3, _4, _5, _6, _7, _8, _9,_10,  \
        _11,_12,_13,_14,_15,_16,_17,_18,_19,_20, \
        _21,_22,_23,_24,_25,_26,_27,_28,_29,_30, \
        _31,_32,_33,_34,_35,_36,_37,_38,_39,_40, \
        _41,_42,_43,_44,_45,_46,_47,_48,_49,_50, \
        _51,_52,_53,_54,_55,_56,_57,_58,_59,_60, \
        _61,_62,_63,N,...) N

#define PP_RSEQ_N() \
        63,62,61,60,                   \
        59,58,57,56,55,54,53,52,51,50, \
        49,48,47,46,45,44,43,42,41,40, \
        39,38,37,36,35,34,33,32,31,30, \
        29,28,27,26,25,24,23,22,21,20, \
        19,18,17,16,15,14,13,12,11,10, \
        9,8,7,6,5,4,3,2,1,0

#define Paste(a,b) a ## b
#define XPASTE(a,b) Paste(a,b)

#define APPLYX1(FUNC, a)           FUNC(a)
#define APPLYX2(FUNC, a,b)         FUNC(a) FUNC(b)
#define APPLYX3(FUNC, a,b,c)       FUNC(a) FUNC(b) FUNC(c)
#define APPLYX4(FUNC, a,b,c,d)     FUNC(a) FUNC(b) FUNC(c) FUNC(d)
#define APPLYX5(FUNC, a,b,c,d,e)   FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e)
#define APPLYX6(FUNC, a,b,c,d,e,f) FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f)
#define APPLYX7(FUNC, a,b,c,d,e,f,g) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g)
#define APPLYX8(FUNC, a,b,c,d,e,f,g,h) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h)
#define APPLYX9(FUNC, a,b,c,d,e,f,g,h,i) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i)
#define APPLYX10(FUNC, a,b,c,d,e,f,g,h,i,j) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i) FUNC(j)
#define APPLYX11(FUNC, a,b,c,d,e,f,g,h,i,j,k) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i) FUNC(j) FUNC(k)
#define APPLYX12(FUNC, a,b,c,d,e,f,g,h,i,j,k,l) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i) FUNC(j) FUNC(k) FUNC(l)
#define APPLYX13(FUNC, a,b,c,d,e,f,g,h,i,j,k,l,m) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i) FUNC(j) FUNC(k) FUNC(l) FUNC(m)
#define APPLYX14(FUNC, a,b,c,d,e,f,g,h,i,j,k,l,m,n) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i) FUNC(j) FUNC(k) FUNC(l) FUNC(m) FUNC(n)
#define APPLYX15(FUNC, a,b,c,d,e,f,g,h,i,j,k,l,m,n,o) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i) FUNC(j) FUNC(k) FUNC(l) FUNC(m) FUNC(n) FUNC(o)
#define APPLYX16(FUNC, a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i) FUNC(j) FUNC(k) FUNC(l) FUNC(m) FUNC(n) FUNC(o) FUNC(p)
#define APPLYX_(M, ...) M(__VA_ARGS__)
#define APPLYXn(FUNC, ...) APPLYX_(XPASTE(APPLYX, PP_NARG(__VA_ARGS__)),FUNC, __VA_ARGS__)

#define APPLYPX1(FUNC,PAR, a)           FUNC(PAR,a)
#define APPLYPX2(FUNC,PAR, a,b)         FUNC(PAR,a) FUNC(PAR,b)
#define APPLYPX3(FUNC,PAR, a,b,c)       FUNC(PAR,a) FUNC(PAR,b) FUNC(PAR,c)
#define APPLYPX4(FUNC,PAR, a,b,c,d)     FUNC(PAR,a) FUNC(PAR,b) FUNC(PAR,c) FUNC(PAR,d)
#define APPLYPX5(FUNC,PAR, a,b,c,d,e)   FUNC(PAR,a) FUNC(PAR,b) FUNC(PAR,c) FUNC(PAR,d) FUNC(PAR,e)
#define APPLYPX6(FUNC,PAR, a,b,c,d,e,f) FUNC(PAR,a) FUNC(PAR,b) FUNC(PAR,c) FUNC(PAR,d) FUNC(PAR,e) FUNC(PAR,f)
#define APPLYPX7(FUNC,PAR, a,b,c,d,e,f,g) \
    FUNC(PAR,a) FUNC(PAR,b) FUNC(PAR,c) FUNC(PAR,d) FUNC(PAR,e) FUNC(PAR,f) FUNC(PAR,g)
#define APPLYPX8(FUNC,PAR, a,b,c,d,e,f,g,h) \
    FUNC(PAR,a) FUNC(PAR,b) FUNC(PAR,c) FUNC(PAR,d) FUNC(PAR,e) FUNC(PAR,f) FUNC(PAR,g) FUNC(PAR,h)
#define APPLYPX9(FUNC,PAR, a,b,c,d,e,f,g,h,i) \
    FUNC(PAR,a) FUNC(PAR,b) FUNC(PAR,c) FUNC(PAR,d) FUNC(PAR,e) FUNC(PAR,f) FUNC(PAR,g) FUNC(PAR,h) FUNC(PAR,i)
#define APPLYPX10(FUNC,PAR, a,b,c,d,e,f,g,h,i,j) \
    FUNC(PAR,a) FUNC(PAR,b) FUNC(PAR,c) FUNC(PAR,d) FUNC(PAR,e) FUNC(PAR,f) FUNC(PAR,g) FUNC(PAR,h) FUNC(PAR,i) FUNC(PAR,j)
#define APPLYPX11(FUNC,PAR, a,b,c,d,e,f,g,h,i,j,k) \
    FUNC(PAR,a) FUNC(PAR,b) FUNC(PAR,c) FUNC(PAR,d) FUNC(PAR,e) FUNC(PAR,f) FUNC(PAR,g) FUNC(PAR,h) FUNC(PAR,i) FUNC(PAR,j) FUNC(PAR,k)
#define APPLYPX12(FUNC,PAR, a,b,c,d,e,f,g,h,i,j,k,l) \
    FUNC(PAR,a) FUNC(PAR,b) FUNC(PAR,c) FUNC(PAR,d) FUNC(PAR,e) FUNC(PAR,f) FUNC(PAR,g) FUNC(PAR,h) FUNC(PAR,i) FUNC(PAR,j) FUNC(PAR,k) FUNC(PAR,l)
#define APPLYPX13(FUNC,PAR, a,b,c,d,e,f,g,h,i,j,k,l,m) \
    FUNC(PAR,a) FUNC(PAR,b) FUNC(PAR,c) FUNC(PAR,d) FUNC(PAR,e) FUNC(PAR,f) FUNC(PAR,g) FUNC(PAR,h) FUNC(PAR,i) FUNC(PAR,j) FUNC(PAR,k) FUNC(PAR,l) FUNC(PAR,m)
#define APPLYPX14(FUNC,PAR, a,b,c,d,e,f,g,h,i,j,k,l,m,n) \
    FUNC(PAR,a) FUNC(PAR,b) FUNC(PAR,c) FUNC(PAR,d) FUNC(PAR,e) FUNC(PAR,f) FUNC(PAR,g) FUNC(PAR,h) FUNC(PAR,i) FUNC(PAR,j) FUNC(PAR,k) FUNC(PAR,l) FUNC(PAR,m) FUNC(PAR,n)
#define APPLYPX15(FUNC,PAR, a,b,c,d,e,f,g,h,i,j,k,l,m,n,o) \
    FUNC(PAR,a) FUNC(PAR,b) FUNC(PAR,c) FUNC(PAR,d) FUNC(PAR,e) FUNC(PAR,f) FUNC(PAR,g) FUNC(PAR,h) FUNC(PAR,i) FUNC(PAR,j) FUNC(PAR,k) FUNC(PAR,l) FUNC(PAR,m) FUNC(PAR,n) FUNC(PAR,o)
#define APPLYPX16(FUNC,PAR, a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p) \
    FUNC(PAR,a) FUNC(PAR,b) FUNC(PAR,c) FUNC(PAR,d) FUNC(PAR,e) FUNC(PAR,f) FUNC(PAR,g) FUNC(PAR,h) FUNC(PAR,i) FUNC(PAR,j) FUNC(PAR,k) FUNC(PAR,l) FUNC(PAR,m) FUNC(PAR,n) FUNC(PAR,o) FUNC(PAR,p)
#define APPLYPX_(M, ...) M(__VA_ARGS__)
#define APPLYPXn(FUNC,PAR, ...) APPLYPX_(XPASTE(APPLYPX, PP_NARG(__VA_ARGS__)),FUNC,PAR, __VA_ARGS__)
#endif

#ifndef _unused
#define _unused_sym(x) ((void) (x) );
#define _unused(...) APPLYXn(_unused_sym, __VA_ARGS__)
#endif
    
#ifndef INTRIN_DEF
#define INTRIN_DEF
#define MK_BUILTIN(FN) \
    inline uint8_t _##FN (const unsigned int& v) {\
        return static_cast<uint8_t>(__builtin_##FN (v));\
    }\
    inline uint8_t _##FN (const unsigned long long& v) {\
        return static_cast<uint8_t>(__builtin_##FN##ll (v));\
    }

MK_BUILTIN(clz);
MK_BUILTIN(ctz);
MK_BUILTIN(popcount);
MK_BUILTIN(parity);
MK_BUILTIN(ffs);
MK_BUILTIN(clrsb);
#endif

#ifdef __GNUC__
#define _pure __attribute__((pure))
#define _unroll __attribute__((optimize("unroll-loops"))) 
#define _tls __thread
#define _likely(x) (__builtin_expect(!!(x), 1))
#define _unlikely(x) (__builtin_expect(!!(x), 0))

#define _allocAfter(x) __attribute__ ((init_priority (x)))
#else
#define pure [[pure]]
#endif

#define _lazy _allocAfter(9999)
#define _late _allocAfter(1500)
#define _early _allocAfter(500)
#define _dalloc(x) _allocAfter(x)
#define _dalloc_pre() _allocAfter(900)
#define _dalloc_body() _allocAfter(1000)
#define _dalloc_post() _allocAfter(1100)
#define _dalloc_early() _allocAfter(500)
#define _dalloc_req() _allocAfter(150)
#define _dalloc_prereq() _allocAfter(101)

#define QUOTE_HELPER(x) #x
#define QUOTE(x) QUOTE_HELPER(x)
#define LITERAL(x) x

#ifndef _instantiator
#define _instantiator weak
#endif 

#ifndef _mutate
#define _mutate(FN, ARGS, VARS) \
inline auto FN ARGS {\
    const auto *const cthis = &const_cast<const decltype(*this)&>(*this);\
    using RetType = typename std::remove_const<typename std::remove_reference<decltype(cthis->FN VARS)>::type>::type;\
    return const_cast<RetType>(cthis->FN VARS);\
}
#define _rmutate(FN, ARGS, VARS) \
inline auto FN ARGS {\
    const auto *const cthis = &const_cast<const decltype(*this)&>(*this);\
    using RetType = typename std::remove_const<typename std::remove_reference<decltype(cthis->FN VARS)>::type>::type;\
    /*std::cout << decltype(const_cast<RetType&>(cthis->FN VARS))::blah << "\n";*/\
    /*std::cout << "RMT " << __PRETTY_FUNCTION__ << "\n";*/\
    return const_cast<RetType&>(cthis->FN VARS);\
}
#endif

#ifndef _alias
#define _alias_attr(FROM, TO, ATTR) \
template<typename ...Args>\
inline auto TO(Args&... args) ATTR { return FROM(args...); };
#define _alias(FROM, TO) _alias_attr(FROM, TO,)
#define _alias_tpl_attr(FROM, TO, ATTR, ...) template<typename ...Args>\
inline TO(Args&...) ATTR { return FROM<__VA_ARGS__>(args...); };
#define _alias_tpl(FROM,TO,...) _alias_tpl_attr(FROM,TO,,__VA_ARGS__)
#endif

template <char... letters>
struct string_t {
    static constexpr const std::size_t size = sizeof...(letters) / sizeof(char);
    static constexpr const char str[size] = {letters...};
    inline static constexpr const char* c_str() {
        return str;
    }
};
template<char ...Args>
constexpr const char string_t<Args...>::str[string_t<Args...>::size];

namespace detail {
template <typename Str,unsigned int N,char... Chars>
struct make_string_t : make_string_t<Str,N-1,Str().chars[N-1],Chars...> {};

template <typename Str,char... Chars>
struct make_string_t<Str,0,Chars...> { typedef string_t<Chars...> type; };
};

#define MACRO_GET_1(str, i) \
(sizeof(str) > (i) ? str[(i)] : 0)

#define MACRO_GET_4(str, i) \
MACRO_GET_1(str, i+0),  \
MACRO_GET_1(str, i+1),  \
MACRO_GET_1(str, i+2),  \
MACRO_GET_1(str, i+3)

#define MACRO_GET_16(str, i) \
MACRO_GET_4(str, i+0),   \
MACRO_GET_4(str, i+4),   \
MACRO_GET_4(str, i+8),   \
MACRO_GET_4(str, i+12)

#define MACRO_GET_32(str, i) \
MACRO_GET_16(str, i+0),  \
MACRO_GET_16(str, i+16)

#define MACRO_GET_64(str, i) \
MACRO_GET_16(str, i+0),  \
MACRO_GET_16(str, i+16), \
MACRO_GET_16(str, i+32), \
MACRO_GET_16(str, i+48)

//CT_STR means Compile-Time_String
#define CT_STR_FORWARD(str) string_t<MACRO_GET_32(str, 0), 0 >
#define CT_STR(str) CT_STR_FORWARD(str)

//#define FOLDXn(FUNC,...) FOLDXn()
/*
#define CONCAT_HELPER(x) x
#define CONCAT2_HELPER(a,b) a ## b
#define CONCAT(...) FOLDXn(CONCAT_HELPER, __VA_ARGS__);
#define CONCAT2(a,b) CONCAT2_HELPER(a,b)
#define RCONCAT2(a,b) CONCAT2_HELPER(b,a)

#define PREPEND(FIX,...) APPLYPXn(CONCAT2, FIX, __VA_ARGS__);
#define APPEND(FIX,...) APPLYPXn(RCONCAT2, PREFIX, __VA_ARGS__);
*/

//#define CONCAT_HELPER(x) x
#define CONCAT(a,b) a ## b
#endif /* SPINSTERPP_H */

