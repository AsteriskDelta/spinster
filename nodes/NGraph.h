#ifndef NGRAPH_H
#define NGRAPH_H
#include "SpinsterInclude.h"
#include <unordered_map>
#include <unordered_set>
#include "MemberLink.h"
#include "MemberID.h"

namespace Spinster {
    namespace Obj {
        class Group;
        class Link;

        class NGraph {
        public:
            NGraph();
            virtual ~NGraph();
            
            struct Node {
                Group *group = nullptr;
                std::string lastName = "", lastType = "";
            };
            struct Groups : public std::unordered_set<MemberID> {
                // ids;
            };
            
            //force: create if it doesn't exist
            SafeRef<Group> get(const MemberID& id, const bool& force = false);
            Node* node(const MemberID& id);
            inline SafeRef<Group> operator[](const MemberID& id) {
                return this->get(id);
            }
            
            bool has(const MemberID& id);
            bool fetch(const MemberID& id);//prefetch the given id, returns true if we already know it, other false indicates fetching
            inline bool hasLink(SafeRef<MemberLink> link) const {
                return byLink.find(link) != byLink.end();
            }
            
            //lazy unlink- remove when all references are properly deleted
            bool unlink(const MemberID& id);
            
            //Add the passed group, true on success
            bool add(SafeRef<Group> grp);
            bool addLink(SafeRef<MemberLink> link);
            bool removeLink(SafeRef<MemberLink> link);
            //We know they exist, but don't particularly care for the details (yet)
            SafeRef<Group> ref(const MemberID& id);
            
            //The group's (info) changed, update our index.
            bool changed(const MemberID &id);
            
            const MemberID& getByName(const std::string& name) const;
            const Groups& getByType(const std::string& type) const;
        private:
            std::unordered_map<MemberID, Node> byID;
            std::unordered_map<std::string, MemberID> byName;
            std::unordered_map<std::string, Groups> byType;
            std::unordered_set<SafeRef<MemberLink>> byLink;
            bool destroyed;
        };
    }
}

#endif /* NGRAPH_H */

