#include "MemberInfo.h"
#include <sstream>
#include "Routing.h"
#include "Member.h"

namespace Spinster {
    MemberInfo::MemberInfo() {
        
    }
    MemberInfo::MemberInfo(Obj::Member *memb) : changed(memb->id()) {
        //MemberID idd = std::get<0>(changed.data);//changed.value();
        changed.on([](decltype(changed)* ev) -> void {Routing::Changed(ev->value());});
    }

    MemberInfo::MemberInfo(const MemberInfo& orig) :type(orig.type),  name(orig.name), changed(orig.changed) {
    }

    MemberInfo::~MemberInfo() {
        changed.disable();
    }

    void MemberInfo::setParentID(const MemberID& newID) {
        changed.value() = newID;
    }
    
    std::string MemberInfo::str() const {
        std::stringstream ss;
        ss << "Info(" << name << " creds=" << type << ")";
        return ss.str();
    }

};