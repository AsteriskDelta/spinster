#include "GroupCredentials.h"

namespace Spinster {

    GroupCredentials::GroupCredentials() {
    }
    GroupCredentials::GroupCredentials(const std::string& nam) : name(nam) {
        
    }

    GroupCredentials::GroupCredentials(const GroupCredentials& orig) : name(orig.name) {
    }

    GroupCredentials::~GroupCredentials() {
    }

        
        std::string GroupCredentials::str() const {
            return "Credentials("+name+")";
        }
};