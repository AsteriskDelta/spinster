#ifndef MEMBERID_H
#define MEMBERID_H
#include "SpinsterInclude.h"
#include <utility>

namespace Spinster {

    class MemberID : public Common, public MCopyable {
    public:

        inline constexpr MemberID() : group(0), member(0) {
        };

        inline constexpr MemberID(const MemberID& orig) : group(orig.group), member(orig.member) {
        };

        inline constexpr MemberID(unsigned int g, unsigned int m) : group(g), member(m) {
        };

        enum Globals {
            LocalGroup = 1, LocalMember = 0,
            ProtoGroup = 1, ProtoMember = 13,
            MainThread = 0
        };

        unsigned int group;
        unsigned int member;
        
        inline bool operator==(const MemberID& o) const {
            return raw() == o.raw();
        }
        inline bool operator!=(const MemberID& o) const {
            return raw() != o.raw();
        }
        
        inline bool valid() const {
            return group != 0 && member != 0;
        }
        inline explicit operator bool() const {
            return this->valid();
        }

        bool isLocal() const;
        SafeRef<Obj::Group> resolve() const;

        std::string str() const;

        inline const uint64_t& raw() const {
            return *reinterpret_cast<const uint64_t*> (&group);
        }

        inline static MemberID Local() {
            return MemberID(LocalGroup, LocalMember);
        }
        inline static MemberID Any() {
            return MemberID();
        }
    private:

    };
};

namespace std {

    template<>
    struct hash<Spinster::MemberID> {

        std::size_t operator()(const Spinster::MemberID& rr) const noexcept {
            return std::hash<uint64_t>{}(rr.raw());
        }
    };

}

#endif /* MEMBERID_H */

