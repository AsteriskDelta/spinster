#include "NGraph.h"
#include "Group.h"

namespace Spinster {
    namespace Obj {

        NGraph::NGraph() : byID(), byLink(), destroyed(false) {
        }

        NGraph::~NGraph() {
            this->destroyed = true;
            for(auto it = byID.begin(); it != byID.end(); ++it) {
                if(it->second.group != nullptr && !it->second.group->isLocal()) delete it->second.group;
            }
        }

        SafeRef<Group> NGraph::get(const MemberID& id, const bool& force) {
            //std::cout << "ngraph get " << id << "\n";
            auto it = byID.find(id);
            if(it == byID.end()) {
                if(force) {
                    return this->ref(id);
                } else return nullptr;
            }
            
            //std::cout << "found, return " << &it->second.group << "\n";
            return it->second.group;
        }
        NGraph::Node* NGraph::node(const MemberID& id) {
            //std::cout << "ngraph get " << id << "\n";
            auto it = byID.find(id);
            if(it == byID.end()) return nullptr;
            else return &(it->second);
        }

        bool NGraph::has(const MemberID& id) {
            auto it = byID.find(id);
            if(it == byID.end()) return false;
            else return true;
        }

        bool NGraph::fetch(const MemberID& id) { //prefetch the given id, returns true if we already know it, other false indicates fetching
            return bool(this->ref(id));
        }

        //lazy unlink- remove when all references are properly deleted
        bool NGraph::unlink(const MemberID& id) {
            if(this->destroyed) return true;
            auto it = byID.find(id);
            if(it == byID.end()) return false;
            
            const auto groupPtr = it->second.group;
            byID.erase(it);
            delete groupPtr;
            return true;
        }

        //Add the passed group, true on success
        bool NGraph::add(SafeRef<Group> grp) {
            auto it = byID.find(grp->id());
            if(it != byID.end() && it->second.group != &grp) delete it->second.group;
            byID.insert({grp->id(), Node{&grp}});
            return true;
        }
        
        bool NGraph::addLink(SafeRef<MemberLink> link) {
            byLink.insert(link);
            return true;
        }
        bool NGraph::removeLink(SafeRef<MemberLink> link) {
            auto it = byLink.find(link);
            if(it == byLink.end()) return false;
            
            byLink.erase(link);
            //link->destroy();
            return true;
        }
        
        bool NGraph::changed(const MemberID &id) {
            Node *cNode = this->node(id);
            if(!cNode) {
                std::cerr << "NGraph::changed on nonexistant group #" << id << "\n";
                return false;
            }
            
            std::cout << "GRAPH #" << id << " changed to name=" << cNode->group->info.name << "\n";
            
            if(cNode->lastName != cNode->group->info.name) {
                if(cNode->lastName.size() > 0) byName.erase(cNode->lastName);
                const std::string& newName = cNode->group->info.name;
                if(!newName.empty()) byName[newName] = id;
                cNode->lastName = newName;
            }
            
            if(cNode->lastType != cNode->group->info.type) {
                if(cNode->lastType.size() > 0) byType[cNode->lastType].erase(id);
                const std::string& newType = cNode->group->info.type;
                if(!newType.empty()) byType[newType].insert(id);
                cNode->lastType = newType;
            }
            
            return true;
        }

        //We know they exist, but don't particularly care for the details (yet)
        SafeRef<Group> NGraph::ref(const MemberID& id) {
            SafeRef<Group> fakeGroup = new Group();
            fakeGroup->setID(id);
            std::cout << "Added group ID " << id << "\n";
            if(this->add(fakeGroup)) return this->get(id);
            else return nullptr;
        }
        
        const MemberID& NGraph::getByName(const std::string& name) const {
            const static MemberID badID;
            auto it = byName.find(name);
            if(it == byName.end()) return badID;
            else return it->second;
        }
        const NGraph::Groups& NGraph::getByType(const std::string& type) const {
            static const Groups badGroups;
            auto it = byType.find(type);
            if(it == byType.end()) return badGroups;
            else return it->second;
        }
    }
}


