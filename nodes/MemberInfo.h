#ifndef MEMBERINFO_H
#define MEMBERINFO_H
#include "SpinsterInclude.h"
#include "SynObject.h"
#include "MemberID.h"
#include "Event.h"

namespace Spinster {
    
    namespace Obj {
        class Group;
        class Member;
    }

    class MemberInfo : public Common, public SynObject {
    public:
        MemberInfo();
        MemberInfo(Obj::Member *member);
        MemberInfo(const MemberInfo& orig);
        virtual ~MemberInfo();
        
        _shared();
        
        //MemberID id;
        std::string type, name;
        
        void setParentID(const MemberID& newID);
        
        std::string str() const;
        inline operator std::string() const {
            return this->str();
        }
        
        Event<MemberID> changed;
    private:

    };
    
    _share(MemberInfo,
            type, name);

};

#endif /* MEMBERINFO_H */

