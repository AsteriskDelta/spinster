#ifndef LOCALGROUP_H
#define LOCALGROUP_H
#include "SpinsterInclude.h"
#include "Member.h"
#include "SafeRef.h"
#include "Group.h"

namespace Spinster {

    namespace Obj {
        
        class Thread;
        class LocalScheduler;

        class LocalGroup : public Group {
        public:
            LocalGroup();
            virtual ~LocalGroup();
            
            void initialize();
            virtual bool assign(SafeRef<Task> task);
            
            using Group::thread;
            virtual  Thread* thread();
        private:
            LocalScheduler *precastedSchd;
        };
    }

    class LocalGroup : public SafeRef<Obj::LocalGroup> {
    public:
        typedef SafeRef<Obj::LocalGroup> Parent;

        inline LocalGroup() : Parent() {
        };
        using Parent::Parent;
    };
    SPINSTER_SAFEREF_NULL(LocalGroup);
};

#endif /* LOCALGROUP_H */

