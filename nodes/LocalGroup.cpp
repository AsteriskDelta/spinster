#include "LocalGroup.h"
#include "Thread.h"
#include "LocalScheduler.h"
#include "Spinster.h"
#include "MainThread.h"

namespace Spinster {

    namespace Obj {
        LocalGroup::LocalGroup() : Group() {
            
        }

        LocalGroup::~LocalGroup() {
            //Scheduler deleted by ~Group()
        }
        
        void LocalGroup::initialize() {
            this->info.type = "Local";
            precastedSchd = new LocalScheduler(this);
            this->scheduler = precastedSchd;
            Group::initialize();
            
            //Register main()
            member = new $::Obj::MainThread();
            
            //Thread::Idt currentThreadCount = 4;
            //this->scheduler->addThreads(currentThreadCount);
            /*for(Thread::Idt i = 0; i < currentThreadCount; i++) {
                this->addMember($::Member(static_cast<Obj::Member*>(new Thread())));
            }*/
        }
        
        bool LocalGroup::assign(SafeRef<Task> task) {
            if(!scheduler) {
                std::cerr << "No sched\n";
                return false;
            }
            
            return scheduler->schedule(task);
        }
        /*
        void LocalGroup::memberIsIdle($::Member member) {
            DBG_EXCL(std::cerr << "Group idle on " << member << "\n");
        }*/
        
        Thread* LocalGroup::thread() {
            if(!precastedSchd) return nullptr;
            
            return precastedSchd->createThread();
        }
    };
};
