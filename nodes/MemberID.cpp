#include "MemberID.h"
#include "Spinster.h"

namespace Spinster {

    std::string MemberID::str() const {
        return std::to_string(group) + ":" + std::to_string(member);
    }

    bool MemberID::isLocal() const {
        return group == LocalGroup || Spinster::Local->id() == *this;
    }
    
    SafeRef<Obj::Group> MemberID::resolve() const {
        return Spinster::GetGroup(*this);
    }

};