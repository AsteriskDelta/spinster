#define IS_IMPL
#include "Group.h"
#include <iostream>
#include "MemberLink.h"
#include "Routing.h"

namespace Spinster {

    namespace Obj {

        Group::Group() : Member(), proxyID() {
        }
        
        Group::Group(const GroupCredentials& creds) : Member(), proxyID() {
            std::cout << "creating group " << creds.name << "\n";
            this->setID(MemberID(rand(), rand()));
            this->info.name = creds.name;
            //this->info.setParentID(this->raw.id);
            Routing::Add(this);
            this->info.changed();
        }

        Group::~Group() {
            for(auto it = memberOf.begin(); it != memberOf.end(); ) {
                auto nextIt = std::next(it);
                SafeRef<Group> rGroup = Routing::Get(*it);
                if(rGroup) {
                    rGroup->removeMember(this);
                }
                it = nextIt;
            }
            
            if (this->owned()) delete &scheduler;
            if (this->symbolTable) delete &symbolTable;
            
            Routing::Remove(this->id());
        }

        unsigned int Group::size() const {
            return members.size();
        }

        Group::iterator Group::begin() {
            return members.begin();
        }

        Group::iterator Group::end() {
            return members.end();
        }

        void Group::initialize() {

        }

        SafeRef<Obj::MemberLink> Group::linkTo(const MemberID& id) {
            for(const auto& link : links) {
                if(link->other(this)->id() == id) return link;
            }
            return nullptr;
        }

        bool Group::addLink(SafeRef<Obj::MemberLink> link) {
            links.push_back(link);
            link->added();
            return true;
        }

        bool Group::removeLink(SafeRef<Obj::MemberLink> link) {
            for(auto it = links.begin(); it != links.end(); ++it) {
                if(it->obj == &link) {
                    links.erase(it);
                    link->removed();
                    
                    if(!this->isLocal() && links.empty()) {
                        //We're orphaned, mark for deletion
                        Routing::Remove(this);
                    }
                    
                    return true;
                };
            }
            return false;
        }

        void Group::addMember(SafeRef<Group> member) {
            if (!this->owned()) {
                std::cerr << "addmember on non-owned group\n";
                return;
            }

            member->memberOf.insert(this->id());
            members.insert(member);
        }
        void Group::removeMember(SafeRef<Group> member) {
            if (!this->owned()) {
                std::cerr << "removemember on non-owned group\n";
                return;
            }

            member->memberOf.erase(this->id());
            members.erase(member);
        }

        void Group::updateMemberStatus($::Member member, const MemberStatus& status, const MemberStatus &oldStatus) {
            // DBG_EXCL(std::cerr << "Group idle on " << member << "\n");
            if (this->owned() && scheduler) scheduler->updateMemberStatus(member, status, oldStatus);
        }

        /*void Group::memberIsIdle($::Member member) {
           // DBG_EXCL(std::cerr << "Group idle on " << member << "\n");
            //if(this->owned() && scheduler) scheduler->addIdle(member);
        }*/

        bool Group::assign(SafeRef<Task> task) {
            if (!this->owned()) {
                //std::cout << "assign on non-owned\n";
                task->targetID() = this->id();
                
                auto link = $::Routing::LinkTo(this->id());
                //std::cout << "remote assign got " << &link << ", bool=" << bool(link) << "\n";
                if(!link) return false;
                
                /*auto *msg = task->constructMessage();
                
                //std::cout << "remote msg = " << msg << "\n";
                if(msg == nullptr) return false;
                
                //std::cout << "sending...\n";
                return link->send(msg);*/
                return task->sendMessage(link);
            } else {

                return this->scheduler->schedule(task);
            }
        }

        std::string Group::str() const {
            return "Group(creds=" + credentials.str() + ", size=" + std::to_string(this->size()) + ", " + Member::str() + ")";
        }

        void Group::constructSymbolTable() {
            //std::cout << "constructing symbol table\n";
            symbolTable = new Obj::SymbolTable(this);
        }

        SafeRef<Obj::TaskGroup> Group::defaultTaskGroup() {
            if (this->owned()) return scheduler->defaultTaskGroup();

            return nullptr;
        }

        SafeRef<Obj::TaskGroup> Group::reservedTaskGroup() {
            if (this->owned()) return scheduler->reservedTaskGroup();

            return nullptr;
        }

        SafeRef<Obj::TaskGroup> Group::eventTaskGroup() {
            if (this->owned()) return scheduler->eventTaskGroup();

            return nullptr;
        }
        
        SafeRef<Obj::Group> Group::get(const MemberID& id) {
            for(auto& member : members) {
                if(member->id() == id) return member;
            }
            return nullptr;
        }
    };
    /*
    */

};
