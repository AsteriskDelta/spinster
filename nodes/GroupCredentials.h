#ifndef GROUPCREDENTIALS_H
#define GROUPCREDENTIALS_H
#include "SpinsterInclude.h"
#include "SynObject.h"

namespace Spinster {

    class GroupCredentials : public SynObject, public Common {
    public:
        GroupCredentials();
        GroupCredentials(const std::string& nam);
        GroupCredentials(const GroupCredentials& orig);
        virtual ~GroupCredentials();
        
        std::string name;
        
        std::string str() const;
        
        _shared();
    private:

    };
    
    _share(GroupCredentials, 
            name);
};

#endif /* GROUPCREDENTIALS_H */

