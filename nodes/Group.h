#ifndef GROUP_H
#define GROUP_H
#include "SpinsterInclude.h"
#include "Member.h"
#include "SafeRef.h"
#include "GroupCredentials.h"
#include "Scheduler.h"
#include "SymbolTable.h"
#include <vector>

#include "Invoker.h"
#include "SynObject.h"
#include <memory>
#include <unordered_set>

/*
template<typename T, typename U> inline constexpr std::size_t moffsetof(U T::*member) {
            return (char*)&((T*)nullptr->*member) - (char*)nullptr;
        }

template <typename T1, typename T2>
inline size_t constexpr offset_of(T1 T2::*member) {
    return reinterpret_cast<size_t>(member);//moffsetof(member);
    //constexpr T2* object = 0x0;;
    //return size_t(&(object->*member)) - size_t(object);
}*/


namespace Spinster {

    namespace Obj {
        class Group;
        class Link;
        class MemberLink;

        class Group : public Member {
        public:
            
            //_synit();
            inline auto _syn_this() const -> decltype(this) {
                return this;
            }
            typedef std::result_of<typename std::remove_pointer<decltype(&_syn_this)>::type> Self;
            
            Group();
            Group(const GroupCredentials& creds);
            virtual ~Group();
            
            void initialize();//Called by owner

            std::unordered_set<SafeRef<Group>> members;
            std::vector<SafeRef<MemberLink>> links;
            
            typedef decltype(members)::iterator iterator;
            $::GroupCredentials credentials;
            $::Scheduler scheduler;
            $::SymbolTable symbolTable;
            //static constexpr std::size_t __attribute__((used)) mof = offsetof(Group, members);//moffsetof(&Group::members);//std::declval(moffsetof(members));
            //template<typename S = Self>
            //friend class ::Spinster::SynObjectProxy;
            
            //static constexpr const std::size_t off = __builtin_offsetof(Self, members);
            //static ::Spinster::SynObjectProxy<Self, decltype(members), offset_of(&((Self*)->member))>* __attribute__((used)) ssy;
            //_syn_sym(members);
            
            //_syn(members);
            //_syn(members, symbolTable);
            
            inline $::SymbolTable symbols() {
                if(!symbolTable) constructSymbolTable();
                return symbolTable;
            }

            unsigned int size() const;
            
            inline bool owned() const {
                return bool(scheduler);
            }
            
            inline $::Member asMember() {
                return $::Member(this);
            }
            inline SafeRef<Group> asGroup() {
                return this;
            }
            
            void addMember(SafeRef<Group> member);
            void removeMember(SafeRef<Group> member);
            virtual bool assign(SafeRef<Task> task);
            
            SafeRef<Obj::Group> get(const MemberID& id);
            inline bool contains(const MemberID& id) {
                return bool(this->get(id));
            }
            
            SafeRef<Obj::MemberLink> linkTo(const MemberID& id);
            inline bool connects(const MemberID& id) {
                return bool(this->linkTo(id));
            }
            bool addLink(SafeRef<Obj::MemberLink> link);
            bool removeLink(SafeRef<Obj::MemberLink> link);
            
            virtual SafeRef<Obj::TaskGroup> defaultTaskGroup() override;
            virtual SafeRef<Obj::TaskGroup> reservedTaskGroup() override;
            virtual SafeRef<Obj::TaskGroup> eventTaskGroup() override;
            
            virtual void updateMemberStatus($::Member member, const MemberStatus& status, const MemberStatus& oldStatus);
            //virtual void memberIsIdle($::Member member);

            /*struct iterator {
                Group *const parent;
                inline iterator(MultiLog * const par) : parent(par) {
                };
                inline iterator(const iterator& o) : parent(o.parent) {
                };

                inline iterator& operator++() {
                    
                };

                inline LogPrototype::iterator& operator*() {
                    return *this->current();
                };

                inline bool operator!=(const iterator& o) const {
                    return 
                };
            };*/
            iterator begin();
            iterator end();
            
            std::string str() const;
            
            MemberID proxyID;
        protected:
            void constructSymbolTable();
            
            std::unordered_set<MemberID> memberOf;
        };
        //_share_sym_ll(Group, members);
        _share(Group, credentials);
    }

 //template<>
        /*struct Reflect<Obj::Group, offset_of(&Obj::Group::members)> {
            static constexpr char str[] = "members";
        };*/
        
        //constexpr char Reflect<Obj::Group, offset_of(&Obj::Group::members)>::str[sizeof(Reflect<Obj::Group, offset_of(&Obj::Group::members)>::str)];
        //template struct Reflect<Obj::Group, offset_of(&Obj::Group::members)>;
        //constexpr MemberMeta<Obj::Group, offset_of(&Obj::Group::members)> reflect_meta = MemberMeta{"_spn"};
        //constexpr auto reflect<Obj::Group, offset_of(&Obj::Group::members)>::reflect_meta = MemberMeta<Obj::Group, offset_of(&Obj::Group::members)>{"_spn"};
    //template class ::Spinster::SynObjectProxy<Obj::Group, decltype(Obj::Group::members), offset_of(&Obj::Group::members), CT_STR("member")>;// _spn __attribute__((used));
        /*
    class Group : public SafeRef<Obj::Group> {
    public:
        typedef SafeRef<Obj::Group> Parent;

        inline Group() : Parent() {
        };
        Group(const GroupCredentials& creds);
        using Parent::Parent;
        inline Group(SafeRef<Obj::Group>& grp) : Parent(grp) {
            
        }
        
        inline operator Parent&() {
            return *reinterpret_cast<Parent*>(this);
        }
    };
    SPINSTER_SAFEREF_NULL(Group);
*/
    typedef SafeRef<Obj::Group> Group;
};

#endif /* GROUP_H */

