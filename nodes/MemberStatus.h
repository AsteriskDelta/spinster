#ifndef MEMBERSTATUS_H
#define MEMBERSTATUS_H
#include "SpinsterInclude.h"

namespace Spinster {

    class MemberStatus : public Common, public MCopyable {
    public:
        enum Code {
            Invalid = 0,
            Waiting,
            Executing,
            Disconnected,
            Connecting,
            Dispatching,
            Yielded,
            Sleeping,
            WakingUp,
            SpecialExecuting,
            MainThread,
            Reserved
        };
        
        inline constexpr MemberStatus() : code(Invalid) {};
        inline constexpr MemberStatus(const MemberStatus& orig) : code(orig.code){};
        inline constexpr MemberStatus(const Code& c) : code(c) {};
        
        Code code;
        
        std::string str() const;
    private:

    };

};

#endif /* MEMBERSTATUS_H */

