#include "MemberStatus.h"

namespace Spinster {
/*
    MemberStatus::MemberStatus() : code(Invalid) {
    }

    MemberStatus::MemberStatus(const MemberStatus& orig) : code(orig.code) {
    }
*/
    std::string MemberStatus::str() const {
        switch(code) {
            default: case Invalid: return "Invalid";
            case Waiting: return "Waiting";
            case Executing: return "Executing";
            case Disconnected: return "Disconnected";
            case Connecting: return "Connecting";
            case Dispatching: return "Dispatching";
            case Yielded: return "Yielded";
            case Sleeping: return "Sleeping";
            case WakingUp: return "WakingUp";
            case SpecialExecuting: return "SpecialExec";
            case MainThread: return "[MainThread]";
            case Reserved: return "[Reserved]";
        }
    }

};