#include "Member.h"
#include "Task.h"
#include "Group.h"
#include "TaskGroup.h"
#include "LocalGroup.h"

namespace Spinster {
    namespace Obj {

        Member::Member() : Invoker(), info(this) {
        }

        Member::Member(const Member& orig) : Invoker(orig), info(orig.info) {
        }

        Member::~Member() {
        }

        const $::MemberID& Member::id() const {
            return raw.id;
        }
        void Member::setID(const $::MemberID& newID) {
            raw.id = newID;
            info.setParentID(raw.id);
        }

        $::MemberStatus Member::status() const {
            return raw.status;
        }
        void Member::setStatus(const $::MemberStatus& stat) {
            MemberStatus oldStatus = raw.status;
            raw.status = stat;
            //if(/*stat.code == $::MemberStatus::Waiting || */stat.code == $::MemberStatus::Sleeping) this->setTask(nullptr);
            if(parent()) parent()->updateMemberStatus(this, stat, oldStatus);
        }
        void Member::setActive(bool st) {
            DBG_EXCL(std::cerr<<"Member setactive\n");
        }

        SafeRef<Obj::Task> Member::task() {
            DBG_EXCL(std::cerr << "member task\n");
            return SafeRef<Obj::Task>();
        }
        bool Member::cancelTask(SafeRef<Obj::Task> specific) {
            SafeRef<Obj::Task> tsk = this->task();
            if(!tsk) return false;
            else if(specific && tsk != specific) return false;
            
            return tsk->cancel(true);
        }

        SafeRef<Obj::LocalGroup> Member::local() {
            return SafeRef<Obj::LocalGroup>();
        }
        SafeRef<Obj::Group> Member::parent() {
            return SafeRef<Obj::Group>();
        }
        
        /*bool assign(SafeRef<Task> task) {
            DBG_EXCL(std::cerr << "Member assign " << task << "\n");
            return false;
        }*/
        bool Member::getTask() {
            return false;
        }
        bool Member::getTaskBlock() {
            return false;
        }
        void Member::setTask(SafeRef<Task> newTask) {
            if(!newTask) return;
            DBG_EXCL(std::cerr << "Member setTask on " << newTask << "\n");
        }
        
        SafeRef<Task> Member::currentTask() const {
            return SafeRef<Task>();
        }
        SafeRef<TaskGroup> Member::affinity() const {
            return SafeRef<TaskGroup>();
        }
        bool Member::active() const {
            
            return true;
        }
        bool Member::busy() const {
            bool ret = false;
            const auto code = this->status().code;
            ret |= code == $::MemberStatus::Executing;
            ret |= code == $::MemberStatus::Yielded;
            ret |= code == $::MemberStatus::MainThread;
            ret |= code == $::MemberStatus::Reserved;
            return ret;
        }
        
        bool Member::assign(SafeRef<Task> task) {
            if(!task) return false;
            DBG_EXCL(std::cerr << "Member assign\n");
            return false;
        }

        std::string Member::str() const {
            return "Member(" + info.str() + ")";
        }
        /*
        void Member::setIdle(bool st) {
            //raw.idle = st == $::MemberStatus::Waiting ;
            this->status() = $::MemberStatus::Waiting;
            //if(this->parent()) this->parent()->memberIsIdle(this);
        }*/
    };

};
