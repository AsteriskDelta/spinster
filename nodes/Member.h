#ifndef MEMBER_H
#define MEMBER_H
#include "SpinsterInclude.h"
#include "MemberInfo.h"
#include "SafeRef.h"
#include "MemberID.h"
#include "MemberStatus.h"
#include "Invoker.h"
#include "SynObject.h"

namespace Spinster {
    namespace Obj {
        class Task;
        class Group;
        class LocalGroup;
        class TaskGroup;
    };
    namespace Obj {

        class Member : public $::Common, public Invoker, public $::SynObject {
        public:
            _synit();
            
            Member();
            Member(const Member& orig);
            virtual ~Member();
            
            MemberInfo info;
            
            virtual const $::MemberID& id() const override;
            virtual void setID(const $::MemberID& newID);
            
            inline operator const $::MemberID&() const {
                return this->id();
            }
            
            virtual $::MemberStatus status() const;
            virtual SafeRef<Obj::Task> task();
            virtual bool cancelTask(SafeRef<Obj::Task> specific = nullptr);
            
            virtual SafeRef<Obj::LocalGroup> local();
            virtual SafeRef<Obj::Group> parent();
            
            virtual std::string str() const;
            
            virtual SafeRef<Task> currentTask() const;
            virtual SafeRef<TaskGroup> affinity() const;
            virtual bool getTask();
            virtual bool getTaskBlock();
            virtual void setTask(SafeRef<Task> newTask);
            virtual bool active() const;
            
            virtual bool assign(SafeRef<Task> task);
            
            void setStatus(const $::MemberStatus& stat);
            virtual void setActive(bool st);
            
            //void setIdle(bool state);
            inline bool idle() const { return this->status().code == $::MemberStatus::Waiting; };
            virtual bool busy() const;
            
            inline bool isLocal() const {
                return raw.id.isLocal();
            }
            
            _shared();
            //protected:
            struct Raw : public $::MCopyable {
                $::MemberID id;
                $::MemberStatus status;
                //bool idle = true;
                //TaskGroup affinity = TaskGroup();
            } raw;
        };
        
        _share(Member, 
                info, raw);
    };

    typedef SafeRef<Obj::Member> Member;
    SPINSTER_SAFEREF_NULL(Member);
};

#endif /* MEMBER_H */

