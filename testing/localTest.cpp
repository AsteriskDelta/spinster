#include <iostream>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <list>
#include "Spinster.h"
#include "Net.h"
#include <sstream>
#include "remoteTasks.h"
#include "Forwarding.h"

double other_func(double st) {
    DBG_EXCL(std::cout << "starting other func...\n");
    sleep(1);
    DBG_EXCL(std::cout << "other func complete\n");
    return st;
}
_expose(other_func);

char long_func(int test, char** argv) {
    DBG_EXCL(std::cout << "yielding to other... v=" << double(test) << "\n");
    double retTest = Spinster::Yield(&other_func, test);
    DBG_EXCL(std::cout << "YIELD GOT" << retTest << "\n");
    unsigned int upper = rand()%5;
    for(unsigned int i = 0; i < upper; i++) {
        DBG_EXCL(std::cout << "long_func got test=" << test << ":"<<i<<" and " << argv[0] << "\n");
        usleep(500000);
    }
    return 0;
}
_expose(long_func);

int remote_delegated(std::string from, int randID) {
    DBG_EXCL(std::cout << "delegated remote task #"<<randID<<" from peer " << from << "\n");
    DBG_EXCL(std::cout << "raw sender info: " << Spin::task->sender << "\n");
    for(int i = 0; i < 10; i++) Spin::YieldFor(1);
    
    int ret = rand();
    DBG_EXCL(std::cout << "completed remote function, returning '"<<ret<<"'...\n");
    return ret;
}
_expose(remote_delegated);

int other_main_loop() {
    while(true) {
        DBG_EXCL(std::cout << "FROM OTHER LOOP\n";);
        usleep(1000*1000*4);
    }
    return 0;
}
_expose(other_main_loop);

std::string getPort(std::string prep) {
    std::stringstream ss;
    ss << prep << " to " << Spin::Net::Port();
    return ss.str();
}
_expose(getPort);

struct TestEventData {
    std::string name;
    int val;
};
typedef Spinster::Event<TestEventData> TestEvent;
_share(TestEventData, name, val);

void gotTest(TestEvent *event) {
    if(!event) {
        DBG_EXCL(std::cout << "\nBAD EVENT\n\n");
        return;
    }
    
    DBG_EXCL(std::cout << "Got event " << event->name << ", val=" << event->val << "\n");
}

static void exitMsg() {
    DBG_EXCL(std::cout << "\nATEXIT INVOKE\n\n";);
}

#include <type_traits>
int main(int argc, char** argv) {
    srand(time(nullptr));

    unsigned short expPort;
    if(argc <= 1) {
        //std::cout << "need port\n";
        //return 1;
        expPort = 1339;
    } else {
        try {
            expPort = strtoul(argv[1], nullptr, 0); 
        } catch(...) { std::cerr << "couldn't parse port from " << argv[1] << "\n"; };
    }
    
    std::list<Spinster::Address> otherAddr;
    for(int i = 2; i < argc; i++) {
        try { otherAddr.push_back("127.0.0.1:"+std::to_string((unsigned short)strtoul(argv[i], nullptr, 0))); }
        catch(...) {};
    }
    
    Spin::MemberInfo myInfo;
    myInfo.type = "simpleTest";
    
    Spin::Initialize(Spinster::MemberID(5, expPort), myInfo);
    
    Spin::TaskSet tasks;
    /*tasks += Spin::Local->invoke([](std::string tr)->int {
        DBG_EXCL(std::cout << "Local Invoke handlers, myInfo.type=" << tr << "\n");
        return 3;
    }, myInfo.type)->callback([](int rtVal) {
        DBG_EXCL(std::cout << "Local handler callback, rt=" << rtVal << std::endl << "\n");
    });*/
    DBG_EXCL(std::cout << "Begin task set\n";);
    for(int i = 0; i < 5; i++) {
        tasks += Spin::Local->invoke(long_func, argc, argv);
    }
    
    DBG_EXCL(std::cout << "Waiting for task group to complete...\n";);
    Spin::Yield(tasks);
    std::cout << "DONE!!!\n";
    //sleep(5);
    std::cout << "RET 0\n";
    sleep(1);
    std::atexit(&exitMsg);
    return 0;
    //exit(0);
    Spin::Net::Expose(expPort);
    
    //sleep(3);
    //_invoke(long_func, argc, argv);
    //for(int i = 0; i < 12; i++) _invoke(Local, long_func, argc_argv);
    
    //Spin::Link("127.0.0.1:");
    const auto mainCreds = Spin::GroupCredentials("MainGroup");
    for(auto &addr : otherAddr) {
        Spin::Connect(addr)->connected([&mainCreds](auto ev) -> void {
            if(Spin::Net::Port() != 1339) Spin::Yield(&Spin::Join(mainCreds));
            auto& link = *ev;
            link->other()->invoke(Spinster::Protocol::Sym::Ping)->callback([](bool s) -> bool {
                std::cout << "ping callback works! s=" << s << "\n";
                //exit(0);
                return true;
            });
            link->other()->invoke(other_func, double(rand())/100000.0)->returned([](double val) -> void {
                std::cout << "got remote's other func val=" << val << "\n";
            });
        });
    }
    
   
    if(expPort == 1339) Spin::Create(mainCreds);
    
    /*while(true) {
        //Spinster::Local->links[0]->update();
        //Spinster::Forward::Update();
        usleep(10000);
    }
    exit(0);*/
    
    Spin::Group mainGroup = new Spinster::Obj::Group(mainCreds);
    Spin::CompleteTrust(mainGroup);
    
    DBG_EXCL(std::cout << "SpinLocal: " << Spin::Local.obj << "\n");
    
    TestEvent event;
    event.name = "TestEvent works!!! ^_^";
    event.val = 13;
    
    event.on(std::function<decltype(gotTest)>(gotTest));
    
    //mainGroup->invoke(&TestRemote::testA, std::string("blah"), 13.131313);
    
    Spin::Local->thread(other_main_loop);
    
    //sleep(5);
    
    Spin::Local->invoke([](std::string tr)->int {
        DBG_EXCL(std::cout << "Local Invoke handlers, myInfo.type=" << tr << "\n");
        return 3;
    }, myInfo.type)->callback([](int rtVal) {
        DBG_EXCL(std::cout << "Local handler callback, rt=" << rtVal << std::endl << "\n");
    });
    
    event.emit();
    
    /*auto tfn = [=]()->auto {
        std::cout << "Invoke handlers, myInfo.type=" << myInfo.type << "\n";
    };*/
    
    /*mainGroup->invoke([](std::string tr)->int {
        std::cout << "Invoke handlers, myInfo.type=" << tr << "\n";
        return 3;
    }, myInfo.type)->callback([](int rtVal) {
        std::cout << "handler callback, rt=" << rtVal << std::endl << "\n";
    });*/
    //mainGroup->$invoke(std::cout << "\tinvokcation test, mt=" << myInfo.type << "\n", myInfo.type);
    
    /*mainGroup->changed()->on([](Spin::Member newMember) {
        std::cout << "\tMain group member changed: " << newMember->str() << "\n";
    });*/
    DBG_EXCL(std::cout << "Joined group with members="<<mainGroup->size() << "\n");
    for(Spin::Group member : mainGroup) {
        DBG_EXCL(std::cout << "\t"<<member->str()<<"\n");
    }
    DBG_EXCL(std::cout << "end member list\n");
    
    /*mainGroup->$invoke(getPort, std::string(argv[1]))->ret([](std::string ret) {
        std::cout << "Got callback as " << ret << std::endl;
    });*/
    //Spin::Local->invoke(&long_func,1,nullptr);
    DBG_EXCL(std::cout << "Scheduling local tasks...\n");
    for(int i = 0; i < 30; i++) {
        Spin::Local->schedule(Spin::Priority::Normal,&long_func,i,argv);
        Spin::Local->schedule(Spin::Priority::Normal, other_func, double(i*5)/3.0);
        if(i % 10 == 0) Spin::Local->schedule(Spin::Priority::Highest, long_func, i+100, argv);
    }
    /*
    std::cout << "Scheduling shared group tasks...\n";
    for(int i = 0; i < 10; i++) {
        int toSend = rand();
        std::cout << "\tsending out task from t=" << Spin::info->type << ", randID=" << toSend << "\n";
        mainGroup->$schedule(Spin::Priority::Normal, remote_delegated(Spin::info->type, toSend));
    }*/
    
    //(*Spin::Local.begin())->setActive(false);
    
    
    unsigned int mainLoopCnt = 0;
    while(true) {
        if(mainLoopCnt % 10 == 0) {
            DBG_EXCL(std::cout << "main loop invocation, local size = " << Spin::Local->size() << ", max size="<<Spinster::MaxActiveThreads <<", active size=" << Spinster::CurrentActiveThreads << ", busy size="<<Spinster::CurrentBusyThreads <<"\n");
            for(auto thread : Spin::Local) {
                DBG_EXCL(std::cout << "\tthread #" << thread->id() << "\t status=" << thread->status() << ", " << thread->task() << "\n");
            }
            DBG_EXCL(std::cout << "Queued tasks ["<<Spin::Local->scheduler->size()<<"]:\n");
            for(auto taskGroup : Spin::Local->scheduler->taskGroups) {
                DBG_EXCL(std::cout << "\t" << taskGroup << "\n");
                for(auto it = taskGroup->getActives()->begin(); it != taskGroup->getActives()->end(); ++it) {
                    DBG_EXCL(std::cout << "\t\t[ACTIVE] " << (*it) << "\n");
                }
                for(auto it = taskGroup->begin(); it != taskGroup->end(); ++it) {
                    DBG_EXCL(std::cout << "\t\t" << (*it) << "\n");
                }
            }
            event.val++;
            event.emit();
        }
        if(mainLoopCnt % 100 == 0) {
            DBG_EXCL(std::cout << "Getting associate's statuses...\n");
            for(Spin::Group member : mainGroup) {
                if(member->isLocal()) continue;
                DBG_EXCL(std::cout << "\t"<<member->str()<<"\n");
                for(auto thread : member->local()) {
                    DBG_EXCL(std::cout << "\t\tthread #" << thread->id() << "\t status=" << thread->status() << ", " << thread->task() << "\n");
                }
            }
        }
        
        usleep(100000);
        mainLoopCnt++;
    }
    
    return 0;
}

//#define BLAH
#ifdef BLAH
class Test {
public:
    int a, b;
    void func() {std::cout << "IT WORKS\n"; };
};

class TestRef {
public:
    Test& obj;
    TestRef(Test& ref) : obj(ref) {};
    inline Test* operator->() { return &obj; };
    inline const Test* operator->() const { return &obj; };
};

class SCT {
public:
    ~SCT() {
        std::cout << "Called!!!\n";
    }
};

SCT testFunc() {
    return SCT();
}

int main(int argc, char** argv) {
    Test val;
    val.a = 50;
    val.b = 60;
    
    TestRef ref(val);
    std::cout << "got " << ref->a << ", " << ref->b << "\n";
    std::cout << "func: "; ref->func();
    
    testFunc();
    std::cout << " after testfunc\n";
}
#endif
