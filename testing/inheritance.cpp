#include "Spinster.h"
#include <sstream>
#include <signal.h>

class ExceptSegv : public std::runtime_error {
public:
    using std::runtime_error::runtime_error;
    ExceptSegv() : std::runtime_error("Segfault") {
        
    }
};

void handle(int sig) {
    if(sig == SIGSEGV) throw ExceptSegv();
}

struct A {
    A() {
        x = 0xDEAD;
        w = 0xBEEF;
    }
    virtual ~A() {
        std::cout << "A's destructor"  << "\n";
    }
    
    int x, w;
    virtual int a() const {
        return x;
    }
};
struct O {
    int z = 0xFEEB, q = 0xDAED;
};

struct B : public virtual A, public virtual O {
    B() {
        
    }
    virtual ~B() {
        std::cout << "B's destructor\n";
    }
    
    int y;
    virtual int b() const {
        return y;
    }
    virtual std::string xy() const {
        std::stringstream ss;
        ss << this->a() << ", " << this->b() << "\n";
        return ss.str();
    }
};

int main(int argc, char** argv) {
    _unused(argc, argv);
    signal(SIGSEGV, handle);
    
    //volatile uint64_t padding[] = {0x0, 0x0, 0x0, 0x0};
    //std::cout << "Padding: " << padding[0] << "\n";
    B *obj = new B();
    A *rep = new A();;
    
    obj->x = 42;
    obj->y = 64;
    rep->x = 13;
    
    auto aptr = static_cast<A*>(obj);
    auto optr = static_cast<O*>(obj);
    
    std::cout << "obj's address: " << obj << "\n";
    std::cout << "obj's A instance: " << aptr << "\n";
    std::cout << "obj's O instance: " << optr << "\n";
    std::cout << "rep A instance: " << rep << "\n";
    
    std::cout << "\nA-B offset: " << (reinterpret_cast<uint8_t*>(aptr) - reinterpret_cast<uint8_t*>(obj)) << "\n";
    std::cout << "\nO-B offset: " << (reinterpret_cast<uint8_t*>(optr) - reinterpret_cast<uint8_t*>(obj)) << "\n";
    
    std::cout << "Addrof a() = " << reinterpret_cast<void*>(&A::a) << "\n";
    std::cout << "Addrof B's a() = " << reinterpret_cast<void*>(&B::a) << "\n";
    std::cout << "Addrof b() = " << reinterpret_cast<void*>(&B::b) << "\n";
    std::cout << "Addrof xy() = " << reinterpret_cast<void*>(&B::xy) << "\n";
    
    auto vptr = *reinterpret_cast<void***>(obj);//const_cast<A**>(reinterpret_cast<A**>(obj));
    int off = 0, nullCnt = 0;;
    try {
        while(nullCnt < 4) {
            std::cout << off << "\t" << vptr << "\t" << (*vptr);
            
            if(*vptr == aptr) std::cout << " FOUND A";
            std::cout  << "\n";
            
            //if(*vptr == 0x0) nullCnt++;
            //else nullCnt = 0;
            
            vptr++;
            off++;
        }
    } catch(const ExceptSegv& except) {
        _unused(except);
        std::cout << "[segfault]\n";
    }
    
    return 0;
}
