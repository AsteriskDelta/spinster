#include <iostream>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <list>
#include "Spinster.h"
#include "Net.h"
#include <sstream>
#include "Forwarding.h"

#include <type_traits>
#include <thread>

int printFromThread(int thrID, int gblID) {
    static std::mutex mtx;
    {
        std::lock_guard<std::mutex> lck(mtx);
        std::cout << "Thread llid=" << std::this_thread::get_id() << " on hlid=" << thrID << " got print request #" << gblID << "\n";
    }
    usleep(1000*1000);
    return 0;
}

int main(int argc, char** argv) {
    srand(time(nullptr));

    unsigned short expPort = 0;
    if(argc <= 1) {
        //std::cout << "need port\n";
        //return 1;
        expPort = 1339;
    } else {
        try {
            expPort = strtoul(argv[1], nullptr, 0); 
        } catch(...) { std::cerr << "couldn't parse port from " << argv[1] << "\n"; };
    }
    
    std::list<Spinster::Address> otherAddr;
    for(int i = 2; i < argc; i++) {
        try { otherAddr.push_back("127.0.0.1:"+std::to_string((unsigned short)strtoul(argv[i], nullptr, 0))); }
        catch(...) {};
    }
    
    Spin::MemberInfo myInfo;
    myInfo.type = "affinityTest";
    
    Spin::Initialize(Spinster::MemberID(8, expPort), myInfo);
    
    std::vector<Spin::Thread> threadGroups;
    threadGroups.resize(4);
    
    for(auto& ptr : threadGroups) {
        ptr = Spin::Local->thread();
    }
    usleep(1000*200);
    Spin::TaskSet tasks;
    
    for(int i = 0; i < 40; i++) {
        int thrID = i % 4;
        tasks += Spin::Local->invoke(printFromThread, thrID, i)->affinity(threadGroups[thrID]->id());
        //usleep(1000*1000);
    }
    
    Spin::Yield(tasks);
    
    std::cout << "All threads completed\n";
    

    return 0;
}
