#ifndef REMOTETASKS_H
#define REMOTETASKS_H
#include "Spinster.h"

namespace TestRemote {
    int testA(std::string str, double num) _remote;
    int testB(int i) _remote;
    bool testC() _remote;
};

_expose(TestRemote::testA, TestRemote::testB, TestRemote::testC);

#endif /* REMOTETASKS_H */

