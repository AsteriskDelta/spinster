#ifndef SPINSTER_H
#define SPINSTER_H
#include "SpinsterInclude.h"

#include "Expose.h"
#include "SynObject.h"
#include "SynGlobal.h"

#include "Priority.h"
#include "Task.h"
#include "TaskGroup.h"
#include "Member.h"
#include "Group.h"
#include "LocalGroup.h"
#include "MemberInfo.h"
#include "MemberLink.h"
#include "Thread.h"
#include "Invoker.h"
#include "Invokee.h"
#include "Event.h"
#include "Address.h"
#include "Protocol.h"
#include "TaskSet.h"

#include <condition_variable>

namespace Spinster {
    
    void Initialize(const MemberID& id);
    void Initialize(const MemberID& id, const MemberInfo& defInfo);
    void Terminate();
    
    //SafeRef<Obj::MemberLink> Connect(const Address& addr, const MemberID& forceID = MemberID());
    SafeRef<Obj::MemberLink> Connect(const Address& addr, Address lcl = Address());
    void Disconnect(SafeRef<MemberLink> link);
    
    Group DoJoin(GroupCredentials groupCreds);
    Invokee<decltype(&DoJoin), GroupCredentials> Join(const GroupCredentials& groupCreds);
    Group Create(const GroupCredentials& groupCreds);
    
    Group Join(Group grp);
    void Leave(Group grp);
    
    
    void Trust(Group grp);
    void CompleteTrust(Group grp);
    
    struct Dispatch_CallInfo {
        bool isRecursive = false;
    };
    bool Dispatch(Dispatch_CallInfo callInfo = Dispatch_CallInfo());
    //void Yield(const double& t);
    
    SafeRef<Obj::Member> GetMember(const MemberID& id);
    SafeRef<Obj::Group> GetGroup(const MemberID& id);
    bool AssignToMember(const MemberID& id, Task task);
    
    template<typename FN, typename ...Args>
    inline typename Obj::Invokee<FN, Args...>::ReturnType Yield(Obj::Invokee<FN, Args...> *invokee) {
        std::mutex yieldMtx; std::condition_variable yieldVar;
        if(thread) thread->setStatus(MemberStatus::Yielded);
        
        typedef typename Obj::Invokee<FN, Args...>::ReturnType RetType;
        RetType ret;
        
        invokee->persist();
        invokee->callback([&yieldVar, &yieldMtx, &ret](RetType retVal)->void {
            ret = retVal;
            std::unique_lock<std::mutex> lck(yieldMtx);
            yieldVar.notify_all();
            //DBG_EXCL(std::cout <<"INVOKEE CALLBACK\n");
            return;
        })->submit(true);
        //invokee = nullptr;//We already submitted and deleted, so don't double-free on destruction of invokee safeptr
        
        
        std::unique_lock<std::mutex> lck(yieldMtx);
        yieldVar.wait(lck);
        
        delete invokee;
        
        if(thread) thread->setStatus(MemberStatus::Executing);
        return ret;
    }
    template<typename FN, typename ...Args>
    inline typename Obj::Invokee<FN, Args...>::ReturnType Yield(Invokee<FN, Args...> &&invokee) {
        auto *const invokeePtr = &invokee;
        invokee = nullptr;//Prevent double execution
        return Yield<FN, Args...>(invokeePtr);
    }
    template<typename FN, typename ...Args, typename=typename std::conditional< std::is_base_of<InvokeeRoot, Bare<FN>>::value, std::false_type, std::true_type>::type>
    inline auto Yield(const FN& f, const Args&... args) {
        /*std::mutex yieldMtx; std::condition_variable yieldVar;
        std::unique_lock<std::mutex> lck(yieldMtx);
        thread->setStatus(MemberStatus::Yielded);
        
        typedef typename Obj::Invokee<FN, Args...>::ReturnType RetType;
        RetType ret;
        
        Local->schedule(Priority::Realtime, f, args...)->callback([&yieldVar, &ret](RetType retVal)->void {
            ret = retVal;
            yieldVar.notify_all();
            return;
        });
        yieldVar.wait(lck);
        
        thread->setStatus(MemberStatus::Executing);
        return ret;*/
        //auto invRet = ;
        return Yield(Local->schedule(bool(task)? task->priority : Priority::Immediate, f, args...));
    }
    
    void Yield(TaskSet& tasks);
    
    void YieldFor(const double& t);
    
    
    int SpinsterDispatcher();
};

namespace spin = Spinster;
namespace Spin = Spinster;

#endif /* SPINSTER_H */

