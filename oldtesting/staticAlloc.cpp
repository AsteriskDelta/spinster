#include <iostream>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
/*#include <list>
#include "Spinster.h"
#include "Net.h"*/
#include <sstream>
#include "SynObject.h"
#define STAT_DEF_WORKS

#define SPIN_EXPOSE_FF
//#define _expose()

//#define _expose_all()

bool functionRun = false;

class TestInit {
public:
    std::size_t tf;
    constexpr TestInit() : tf(-1) {
        if(!functionRun) std::cout << tf << " : It works!!!\n";
        else std::cout << tf << " : init after ;-;\n";
    }
    constexpr TestInit(const auto& t = "") : tf(t) {
        if(!functionRun) std::cout << tf << " : It works!!!\n";
        else std::cout << tf << " : init after ;-;\n";
    }
    /*~TestInit() {
        std::cout << "del testinit\n";
    }*/
};

#include <dlfcn.h>
extern TestInit init;
template<typename FF>
constexpr unsigned int proxll() {
    TestInit init(0);
    std::cout << "proxll called";
    return 1;
}
/*
template <char... Chars>
struct string_t {
    static constexpr char const* c_str() {
        constexpr char string[]={Chars...,'\0'};
        return string;
    }
};*/
namespace Constexpr {
    inline constexpr unsigned int strlen(const char *const str, const unsigned int idx = 0) {
        if(str[idx] == 0x0) return idx;
        else return strlen(str, idx+1);
    }
};

template<typename T, typename F>
class array_cast {
public:
    union {
        T& t;
        F& f;
    };
    
    inline constexpr operator T&() {
        return t;
    }
    inline constexpr array_cast(F& nf) : f(nf) {
        
    }
};
/*
template <char... letters>
struct string_t{
    static constexpr const std::size_t size = sizeof...(letters) / sizeof(char);
    static constexpr const char str[size] = {letters...};
    inline static constexpr const char* c_str() {
        return str;
    }
    //static constexpr const std::size_t size = Constexpr::strlen(rawStr);//sizeof...(letters) / sizeof(char);
    //static constexpr const char (&str)[size+1] = array_cast<const char[size+1]>(rawStr);
    //inline static constexpr const char* c_str() {
     //   constexpr const char string[size] = {letters...};
    //    return string;
    //}
};
template<char ...Args>
constexpr const char string_t<Args...>::str[string_t<Args...>::size];

namespace detail {
    template <typename Str,unsigned int N,char... Chars>
    struct make_string_t : make_string_t<Str,N-1,Str().chars[N-1],Chars...> {};

    template <typename Str,char... Chars>
    struct make_string_t<Str,0,Chars...> { typedef string_t<Chars...> type; };
} // namespace detail
 * */
/*
#define CSTR(str) []{ \
    struct Str { const char *chars = str; }; \
    return detail::make_string_t<Str,sizeof(str)>::type(); \
  }()

#define QREP(STR) static constexpr const char __func_link__[sizeof(STR)-2] 
#include <experimental/string_view>
template<const char *const SYM_PTR>
class InitTpl {
public:
    inline InitTpl() {
        val = reinterpret_cast<std::size_t>(( dlsym(RTLD_DEFAULT, SYM_PTR) ));
    };
    
    inline void doStuff () {
        std::cout << val.tf << "\n";
    }
    
    static TestInit val;
};
template<const char *const SYM_PTR> TestInit InitTpl<SYM_PTR>::val = TestInit(1);
void stTest(int i) {
    static constexpr const std::experimental::string_view funcView(__func__, sizeof(__func__));
    InitTpl<funcView.data()> itp;
    //itp.doStuff();
    //static Test st;
    std::cout << "test got " << i << "\n";
}*/
/*
#define MACRO_GET_1(str, i) \
(sizeof(str) > (i) ? str[(i)] : 0)

#define MACRO_GET_4(str, i) \
MACRO_GET_1(str, i+0),  \
MACRO_GET_1(str, i+1),  \
MACRO_GET_1(str, i+2),  \
MACRO_GET_1(str, i+3)

#define MACRO_GET_16(str, i) \
MACRO_GET_4(str, i+0),   \
MACRO_GET_4(str, i+4),   \
MACRO_GET_4(str, i+8),   \
MACRO_GET_4(str, i+12)

#define MACRO_GET_32(str, i) \
MACRO_GET_16(str, i+0),  \
MACRO_GET_16(str, i+16)

#define MACRO_GET_64(str, i) \
MACRO_GET_16(str, i+0),  \
MACRO_GET_16(str, i+16), \
MACRO_GET_16(str, i+32), \
MACRO_GET_16(str, i+48)

//CT_STR means Compile-Time_String
#define CT_STR(str) string_t<MACRO_GET_32(str, 0), 0 >//guard for longer strings
 * */
#include <tuple>
#include <utility>
template <size_t N, typename... Args>
decltype(auto) magic_get(Args&&... as) noexcept {
    return std::get<N>(std::forward_as_tuple(std::forward<Args>(as)...));
}
#include <functional>

struct TRT {
    int x;
};
namespace testN {
template<typename S1, typename OP_T, OP_T* OP>
class InitTpl : public TRT, public Spinster::SynObject {
public:
    _synit();
    /*typedef std::tuple< Args... > Tuple_T;
    static constexpr unsigned int TupleSize = sizeof...(Args);
    constexpr std::string unpackStr(const std::string& str, unsigned int idx, const Tuple_T& tup) {
        return str + 
    }*/
    /*template<typename ...OArgs> 
    constexpr std::string unpackStr(const std::string& str, const unsigned int idx, const Tuple_T& tup) {
        constexpr char c = std::get<idx>(tup);
        if(c == 0x0 || idx+1 == TupleSize) return str;
        else return unpackStr(str+c, idx+1, tup);
    }*/
    std::string symbolName;
    std::function<OP_T> target;
    //_syn(symbolName);
    //_syn_sym(symbolName);
    //static ::Spinster::SynObjectProxy<Self, decltype(symbolName), 
    //        (offsetof(Self, symbolName))> SPIN_PST(_syn_, SPIN_ESC(__COUNTER__)) __attribute__((used)) obj;
    static constexpr std::size_t off = offsetof(Self, symbolName);
    //static std::vector<decltype(off)> offVec;
    //static std::vector<decltype(symbolName)> nameVec;
    
    inline InitTpl() {
        std::string cname = __PRETTY_FUNCTION__;
        std::cout << "functionRun? " << functionRun << ", pretty " << cname << "\n";
        std::cout << "got ptr " << reinterpret_cast<void*>(OP) << "\n";
        std::cout << "name = " << typeid(Self).name() << "\n";
        //constexpr const Tuple_T tup(Args...);
        //char symbolName[tupSize];
        //constexpr int N[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
        //std::string str = {std::get<N>(std::forward<Tuple_T>(tup))...};//reinterpret_cast<const char*>(&(std::get<0>(tup)));
        //constexpr std::string symbolName = unpackStr("", 0, tup);
        //constexpr const char string[S1::size] = {S1::str;}//reinterpret_cast<const char[S1::size]>(S1::c_str());
        //static constexpr char symbolName[] = {Args..., 0x0};
        symbolName = S1::c_str();
        target = OP;
        //target = std::function<FN_T>();
        //constexpr void*const vp = __builtin_return_address(0);
        //constexpr const char xp = __PRETTY_FUNCTION__[0];
        std::cout << "Init got type " << symbolName << " at " << reinterpret_cast<void*>(OP) << "\n";
        //static constexpr auto tup = std::make_tuple(magic_get<);
        //static constexpr auto tup = std::make_tuple(magic_get(Args));
        //static constexpr symName = &tup.get(0);
        
        //val = reinterpret_cast<std::size_t>(( dlsym(RTLD_DEFAULT, symName) ));
    };
    
    inline void doStuff () {
        std::cout << val.tf << "\n";
    }
    
    static TestInit val;
};
};
using namespace testN;
/*
template<typename S1, typename FN_T, FN_T (*FN_PTR)>
std::string InitTpl<S1, FN_T, FN_PTR>::symbolName;
template<typename S1, typename FN_T, FN_T (*FN_PTR)>
std::function<FN_T> InitTpl<S1, FN_T, FN_PTR>::target;*/

template<typename S1, typename OP_T, OP_T* OP>
class InitProxy {
public:
    static InitTpl<S1,OP_T,OP> _tpl;
    static void dbg() {
        auto vp = reinterpret_cast<void*>(OP);
        auto ROP = reinterpret_cast<OP_T*>(vp);
        std::cout << "InitProxy with OP=" << reinterpret_cast<void*>(OP) << ", " << (ROP)(4,5) << " ?= 9\n";
        std::cout << "InitProxy with tpl =" << _tpl.symbolName << "\n";
    }
};

template<typename S1, typename OP_T, OP_T* OP>
InitTpl<S1,OP_T,OP> InitProxy<S1, OP_T, OP>::_tpl;

template<typename T>
inline constexpr char fnfirst(const T& fn) {
    return fn[3];
}
/*
#define FX(FV) "\"" #FV "\""
#define FY(FV) FX(FV)
#define FN FY(__FUNCTION__)
#define CT_PRX(x) CT_STR((x))
*/
//static constexpr void* _fnPtr = reinterpret_cast<void*>(&FN);
//#define _expose(FN) 
//static InitProxy<CT_STR(FY(FN)), decltype(FN), FN> __exposeTemplate;
//std::cout <<_fnPtr << " with tpl = " << decltype(__exposeTemplate)::_tpl.symbolName << "\n";

//void stTest(int i);
int stTest(int i, int j) { 
    //static constexpr char c = fnfirst(FX(__FUNCTION__));
    //std::cout << "got fn char=" << c << "\n";
    //extern constexpr const char* __func__;
    //static constexpr const char pf = __func__[0];
    //const char
    //static InitTpl<CT_STR("stTest")> tpl;
std::cout << "GOT STTEST i=" << i << ", j=" << j << "\n";
return i+j;
}

template<typename OP_T, OP_T* OP>
class TestFnc {
public:
    int do_op(int a, int b) { return (*OP)(a,b); }
};

template<typename F, F* OP>
int do_op(int a, int b) { return (*OP)(a,b); }
float fadd(float a, float b) { return a+b; };

#define PP_NARG(...)    PP_NARG_(__VA_ARGS__,PP_RSEQ_N())
#define PP_NARG_(...)   PP_ARG_N(__VA_ARGS__)

#define PP_ARG_N( \
        _1, _2, _3, _4, _5, _6, _7, _8, _9,_10,  \
        _11,_12,_13,_14,_15,_16,_17,_18,_19,_20, \
        _21,_22,_23,_24,_25,_26,_27,_28,_29,_30, \
        _31,_32,_33,_34,_35,_36,_37,_38,_39,_40, \
        _41,_42,_43,_44,_45,_46,_47,_48,_49,_50, \
        _51,_52,_53,_54,_55,_56,_57,_58,_59,_60, \
        _61,_62,_63,N,...) N

#define PP_RSEQ_N() \
        63,62,61,60,                   \
        59,58,57,56,55,54,53,52,51,50, \
        49,48,47,46,45,44,43,42,41,40, \
        39,38,37,36,35,34,33,32,31,30, \
        29,28,27,26,25,24,23,22,21,20, \
        19,18,17,16,15,14,13,12,11,10, \
        9,8,7,6,5,4,3,2,1,0

#define Paste(a,b) a ## b
#define XPASTE(a,b) Paste(a,b)

#define APPLYX1(FUNC, a)           FUNC(a)
#define APPLYX2(FUNC, a,b)         FUNC(a) FUNC(b)
#define APPLYX3(FUNC, a,b,c)       FUNC(a) FUNC(b) FUNC(c)
#define APPLYX4(FUNC, a,b,c,d)     FUNC(a) FUNC(b) FUNC(c) FUNC(d)
#define APPLYX5(FUNC, a,b,c,d,e)   FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e)
#define APPLYX6(FUNC, a,b,c,d,e,f) FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f)
#define APPLYX7(FUNC, a,b,c,d,e,f,g) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g)
#define APPLYX8(FUNC, a,b,c,d,e,f,g,h) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h)
#define APPLYX9(FUNC, a,b,c,d,e,f,g,h,i) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i)
#define APPLYX10(FUNC, a,b,c,d,e,f,g,h,i,j) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i) FUNC(j)
#define APPLYX11(FUNC, a,b,c,d,e,f,g,h,i,j,k) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i) FUNC(j) FUNC(k)
#define APPLYX12(FUNC, a,b,c,d,e,f,g,h,i,j,k,l) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i) FUNC(j) FUNC(k) FUNC(l)
#define APPLYX13(FUNC, a,b,c,d,e,f,g,h,i,j,k,l,m) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i) FUNC(j) FUNC(k) FUNC(l) FUNC(m)
#define APPLYX14(FUNC, a,b,c,d,e,f,g,h,i,j,k,l,m,n) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i) FUNC(j) FUNC(k) FUNC(l) FUNC(m) FUNC(n)
#define APPLYX15(FUNC, a,b,c,d,e,f,g,h,i,j,k,l,m,n,o) \
    FUNC(a) FUNC(b) FUNC(c) FUNC(d) FUNC(e) FUNC(f) FUNC(g) FUNC(h) FUNC(i) FUNC(j) FUNC(k) FUNC(l) FUNC(m) FUNC(n) FUNC(o)
#define APPLYX_(M, ...) M(__VA_ARGS__)
#define APPLYXn(FUNC, ...) APPLYX_(XPASTE(APPLYX, PP_NARG(__VA_ARGS__)),FUNC, __VA_ARGS__)


//inline std::string _expose_scope_q();

template<typename OP_T, OP_T* OP>
class SymbolLookup {
public:
    std::string symbolName;
    std::string returnString;
    std::string argsString;
    std::function<OP_T> target;
    
    
    inline SymbolLookup() {
        std::string cname = __PRETTY_FUNCTION__;
        try {
            const std::string opHeader = "OP_T* OP = ";
            const std::string typesHeader = " [with OP_T = ";
            
            const auto opStart = cname.find(opHeader);
            const auto opEnd = cname.find_last_of("]");
            symbolName = cname.substr(opStart+opHeader.size(), opEnd - opStart - opHeader.size());
            
            const auto typeStart = cname.find(typesHeader) + typesHeader.size();
            const auto typeEnd = cname.find_last_of(")", opStart);
            const auto retEnd = cname.find_first_of("(", typeStart+1);
            
            returnString = cname.substr(typeStart, retEnd-typeStart);
            argsString = cname.substr(retEnd+1, typeEnd - retEnd - 1);
        } catch(...) {
            symbolName = "err";
            returnString = "void";
            argsString = "";
        }
        std::cout << "BEGIN REG, "<<returnString << " " << symbolName << "(" << argsString << ")" << "\n";
        std::cout << "functionRun? " << functionRun << ", pretty " << cname << "\n";
        std::cout << "got ptr " << reinterpret_cast<void*>(OP) << "\n";
        //symbolName = S1::c_str();
        target = OP;
        std::cout << "REG got type " << symbolName << " at " << reinterpret_cast<void*>(OP) << "\n";

    };
};
/*
template<typename S1>
class ScopeTpl {
public:
    ScopeTpl() {
        std::cout << "\n\nSCOPETPL init, pf=" << __PRETTY_FUNCTION__ << "\n\n";
    }
};

template<typename S1>
struct ScopeProxy {
    static ScopeTpl<S1> _tpl __attribute__((used));
};
*/

template<typename OP_T, OP_T* OP>
class SymbolLookupProxy {
public:
    static SymbolLookup<OP_T,OP> _lookup  __attribute__((used));
};
template<typename OP_T, OP_T* OP>
SymbolLookup<OP_T,OP> SymbolLookupProxy<OP_T,OP>::_lookup;
/*
#define ESC(...) __VA_ARGS__
#define FWD(tx) tx
#define OST(a,b) a ## b
#define PST(a,b) OST(a,b)
#define _expose_sym_ll(sym) static ::SymbolLookupProxy< decltype(sym), sym> PST(_expose_, ESC(__COUNTER__)) __attribute__((used));
#define _expose_sym(sym) _expose_sym_ll(sym)
#define _expose(...) APPLYXn(_expose_sym_ll, __VA_ARGS__);
*/
//#define _share_sym_ll(sym) static ::SymbolLookupProxy< decltype(sym), sym> PST(_expose_, ESC(__COUNTER__)) __attribute__((used));
//#define _share_sym(sym) _expose_sym_ll(sym)
//#define _share(...) APPLYXn(_expose_sym_ll, __VA_ARGS__);

//#undef X

template<int X>
int otherTest() {
    return 5*X;
}

//static ::ScopeProxy<CT_STR("TEST")> __scopeProxy __attribute__((used));
_expose(stTest,
        fadd);
_expose(otherTest<3>, 
        otherTest<8>);
/*
int weakTest() __attribute__((weak));
int weakTest() {
    std::cout << "t1";
    return 1;
}
int weakTest() {
    std::cout << "t2";
    return 2;
}*/
#define spun __attribute__((weak))
int preOnly() spun;

_expose(preOnly);

#define MCT(FN, v) FN(v)
#define TESTW(x) ("TEST WORKS FOR " # x)
#define TQUOTE(x) MCT(TESTW, x)

namespace other {
    char at(int i) {
        return static_cast<char>(i);
    }
    _expose(at);
}

#include "remoteTasks.h"

int main(int argc, char** argv) {
    std::cout << "top of main, " << TQUOTE("testContents") << "\n";
    //static InitProxy<CT_STR(FY(FN)), decltype(FN), typename> __exposeTemplate;
    int c = do_op<decltype(stTest), stTest>(4,5);
    std::cout << "Main start, c = " << c <<"\n";
    functionRun = true;
    TestFnc<decltype(stTest), stTest> stft;
    std::cout << "cls mode: " << stft.do_op(4,5) << "\n";
    int x;
    //new(&x) int;
   // decltype(__exposeTemplate)::dbg();
    //proxll<int>();
    //stTest(argc);
    //((void)::ScopeProxy<CT_STR("TEST")>::_tpl);
    return 0;
}