#include "SymbolTable.h"
#include "Group.h"
#include <sstream>
#include "LocalGroup.h"
#include "Protocol.h"
#include "Spinster.h"

namespace Spinster {

    SymIDT InitForceSymbolID = 0;

    void RegisterSymbol(const SymbolLookupIndex &ins, const SymbolLookupHandler &hand) {
        _unused(ins);
        _unused(hand);
        //TODO
    }

    void MapSymbol(SymIDT from, SymIDT to) {
        Obj::SymbolTable *table = &Local->symbols();
        table->mapID(from, to);
    }

    namespace Obj {

        SymbolEntry::SymbolEntry(const SymIDT& i, const SymbolLookupIndex& idx, const SymbolLookupHandler& handle)
        : id(i), handler(handle), index(idx) {

        }

        std::string SymbolEntry::str() const {
            std::stringstream ss;
            ss << index.type << " " << index.returns << " " << (index.name.size() > 0 ? index.name : "[deferred]") << " (" << index.arguments << ")";
            return ss.str();
        }

        SymbolTable::SymbolTable() : parent(nullptr), entries(), lookups() {
        }

        SymbolTable::SymbolTable(SafeRef<Group> par) : parent(par), entries(), lookups() {
            //std::cout << "SymbolTable constructor\n";
            if (par->isLocal()) {
                entries.resize(SPINSTER_RESERVED_SYMBOLS);
            }

        }

        SymbolTable::~SymbolTable() {

        }

        $::SymbolEntry SymbolTable::add(const SymbolLookupIndex& idx, const SymbolLookupHandler& handle) {
            //std::cout << "adding " << idx.name << "\n";
            auto it = lookups.find(idx);
            SymIDT id;
            if (it == lookups.end()) id = this->addEntry(idx, handle);
            else id = it->second;

            SymbolEntry *sym = &entries[id];
            //std::cout << "accessing sym at " << id << " -> " << sym << "\n";
            //if(idx.type == SymbolType::Function) handle.constructArgs(nullptr, nullptr);
            //std::cout << "Existing sym name:" << sym->index.name << "\n";
            //Update to the newest mapping given
            if (idx.name.size() != sym->index.name.size() && idx.name.size() > 0) {
                sym->~SymbolEntry();
                new(sym) SymbolEntry(id, idx, handle);
                lookups.erase(it);
                lookups[idx] = id;
            }

            if (sym->index.address != nullptr) {
                addresses[sym->index.address] = id;
            }
            return sym;
        }

        /*bool SymbolTable::remove(const SymbolLookupIndex& idx) {
            _unused(idx);
        }*/

        SymIDT SymbolTable::getID(const SymbolLookupIndex& idx) {
            auto it = lookups.find(idx);

            if (it == lookups.end()) return 0;
            else return it->second;
        }

        $::SymbolEntry SymbolTable::get(const SymbolLookupIndex& idx, MemberLink *context) {
            _unused(context);
            auto it = lookups.find(idx);

            if (it == lookups.end()) {
                if (!parent->isLocal()) {//We're remote, so query the assignee for the symbol's info

                }
                it = lookups.find(idx);
                if (it == lookups.end()) return nullptr;
            }

            const SymIDT id = it->second;
            return this->at(id);
        }

        $::SymbolEntry SymbolTable::at(const SymIDT& id, MemberLink *context) {
            _unused(context);
            if (id >= entries.size()) return nullptr;
            else return entries[id];
        }

        $::SymbolEntry SymbolTable::addr(void *const& ptr) {
            //DBG_EXCL(std::cout << "looking up addr " << ptr << "\n");
            auto it = addresses.find(ptr);
            if (it == addresses.end()) return nullptr;

            //DBG_EXCL(std::cout << "got id=" << it->second << ", " << this->at(it->second)->index.name << "\n");
            return this->at(it->second);
        }

        $::SymbolEntry SymbolTable::request(const SymbolLookupIndex& idx) { //Yields (ie, blocks active task)
            const SymIDT lclID = $::Local->symbols()->getID(idx);
            SymIDT rval = Spinster::Yield(parent->invoke(Protocol::Sym::SymLookup, idx)->returned([this, lclID](SymIDT extID) mutable -> void {
                this->conversion(lclID, extID);
            }));
            
            return $::Local->symbols()->at(this->convert(rval));
        }

        void SymbolTable::prefetch(const SymbolLookupIndex& idx) { //Nonblocking
            const SymIDT lclID = $::Local->symbols()->getID(idx);
            parent->invoke(Protocol::Sym::SymLookup, idx)->returned([this, lclID](SymIDT extID) mutable -> void {
                this->conversion(lclID, extID);
            });
        }

        void SymbolTable::mapID(const SymIDT& from, const SymIDT& to) {
            if (entries.size() < to) {
                std::cerr << "MapSymbolID called on " << from << " -> " << to << ", which is larger than table of size " << entries.size() << "\n";
                return;
            }

            //entries[to].handler.constructArgs(nullptr, nullptr);

            entries[from] = entries[to];
            entries[from].id = from;
            addresses[this->lookupOf(from).address] = from;
            /*std::cout << "MAPPED " << from << " to " << to << ", for construct args ptr=" 
                    << entries[from].handler.constructArgs.target<bool(*)(void*,void*)>() << "\n"
                    << "\t from ptr: " << entries[to].handler.constructArgs.target<bool(*)(void*,void*)>() << "\n";
            entries[to].handler.constructArgs(nullptr, nullptr);*/
        }

        SymIDT SymbolTable::addEntry(const SymbolLookupIndex& idx, const SymbolLookupHandler& handle) {
            SymIDT id = entries.size();
            entries.push_back(SymbolEntry(id, idx, handle));
            lookups[idx] = id;
            //std::cout << "\treturned entry " << idx.name << " at " << id << "\n";
            //std::cout << "\treading value at " << &entries[id] << " = " << entries[id].str() << "\n";
            //std::cout << "\tname = " << entries[id].index.name << "\n\n";
            return id;
        }

        bool SymbolTable::knows(const SymIDT& id) {
            return id < SPINSTER_RESERVED_SYMBOLS || addresses.find(reinterpret_cast<void*> (uint64_t(id))) != addresses.end();
        }

        void SymbolTable::conversion(const SymIDT& local, const SymIDT& remote) {
            void *const lkAddr = reinterpret_cast<void*> (uint64_t(local));
            addresses[lkAddr] = remote;
        }

        SymIDT SymbolTable::convert(const SymIDT& localID) {
            if (localID < SPINSTER_RESERVED_SYMBOLS) return localID;
            else {
                auto it = addresses.find(reinterpret_cast<void*> (uint64_t(localID)));
                if (it == addresses.end()) {
                    std::cerr << "can't convert symbol local#" << localID << "\n";
                    return 0;
                }
                return it->second;
            }
        }

    }
}
