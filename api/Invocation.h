#ifndef INVOCATION_H
#define INVOCATION_H
#include "SpinsterInclude.h"

namespace Spinster {
    bool QueueSymInvocation(SafeRef<Obj::SymbolEntry> sym, SafeRef<Message> msg);
    
    bool HandleSymInvocation(SafeRef<Obj::SymbolEntry> sym, SafeRef<Message> msg);
    void ReturnSymInvocation(SafeRef<Message> msg, SafeRef<MemBuffer> outputBuffer);
};

#endif /* INVOCATION_H */

