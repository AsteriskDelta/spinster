#include <iostream>
#include "Expose.h"
#include <cxxabi.h>
#include <memory>
#include "SynObject.h"
#include <sstream>

namespace Spinster {
    std::string Demangle(const char* name) {
        int status = -4; // some arbitrary value to eliminate the compiler warning
        std::unique_ptr<char, void(*)(void*) > res{
            abi::__cxa_demangle(name, NULL, NULL, &status),
            std::free};

        return (status == 0) ? res.get() : name;
    }

    std::ostream& operator<<(std::ostream& out, const SymbolType& g) {
        switch (g) {
            default:
            case SymbolType::Void: out << "Void";
                break;

            case SymbolType::Function: out << "Function";
                break;
            case SymbolType::Variable: out << "Variable";
                break;
            case SymbolType::Class: out << "Class";
                break;
            case SymbolType::MemberVar: out << "MemberVar";
                break;
            case SymbolType::Namespace: out << "Namespace";
                break;
        }
        return out;
    }
    
    std::string SymbolLookupIndex::str() const {
        std::stringstream ss;
        ss << "" << type << " " << this->returns << " " << this->name << "(" << this->arguments << "";
        return ss.str();
    }
    
    
    _share(SymbolLookupIndex, name, returns, arguments, address, type);
};