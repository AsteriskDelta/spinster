#ifndef INVOKEE_H
#define INVOKEE_H
#include "SpinsterInclude.h"
#include <type_traits>
#include <functional>
#include <list>
#include "Priority.h"
#include "SafeRef.h"
#include "MemberID.h"
#include "Task.h"
#include "LocalTask.h"
#include "RemoteTask.h"
#include "ExternalTask.h"
#include "Message.h"
#include <tuple>
#include <experimental/tuple>
#include "MemBuffer.h"
#include "Serializers.h"

#include "Expose.h"
#include "SymbolTable.h"

namespace Spinster {
    
    template<typename T>
    void* GetFNPtr(const T& obj, typename std::enable_if<std::is_function<T>::value || std::is_function<typename std::remove_pointer<T>::type>::value, const void *const&>::type vp = nullptr) {
        _unused(vp);
        return reinterpret_cast<void*>(reinterpret_cast<typename std::add_pointer<typename std::remove_pointer<T>::type>::type>(obj));
    }
    template<typename T>
    void* GetFNPtr(const T& obj, typename std::enable_if<!(std::is_function<T>::value || std::is_function<typename std::remove_pointer<T>::type>::value), const void *const&>::type vp = nullptr) {
        _unused(vp);
        _unused(obj);
        return nullptr;
        /*union FT {
            const T& robj;
            void *const& rptr;
            inline FT(const T& o) : robj(o) {};
        };
        FT ft(obj);
        return ft.rptr;*/
        //return reinterpret_cast<void*>(std::function<T>(obj).template target<T>());
    }
    
    namespace Obj {
        class Member;
        class TaskGroup;
    }
    SafeRef<Obj::Member> GetMember(const MemberID& id);
    bool AssignToMember(const MemberID& id, Task task);
     
    struct InvokeeRoot {};
    
    namespace Obj {
        
        template<typename T>
        class SymbolLookupLamda {
            public:
                SymIDT symbolID;
                SymbolLookupLamda(const T& obj) : symbolID(SymIDT_None) {
                    //symbolID = SymIDT_None;//LookupSymbolAddr(obj);
                    //void* addr = reinterpret_cast<void*>(obj.target<T>());
                    //DeferredSymbolLookup
                    _unused(obj);
                }
        };

        template<typename FN, typename ...Args>
        class Invokee : public InvokeeRoot {
        public:
            typedef typename std::result_of<FN(Args...)>::type ReturnType;
            typedef Invokee<FN,Args...> Self;
            
            inline constexpr Invokee() : target(), priority(Priority::Normal), callbacks(), rTaskGroup(), dataBuffer(nullptr), symbolEntry(nullptr), flags() {
            };

            inline constexpr Invokee(const Invokee& orig) : 
            target(orig.target), func(orig.func), priority(orig.priority), 
                    callbacks(orig.callbacks), data(orig.data), rTaskGroup(orig.rTaskGroup), dataBuffer(nullptr), symbolEntry(nullptr), flags(orig.flags) {
            };

            inline constexpr Invokee(const MemberID& targ, const Priority &pr, FN f, const Args&... args) : target(targ), func(f), 
                    priority(pr), data(args...), rTaskGroup(), dataBuffer(nullptr), symbolEntry(nullptr), flags() {
                //DBG_EXCL(std::cout << "created invokee for " << target << ", " << pr << ", fn=" << &f << " T=" << typeid(decltype(f(args...))).name() << ", args=\n");
            };

            typedef ReturnType CallbackArg;

            MemberID target, sender;
            FN func;
            Priority priority;
            typedef std::function<void(CallbackArg) > Callback;
            std::list<Callback> callbacks;
            std::tuple<Args...> data;
            SafeRef<TaskGroup> rTaskGroup;
            union {
                MemBuffer* dataBuffer;
                Message* messagePtr;
            };
            SymbolEntry *symbolEntry;
            
            struct {
                bool persistant = false;
                bool disabled = false;
                bool fromMessage = false;
            } flags;
            
            $::RemoteTask remoteTask;

            inline Invokee* returned(Callback cb) {
                callbacks.push_back(cb);
                return this;
            }
            inline Invokee* callback(Callback cb) {
                return returned(cb);
            }
            inline Invokee* on(Callback cb) {
                return returned(cb);
            }
            inline Invokee* operator()(Callback cb) {
                return returned(cb);
            }
            
            
            inline Invokee* taskGroup(SafeRef<TaskGroup> tg) {
                this->rTaskGroup = tg;
                return this;
            }
            inline Invokee* prioritize(const $::Priority& pr) {
                priority = pr;
                return this;
            }
            inline Invokee* persist() {
                flags.persistant = true;
                return this;
            }
            void disable() {
                flags.disabled = true;
            }
            
            inline Invokee* from(const MemberID& mID) {
                sender = mID;
                return this;
            }
            inline Invokee* to(const MemberID& tID) {
                target = tID;
                return this;
            }
            inline Invokee* affinity(const MemberID& tID) {
                return this->to(tID);
            }
            
            inline Invokee* fromMessage(Message *const& msg) {
                this->messagePtr = msg;
                this->sender = msg->header.from;
                this->target = msg->header.to;
                this->priority = msg->header.priority;
                flags.fromMessage = true;
                return this;
            }
            inline Invokee* symbol(SymbolEntry *const& sym) {
                symbolEntry = sym;
                return this;
            }
            
            inline Invokee* noReturn() {
                this->priority.noReply();
                return this;
            }
            
            /*template<typename T>
            inline void serializeElement(const T& obj) {
                Serialize(obj, this->dataBuffer);
            }*/
            
            inline $::Task submit(const bool oneshot = false) {
                if(flags.disabled) return nullptr;
                
                if(oneshot) flags.disabled = true;
                
                if(!sender) sender = MyID();
               // #pragma GCC diagnostic push
//#pragma GCC diagnostic warning "-fpermissive"
                //static typename std::conditional<!(std::is_function<FN>::value || std::is_function<typename std::remove_pointer<FN>::type>::value), SymbolLookupLamda<FN>,
                //        DeferredSymbolLookup<typename std::remove_pointer<decltype(func)>::type>>::type _lkup __attribute__((used))(func);
                //_unused(_lkup);
//#pragma GCC diagnostic pop
                //static typename std::enable_if<std::is_function<DeferredSymbolLookup<typename std::remove_pointer<decltype(func)>::type>>::value, 
                //         DeferredSymbolLookup<typename std::remove_pointer<decltype(func)>::type>>::type _lkup(func);
                
                //DBG_EXCL(std::cout << "SUBMIT to " << target << " with priority " << priority << " with " << callbacks.size() << " calls\n");
                $::Task retRef;
                bool selfDelete = false;
                if(!target) target = MyID();//Assign to self by default
                
                if(!sender.isLocal() && target.isLocal()) {
                    if(true/*||target.isLocal()*/) {//We're supposed to execute it, queue it as an external task
                        $::ExternalTask locTask = new $::Obj::ExternalTask();
                        locTask->message = this->messagePtr;
                        if(this->symbolEntry != nullptr) {
                            locTask->entry = this->symbolEntry;
                            locTask->symbolID = symbolEntry->id;
                        }
                        
                        retRef = &locTask;
                        retRef->symbolID = messagePtr->symbolID();
                        selfDelete = true;
                        //rTaskGroup = ;
                    } else {//Forward the message to whomever needs it next
                        std::cerr << "Asked to forward message to #" << target << "\n";
                        delete this;
                        return nullptr;
                        //throw proxyExcept;
                        //rTaskGroup = ;
                        
                    }
                } else if(target.isLocal()) {//Local task, queue it for one of our threads
                    //retRef->setWorker(target);
                    Self *const invokeeToDelete = flags.persistant? nullptr : this;
                    
                    $::LocalTask locTask = new $::Obj::LocalTask();
                    locTask->sender = MyMember();
                    locTask->invocation = std::function<void(void*)>([=](void *objPtr) {
                        ReturnType& rt = *reinterpret_cast<ReturnType*>(objPtr);
                        
                        std::exception proxyExcept;
                        bool proxyThrow = false;
                        try {
                            rt = std::experimental::apply(func, data);
                        } catch(const std::exception& exp) {
                            proxyExcept = exp;
                            proxyThrow = true;
                        }
                        
                        //for(const auto& cb : callbacks) cb(rt);
                        for(auto it = callbacks.rbegin(); it != callbacks.rend(); ) {
                            auto next = std::next(it);
                            const bool isLast = (next == callbacks.rend());
                            (*it)(rt);
                            if(isLast) break;
                            else it = next;
                        }
                        rt.~ReturnType();
                        
                        if(invokeeToDelete != nullptr) {
                            //invokeeToDelete->disable();
                            delete invokeeToDelete;
                        }
                        
                        const_cast<typename std::remove_const<decltype(&locTask)>::type>(&locTask)->complete(!proxyThrow);
                       
                        if(proxyThrow) throw proxyExcept;
                    });
                    
                    
                    retRef = &locTask;
                    void *const funcPtr = GetFNPtr(func);
                    retRef->symbolID = LookupSymbolIDAddr(funcPtr);
                    if(retRef->symbolID == SymIDT_None && funcPtr != nullptr) {//Register an object (done in its constructor) if not yet matched
                        typename std::conditional<!(std::is_function<FN>::value || std::is_function<typename std::remove_pointer<FN>::type>::value), SymbolLookupLamda<FN>,
                        DeferredSymbolLookup<typename std::remove_pointer<decltype(func)>::type>>::type _lkup (func);
                        _unused(func);
                    }
                } else {//Remote task, ask someone else to do it
                    
                    if(!remoteTask) {
                        remoteTask = new $::Obj::RemoteTask();
                        if(flags.fromMessage) {
                            remoteTask->sender = GetMember(messagePtr->to());
                            remoteTask->symbolID = messagePtr->symbolID();
                            remoteTask->data = messagePtr->data;
                        } else {
                            remoteTask->sender = MyMember();
                            void *const funcPtr = GetFNPtr(func);
                            retRef->symbolID = LookupSymbolIDAddr(funcPtr);
                            dataBuffer = new MemBuffer();
                            remoteTask->data = dataBuffer;
                        }
                        
                        Self *const invokeeToDelete = flags.persistant? nullptr : this;



                        remoteTask->onReturn = std::function<void(void*)>([=](void *objPtr) {
                            //Post-execution, objPtr contains return value
                            const bool failed = (objPtr == nullptr);
                            if(!failed) {
                                ReturnType& rt = *reinterpret_cast<ReturnType*>(objPtr);

                                for(const auto& cb : callbacks) cb(rt);
                                rt.~ReturnType();
                            }
                            const auto* invokeeDelPtr = invokeeToDelete;//Force stack allocation so we can delete our tuple holder
                            const_cast<typename std::remove_const<decltype(&remoteTask)>::type>(&remoteTask)->complete(!failed);

                            if(invokeeDelPtr != nullptr) delete invokeeDelPtr;
                            //if(proxyThrow) throw proxyExcept;
                        });
                        
                        //void *const funcPtr = GetFNPtr(func);
                        retRef = &remoteTask;
                        //retRef->symbolID = LookupSymbolIDAddr(funcPtr);
                        //retRef->sender = MyMember();
                    } else {//Immortal task, just update frame
                        retRef = &remoteTask;
                    }

                    const size_t frameStart = remoteTask->data->size();
                    const auto serArgs = std::tuple_cat(std::make_tuple(SafeRef<MemBuffer>(remoteTask->data)), data);
                    bool worked = std::apply([&](SafeRef<MemBuffer>buff, const Args&... args) -> bool {
                        return SerializeTo<Args...>(buff, args...);
                    }, serArgs);
                    const size_t frameSize = remoteTask->data->size() - frameStart;

                    if (!worked) {
                        std::cerr << "Remote task serialization failed, dropping it...\n";
                        if(!priority.immortal()) {
                            delete dataBuffer;
                            delete &remoteTask;
                        }
                        return nullptr;
                    }
                    
                    std::cout << "remote frame from " << frameStart << " to " << remoteTask->data->size() << ", immortal=" << priority.immortal() << ", rtaskMsg=" << remoteTask->cachedMessage << "\n";
                    if(priority.immortal() && remoteTask->cachedMessage != nullptr) {
                        remoteTask->cachedMessage->sendFrame(frameStart, frameSize);
                    }
                    
                    /*std::cout << "packet dump for remote sz=" << dataBuffer->size() << ":\n" << std::hex;
                    for(unsigned int i = 0; i < dataBuffer->size(); i++) {
                        if(dataBuffer->at(i) < 16) std::cout << 0;
                        std::cout << std::hex << static_cast<uint16_t>(dataBuffer->at(i)) << " ";
                        if(i != 0 && i % 24 == 0) std::cout << "\n";
                    }
                    std::cout << std::dec << "\n";*/
                    
                    //retRef->symbolID = SymIDT_None;
                    //retRef = &remoteTask;
                }
                
                retRef->setStatus($::Tasks::Constructed);
                retRef->setPriority(priority);
                retRef->setTaskGroup(rTaskGroup);
                
                AssignToMember(target, retRef);
                if(selfDelete) delete this;
                return retRef;
            }
            inline void emit() {
                this->submit();
            }
            template<class=void>
            inline void emit(const Args&... args) {
                this->data = {args...};
                this->submit();
            }
            template<class=void>
            inline void setData(const Args&... args) {
                this->data = {args...};
            }
        private:

        };
    };

    template<typename FN, typename ...Args>
    class Invokee : public SafeRef<Obj::Invokee<FN, Args...>>, public InvokeeRoot  {
    public:
        typedef SafeRef<Obj::Invokee<FN, Args...>> Parent;
        typedef Obj::Invokee<FN, Args...> Endpoint;

        inline Invokee() : Parent() {
        };
        inline Invokee(Endpoint* ep) {
            this->obj = ep;
        };
        inline ~Invokee() {
            if(Parent::valid()) {
                (*this)->submit();
                //delete Parent::obj;
            }
        }
        using Parent::Parent;
    };
    //SPINSTER_SAFEREF_NULL(Invokee);

};

#endif /* INVOKEE_H */

