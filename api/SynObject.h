#ifndef SYNOBJECT_H
#define SYNOBJECT_H
#include "SpinsterInclude.h"
#include <vector>
#include "Expose.h"
#include "SafeRef.h"
#include <cstddef>
#include <utility>
#include <functional>

#include "Serializers.h"


/*
template<typename T, typename U> inline constexpr std::size_t moffsetof(U T::*member) {
            return (char*)&((T*)nullptr->*member) - (char*)nullptr;
        }
*/
#define _share_sym_ll(GRP,sym) \
__attribute__((used)) __attribute((weak)) void SPIN_PST(_share,SPIN_ESC(__COUNTER__))(\
 ::Spinster::SynObjectProxy<GRP, decltype(GRP::sym), offset_of<GRP, decltype(GRP::sym), &GRP::sym>(), CT_STR(QUOTE(sym))>){};\


//static ::Spinster::SynObjectProxy<GRP, decltype(GRP::sym), offset_of<GRP, decltype(GRP::sym), &GRP::sym>(), CT_STR(QUOTE(sym))> 
//SPIN_PST(SPIN_PST(SPIN_PST(_share,),_),sym);};

//__attribute__((used));
//;

#define _share_sym(GRP, sym) _share_sym_ll(GRP,sym)
#define _share(GRP, ...) APPLYPXn(_share_sym,GRP, __VA_ARGS__);

#define _synit() \
inline auto _syn_this() const -> decltype(this) {\
    return this;\
}\
typedef std::result_of<typename std::remove_pointer<decltype(&_syn_this)>::type> Self;

#define _shared() \
template<typename T>\
inline bool serialize(SafeRef<MemBuffer> &buffer) {\
    bool ret = true;\
    for (SerialFN& fn : SynSub<T>::serializers) {\
        ret &= fn(const_cast<::Spinster::SynObject*> ((const ::Spinster::SynObject*)this), buffer);\
    }\
    return ret;\
}\
template<typename T>\
inline bool deserialize(SafeRef<MemBuffer> &buffer) {\
    bool ret = true;\
    for (SerialFN& fn : SynSub<T>::deserializers) {\
        ret &= fn(const_cast<::Spinster::SynObject*> ((::Spinster::SynObject*)this), buffer);\
    }\
    return ret;\
}\
inline bool serialize(SafeRef<MemBuffer> &buffer) {\
    return this->serialize < Bare<decltype(*this)>>(buffer);\
}\
inline bool deserialize(SafeRef<MemBuffer> &buffer) {\
    return this->deserialize < Bare<decltype(*this)>>(buffer);\
}


/*inline auto _syn_this() const {\
    return this;\
}\
typedef decltype(std::declval(_syn_this())) Self;*/

namespace Spinster {
    class Syn;
    typedef std::function<bool(void *const&, SafeRef<MemBuffer>) > SerialFN;

    template<typename T>
    struct SynSub {
        //static std::vector<void*> offsets;
        static std::vector<SerialFN> serializers;
        static std::vector<SerialFN> deserializers;
        struct Noisy {
            Noisy() {
                std::cout << "init for syn serializer vector\n";
            }
        };
        static Noisy dbg;
    };
    template<typename T> _dalloc_pre() std::vector<SerialFN> SynSub<T>::serializers{};
    template<typename T> _dalloc_pre() std::vector<SerialFN> SynSub<T>::deserializers{};
    template<typename T> _dalloc_pre() typename SynSub<T>::Noisy SynSub<T>::dbg;

    class SynObject {
    public:
        //SynObject();
        //SynObject(const SynObject& orig);
        //virtual ~SynObject();

        SafeRef<Syn> expose();
        bool exposed() const;
        bool hide();

        //inline auto getType() { return *this; };
        //typedef decltype(getType()) Self;

        //virtual bool serialize(SafeRef<MemBuffer> buffer) const;
        //virtual bool deserialize(SafeRef<MemBuffer> buffer);
        /*template<typename T>
        inline bool serialize(SafeRef<MemBuffer> buffer) const {
            bool ret = true;
            for(SerialFN& fn : SynSub<T>::serializers) {
                ret &= fn(const_cast<SynObject*>(this), buffer);
            }
            return ret;
        }
        template<typename T>
        inline bool deserialize(SafeRef<MemBuffer> buffer) {
            bool ret = true;
            for(SerialFN& fn : SynSub<T>::deserializers) {
                ret &= fn(const_cast<SynObject*>(this), buffer);
            }
            return ret;
        }
        inline bool serialize(SafeRef<MemBuffer> buffer) const {
            return this->serialize<Bare<decltype(*this)>>(buffer);
        }
        inline bool deserialize(SafeRef<MemBuffer> buffer) {
            return this->deserialize<Bare<decltype(*this)>>(buffer);
        }*/

        /*
        template<typename T, typename U> inline static void AddSynField(U T::*member) {
            void *const synMemberPtr = static_cast<void*>(offset_of(member));
            SynSub<T>::offsets.push_back(synMemberPtr);
        }
        template<typename T>
        inline static void AddSynOffset(void *synMemberPtr) {
            SynSub<T>::offsets.push_back(synMemberPtr);
        }*/
        template<typename SELF>
        static void AddSerializer(const SerialFN& fn) {
            SynSub<SELF>::serializers.push_back(fn);
        }

        template<typename SELF>
        static void AddDeserializer(const SerialFN& fn) {
            SynSub<SELF>::deserializers.push_back(fn);
        }
    protected:

    };

    template<typename SELF, typename T, std::size_t OFF, typename NAME>
    class SynObjectChild {
    public:

        SynObjectChild() {
            //std::cout << "syn child created for " << NAME::c_str() << " at +" << OFF << "\n";

            SymbolLookupIndex lookupIns;
            lookupIns.type = SymbolType::MemberVar;
            lookupIns.name = NAME::c_str();
            lookupIns.returns = typestring<T>();
            lookupIns.arguments = typestring<SELF>();
            lookupIns.offset = OFF;

            const size_t lmOff = OFF;
            const auto serializer = [lmOff](void *const& base, SafeRef<MemBuffer> buffer) -> bool {
                T * const objPtr = reinterpret_cast<T*> (reinterpret_cast<unsigned char*> (base) + lmOff);
                return Serialize<T>(objPtr, buffer);
            };
            const auto deserializer = [lmOff](void *const& base, SafeRef<MemBuffer> buffer) -> bool {
                T * const objPtr = reinterpret_cast<T*> (reinterpret_cast<unsigned char*> (base) + lmOff);
                return Deserialize<T>(objPtr, buffer);
            };

            //SELF::SynObject::template AddSerializer<SELF>(serializer);
            //SELF::SynObject::template AddDeserializer<SELF>(deserializer);
            //std::cout << "added serializers for " << __PRETTY_FUNCTION__ << "\n";
            ::Spinster::SynSub<SELF>::serializers.push_back(serializer);
            ::Spinster::SynSub<SELF>::deserializers.push_back(deserializer);
            //std::cout << "\ttotal serializers: " << ::Spinster::SynSub<SELF>::serializers.size() << " at " << &::Spinster::SynSub<SELF>::serializers << "\n";
            SymbolLookupHandler lookupHandler;
            lookupHandler.returnSize = sizeof (T);

            RegisterSymbol(MemberID::Local(), lookupIns, lookupHandler);
        }
    };

    template<typename SELF, typename T, std::size_t OFF, typename NAME>
    class SynObjectProxy {
    public:
        //extern template struct Reflect<SELF,OFF>;

        SynObjectProxy() {
        }
    protected:
        static SynObjectChild<SELF, T, OFF, NAME> __attribute__ ((used)) _childObject;
    };
    template<typename SELF, typename T, std::size_t OFF, typename NAME>
    SynObjectChild<SELF, T, OFF, NAME> SynObjectProxy<SELF, T, OFF, NAME>::_childObject;



}

#endif /* SYNOBJECT_H */

