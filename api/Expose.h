#ifndef SPIN_EXPOSE_H
#define SPIN_EXPOSE_H
#include "SpinsterInclude.h"
#include <string>
#include <functional>
#include "MemberID.h"
#include <experimental/tuple>
#include <typeinfo>
#include "Serializers.h"
#include "SafeRef.h"

namespace Spinster {
    std::string Demangle(const char* name);

    template <class T> inline std::string typestring(const T& t) {
        _unused(t);
        return typestring<T>();
    }
    template <class T> inline std::string typestring() {
        std::string cname = __PRETTY_FUNCTION__;
        //std::cout << "TYPESTRING \"" << cname << "\"\n";
        const std::string typesHeader = "string Spinster::typestring() [with T = ";
        const auto typeStart = cname.find(typesHeader) + typesHeader.size();
        const auto typeEnd = cname.find("; ", typeStart);
        //std::cout << "for substr" << cname.substr(typeStart, typeEnd-typeStart) << " between " << typeStart << " and " << typeEnd <<"\n";
        return cname.substr(typeStart, typeEnd-typeStart);
    }
    
    enum class SymbolType : char {
            Void,
            Function,
            Variable,
            Class,
            MemberVar,
            Namespace
    };
    std::ostream& operator<<(std::ostream& out, const SymbolType& g);

    struct SymbolLookupIndex : public SynDeferred, public Common {
        std::string name = "";
        std::string returns = "";
        std::string arguments = "";
        union {
            void *address = nullptr;
            std::size_t offset;
        };
        SymbolType type = SymbolType::Void;

        inline bool operator==(const SymbolLookupIndex& o) const {
            return address == o.address && (name == o.name || name.size() == 0 || o.name.size() == 0) && 
                   arguments == o.arguments && returns == o.returns && type == o.type;
        }

        inline bool operator!=(const SymbolLookupIndex& o) const {
            return !(*this == o);
        }
        
        std::string str() const;
    };

    struct SymbolLookupHandler {
        std::function<bool(void*, void*) > invocation; //argsPtr, returnPtr
        std::function<bool(void*, void*) > constructArgs; //argsBuffer, bufferSize, outputArgsPtr
        std::function<bool(void*) > deleteArgs, deleteReturn;
        std::size_t returnSize = 0, argumentSize = 0;
        
        inline decltype(deleteArgs)& serializer() {
            return deleteArgs;
        }
        inline decltype(deleteReturn)& deserializer() {
            return deleteReturn;
        }
    };

    template<typename OP_T>
    class DeferredSymbolLookup {
    public:
        std::string symbolName;
        std::string returnString;
        std::string argsString;
        std::function<OP_T> target;
        SymIDT symbolID;

        typedef typename std::return_type<OP_T*>::type ReturnType;
        typedef typename std::function_traits<typename std::function<OP_T>>::type ArgsType;

        inline DeferredSymbolLookup() : symbolID(SymIDT_None) {

        }

        inline void registerSymbol(const OP_T *OP) {
            SymbolLookupIndex lookupIns;
            lookupIns.type = SymbolType::Function;
            lookupIns.name = symbolName;
            lookupIns.returns = returnString;
            lookupIns.arguments = argsString;
            lookupIns.address = reinterpret_cast<void*> (OP);

            SymbolLookupHandler lookupHandler;
            lookupHandler.returnSize = sizeof (ReturnType);
            lookupHandler.argumentSize = sizeof (ArgsType);
            lookupHandler.deleteReturn = [](void* ptr) -> bool {
                ReturnType *ret = reinterpret_cast<ReturnType*> (ptr);
                ret->~ReturnType();
                return true;
            };
            lookupHandler.deleteArgs = [](void* ptr) -> bool {
                ArgsType *args = reinterpret_cast<ArgsType*> (ptr);
                args->~ArgsType();
                //delete args;//This is allocated and owned by someone else, now
                return true;
            };
            lookupHandler.constructArgs = [](void* argBuffer, void *args) -> bool {
                //args = new ArgsType();
                //args = new ArgsType();
                auto *const argTuple = reinterpret_cast<ArgsType*>(args);
                auto castBuffer = SafeRef<MemBuffer>(reinterpret_cast<MemBuffer*>(argBuffer));
                //auto serArgs = std::tuple_cat(std::make_tuple(castBuffer), *argTuple);
                
                /*bool worked = std::apply([&](SafeRef<MemBuffer>buff, const ArgsType&... args) -> bool {
                    return DeserializeTo<Args...>(buff, args...);
                }, serArgs);*/
                //bool worked = std::apply(DeserializeTo<ArgsType>, serArgs);
                bool worked = DeserializeTuple(castBuffer, *argTuple);
                return worked;
                /*
                if(!worked) {
                    //delete args;
                    //args = nullptr;
                    return false;
                }*/
                //ArgsType *args = reinterpret_cast<ArgsType*>(ptr);
                //args->~ArgsType();
            };
            lookupHandler.invocation = [OP](void* argsRaw, void* returnsRaw) -> bool {
                ReturnType *ret = reinterpret_cast<ReturnType*> (returnsRaw);
                ArgsType *args = reinterpret_cast<ArgsType*> (argsRaw);

                *ret = std::experimental::apply(std::function<OP_T>(OP), *args);
                return true;
            };
            
            symbolID = RegisterSymbol(MemberID::Local(), lookupIns, lookupHandler);
            return;
        }

        inline DeferredSymbolLookup(const OP_T *OP) {
            SymIDT existingID = LookupSymbolIDAddr(reinterpret_cast<void*>(OP));
            if(existingID != SymIDT_None) {
                this->symbolID = existingID;
                return;
            }
            
            std::string cname = __PRETTY_FUNCTION__;
            try {
                //const std::string opHeader = "OP_T* OP = ";
                const std::string typesHeader = " [with OP_T = ";
                const decltype(std::string::npos) opHeader = 0;

                const auto opStart = cname.find(opHeader);
                //const auto opEnd = cname.find_last_of("]");
                symbolName = ""; //cname.substr(opStart + opHeader.size(), opEnd - opStart - opHeader.size());

                const auto typeStart = cname.find(typesHeader) + typesHeader.size();
                const auto typeEnd = cname.find_last_of(")", opStart);
                const auto retEnd = cname.find_first_of("(", typeStart + 1);

                returnString = cname.substr(typeStart, retEnd - typeStart);
                argsString = cname.substr(retEnd + 1, typeEnd - retEnd - 1);
            } catch (...) {
                symbolName = "err";
                returnString = "void";
                argsString = "";
            }
            
            this->registerSymbol(OP);
            target = OP;

            /*DBG_EXCL(std::cout << "BEGIN REG, " << returnString << " " << symbolName << " (" << argsString << ")" << "\n";
                    //std::cout << "functionRun? " << functionRun << ", pretty " << cname << "\n";
                    //std::cout << "got ptr " << reinterpret_cast<void*> (OP) << " from \"" << cname << "\""<<"\n";
                    
                    std::cout << "REG got type " << symbolName << " at " << reinterpret_cast<void*> (OP) << "\n");*/



        };
    };

    template<typename OP_T, OP_T* OP>
    class SymbolLookup : public DeferredSymbolLookup<OP_T> {
    public:
        typedef DeferredSymbolLookup<OP_T> Parent;

        inline SymbolLookup() : DeferredSymbolLookup<OP_T>() {
            SymIDT existingID = LookupSymbolIDAddr(reinterpret_cast<void*>(OP));
            if(existingID != SymIDT_None) {
                this->symbolID = existingID;
                return;
            }
            
            std::string cname = __PRETTY_FUNCTION__;
            try {
                const std::string opHeader = "OP_T* OP = ";
                const std::string typesHeader = " [with OP_T = ";

                const auto opStart = cname.find(opHeader);
                const auto opEnd = cname.find_last_of("]");
                Parent::symbolName = cname.substr(opStart + opHeader.size(), opEnd - opStart - opHeader.size());

                const auto typeStart = cname.find(typesHeader) + typesHeader.size();
                const auto typeEnd = cname.find_last_of(")", opStart);
                const auto retEnd = cname.find_first_of("(", typeStart + 1);

                Parent::returnString = cname.substr(typeStart, retEnd - typeStart);
                Parent::argsString = cname.substr(retEnd + 1, typeEnd - retEnd - 1);
            } catch (...) {
                Parent::symbolName = "err";
                Parent::returnString = "void";
                Parent::argsString = "";
            }
            
            Parent::registerSymbol(OP);
            Parent::target = OP;
            
            /*DBG_EXCL(std::cout << "BEGIN REG, " << Parent::returnString << " " << Parent::symbolName << " (" << Parent::argsString << ")" << "\n";
                    //std::cout << "functionRun? " << functionRun << ", pretty " << cname << "\n";
                    //std::cout << "got ptr " << reinterpret_cast<void*> (OP) << " from \"" << cname << "\""<<"\n";
                    std::cout << "REG got type " << Parent::symbolName << " at " << reinterpret_cast<void*> (OP) << "\n");*/

        };
    };

    template<typename OP_T, OP_T* OP>
    class SymbolLookupProxy {
    public:
        static SymbolLookup<OP_T, OP> _lookup __attribute__ ((used));
    };
    template<typename OP_T, OP_T* OP>
    SymbolLookup<OP_T, OP> SymbolLookupProxy<OP_T, OP>::_lookup;
};

namespace std {

    template <>
    struct hash<::Spinster::SymbolLookupIndex> {

        std::size_t operator()(const ::Spinster::SymbolLookupIndex& k) const noexcept {
            using std::hash;
            return ((hash<string>()(k.name)) ^ (hash<uint64_t>()(reinterpret_cast<uint64_t> (k.address))));
        }
    };

}

#define SPIN_ESC(...) __VA_ARGS__
#define SPIN_OST(a,b) a ## b
#define SPIN_PST(a,b) SPIN_OST(a,b)

#define _expose_sym_ll(sym) static ::Spinster::SymbolLookupProxy< decltype(sym), sym> SPIN_PST(_expose_, SPIN_ESC(__COUNTER__)) __attribute__((used));

#define _expose_sym(sym) _expose_sym_ll(sym)
#define _expose(...) APPLYXn(_expose_sym_ll, __VA_ARGS__);

#define _exposed __attribute__((weak)) 
#define _remote __attribute__((weak)) 


#endif /* EXPOSE_H */

