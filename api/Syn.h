#ifndef SYN_H
#define SYN_H
#include "SpinsterInclude.h"

namespace Spinster {
    //typedef SafeRef<Obj::SymbolEntry> SymbolEntry;
    namespace Obj {
        class Group;
        class Syn {
        public:
            Syn();
            virtual ~Syn();
            
            
        protected:

        };
    };
    
    typedef SafeRef<Obj::Syn> Syn;
}

#endif /* SYN_H */

