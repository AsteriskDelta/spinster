#include "Invoker.h"

namespace Spinster {
        static MemberID fakeID;

    Invoker::Invoker() {
    }

    Invoker::Invoker(const Invoker& orig) {
        _unused(orig);
    }

    Invoker::~Invoker() {
    }
    
    const MemberID& Invoker::id() const {
        DBG_EXCL(std::cout << "ret ID at addr=" << &fakeID << "\n");
        return fakeID;
    }

    SafeRef<Obj::TaskGroup> Invoker::defaultTaskGroup() {
        return nullptr;
    }
    SafeRef<Obj::TaskGroup> Invoker::reservedTaskGroup() {
        return nullptr;
    }
    SafeRef<Obj::TaskGroup> Invoker::eventTaskGroup() {
        return nullptr;
    }
};