#ifndef INVOKER_H
#define INVOKER_H
#include "SpinsterInclude.h"
#include "Invokee.h"
#include "Priority.h"
#include "MemberID.h"

namespace Spinster {
    namespace Obj {
        class TaskGroup;
    }
    
    class Invoker {
    public:
        Invoker();
        Invoker(const Invoker& orig);
        virtual ~Invoker();
        
        virtual const MemberID& id() const;
        virtual SafeRef<Obj::TaskGroup> defaultTaskGroup();
        virtual SafeRef<Obj::TaskGroup> reservedTaskGroup();
        virtual SafeRef<Obj::TaskGroup> eventTaskGroup();
        
        
        template<typename FN, typename ...Args>
        inline auto schedule(typename std::enable_if<std::is_pointer<FN>::value || !std::is_function<FN>::value, const Priority&>::type priority, const FN& f, const Args&... args) {
            //DBG_EXCL(std::cout << "Invoker at " << this << "\n");
            //DBG_EXCL(std::cout << "schedule on id " << this->id() << "\n");
            //DBG_EXCL(std::cout << f << "\n");
            auto *ret = new Obj::Invokee<FN, Args...>(this->id(), priority, f, args...);
            return Invokee<FN,Args...>(*ret);//Returns pseudo-ptr
        }
        template<typename FN, typename ...Args>
        inline auto schedule(typename std::enable_if<!std::is_pointer<FN>::value && std::is_function<FN>::value, const Priority&>::type  priority, const FN& f, const Args&... args) {
            return this->schedule(priority, &f, args...);
        }
        
        template<typename FN, typename ...Args>
        inline auto invoke(const FN& f, const Args&... args) {
            //DBG_EXCL(std::cout << "invoke at " << this << "\n");
            return this->schedule(Priority::Invoked(), f, args...);
        }
        template<typename FN, typename ...Args>
        inline auto thread(const FN& f, const Args&... args) {
            return this->schedule(Priority::Loop(), f, args...)->taskGroup(this->reservedTaskGroup());
        }
    private:

    };
};

#endif /* INVOKER_H */

