#ifndef SYNTYPE_H
#define SYNTYPE_H
#include "SpinsterInclude.h"
#include <vector>
#include "Expose.h"
#include "SafeRef.h"

#define _declare_sym_ll(GRP,sym) \
__attribute__((used)) __attribute((weak)) void SPIN_PST(_share,SPIN_ESC(__COUNTER__))(\
 ::Spinster::SynTypeProxy<GRP, decltype(GRP::sym), offset_of<GRP, decltype(GRP::sym), &GRP::sym>(), CT_STR(QUOTE(sym))>){};\

#define _declare_sym(GRP, sym) _share_sym_ll(GRP,sym)
#define _declare(TYPE) APPLYPXn(_declare_sym,TYPE);

namespace Spinster {
    
};

#endif /* SYNTYPE_H */

