#include "Invocation.h"
#include "SymbolTable.h"
#include "Invokee.h"
#include "MemberLink.h"
#include "Message.h"
#include "Expose.h"
#include "LocalGroup.h"
#include "Priority.h"
#include "Protocol.h"

namespace Spinster {
    bool HandleSymInvocation(SafeRef<Obj::SymbolEntry> sym, SafeRef<Message> msg) {
        std::cout << "handling sym invocation for " << sym << " from message of dataSize=" << msg->data->size() << "\n";
        static thread_local MemBuffer argBuffer = MemBuffer(), retBuffer = MemBuffer();
        argBuffer.ensure(sym->handler.argumentSize);
        retBuffer.ensure(sym->handler.returnSize);
        
        bool success = true;
        if(msg->dataPtr() == nullptr) return false;
        
        bool immortal = msg->priority().immortal();
        do {
            argBuffer.clear(); retBuffer.clear();

            if(sym->handler.argumentSize > 0) {
                //DBG_EXCL(std::cout << "constructing args using fn " << "\n");
                const void *const dPtr = reinterpret_cast<const void*>(msg->data.get());
                success &= sym->handler.constructArgs(const_cast<void*>(dPtr), reinterpret_cast<void*>(argBuffer.data));
            }
            //DBG_EXCL(std::cout << "invoking..., constructedArgs? " << success << "\n");
            if(success) success &= sym->handler.invocation(reinterpret_cast<void*>(argBuffer.data), reinterpret_cast<void*>(retBuffer.data));
            else std::cerr << "\tError constructing invocation arguments\n";
            if(success) retBuffer.skip(sym->handler.returnSize);

            //Copy retBuffer to the message we'll send back (if any)

            if(success) {
                //DBG_EXCL(std::cout << "SUCCESS, returning and deleting return\n");
                ReturnSymInvocation(msg, &retBuffer);
                sym->handler.deleteReturn(reinterpret_cast<void*>(retBuffer.data));
            } else ReturnSymInvocation(msg, nullptr);

            //DBG_EXCL(std::cout << "deleting args...\n");
            sym->handler.deleteArgs(reinterpret_cast<void*>(argBuffer.data));
        } while(success && immortal && msg->nextFrame());
        
        if(!success) {
            std::cerr << "HandleSymInvocation FAILED for symbol" << sym << "\n";
        }
        
        return success;
    }
    
    void ReturnSymInvocation(SafeRef<Message> msg, SafeRef<MemBuffer> outputBuffer) {
        //SafeRef<MemberLink> link = msg->owner->parent;
        if(msg->priority().immortal()) {
            //Continuous, no reply needed
        } else if(!msg->priority().expectsReply()) {
            msg->owner->returned(msg->id());
        } else {
            msg->owner->reply(&msg, outputBuffer);//Copies the output buffer
        }
    }
    
    bool QueueSymInvocation(SafeRef<Obj::SymbolEntry> sym, SafeRef<Message> msg) {
        //std::cout << "Queueing for msg at " << &msg << ", size=" << msg->data->size() << "\n";
        //Group target = msg->to().resolve();
        //bool possible = target && target->symbols();
        //if(!possible) return false;
        
        //if(target->symbols()->knows(msg->symbolID())) {
            Local->invoke(HandleSymInvocation, sym, msg)->fromMessage(&msg)->symbol(&sym);
        /*} else  {
            target->invoke(Protocol::Sym::SymLookup, index)->onReturn([sym,msg,target](SymIDT extID) -> bool {
                target->symbols()->;
                msg->symbolID() = extID;
                target->invoke
                return true;
            });
        }*/
        return true;
    }
    
    _expose(HandleSymInvocation);
};

