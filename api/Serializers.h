#ifndef SERIALIZERS_H
#define SERIALIZERS_H
#include "SpinsterInclude.h"
#include "MemBuffer.h"
#include <cstring>
#include <vector>
#include <string>
#include <tuple>
#include <utility>

#define _Args(...) __VA_ARGS__
#define STRIP_PARENS(X) X
#define PASS_PARAMETERS(X) STRIP_PARENS( _Args X )

//G++ expression statements FTW
#define SPEC_STRUCT(NAME, ATTR, CONDITION,...) \
template <typename T>\
struct NAME <T, typename std::enable_if< PASS_PARAMETERS(CONDITION) >::type> {\
    bool status = false;\
    inline NAME (ATTR typename std::remove_reference<T>::type * const & obj, SafeRef<MemBuffer> &buffer) {\
        status = ({( __VA_ARGS__ );});\
    }\
    inline operator const bool&() const {\
        return status;\
    }\
};

#define SERIALIZER(CONDITION,...) SPEC_STRUCT(Serialize, const, (PASS_PARAMETERS(CONDITION) && !std::is_reference<T>::value), __VA_ARGS__)
#define DESERIALIZER(CONDITION,...) SPEC_STRUCT(Deserialize, , (PASS_PARAMETERS(CONDITION) && !std::is_reference<T>::value), __VA_ARGS__)
#define LLSERIALIZER(CONDITION,...) SPEC_STRUCT(Serialize, const, CONDITION, __VA_ARGS__)
#define LLDESERIALIZER(CONDITION,...) SPEC_STRUCT(Deserialize, , CONDITION, __VA_ARGS__)

#define MEMCOPYABLE(T) ((std::is_fundamental<T>::value || std::is_trivial<T>::value || std::is_base_of<::Spinster::MCopyable, T>::value) && !std::is_pointer<T>::value)

namespace Spinster {
    class Syn;
    class MemBuffer;
    class SynObject;

    template <typename T, class Enable = void> struct Serialize {
    };

    template <typename T, class Enable = void> struct Deserialize {
    };
    /*
        template <typename T>
        struct Serialize<T, typename std::enable_if<std::is_pointer<Bare<T>>::value>::type>
        {

            inline bool operator()(const T * const & obj, SafeRef<MemBuffer> &buffer) {
                return ::Spinster::Serialize<uint64_t>(reinterpret_cast<uint64_t*> (obj), buffer);
            }
        };

        //Strings

        template <typename T>
        struct Serialize<T, typename std::enable_if<std::is_same<Bare<T>, std::string>::value>::type> {
            bool status = false;

            inline Serialize(const T * const & obj, SafeRef<MemBuffer> &buffer) {
                status = buffer->write(obj->c_str(), (obj->size() + 1) * sizeof (typename T::value_type));
            }

            inline operator bool() const {
                return status;
            }
        };*/
    //Deconst
    SERIALIZER((std::is_const<T>::value), {
        Serialize(&const_cast<typename std::add_lvalue_reference<Bare<T>>::type>(obj), buffer);
    });
    
    //References
    LLSERIALIZER((std::is_reference<T>::value), {
        Serialize(obj, buffer);
    });
    LLDESERIALIZER((std::is_reference<T>::value), {
        auto *const ptrStart = reinterpret_cast<char*>(obj);
        auto *const ptrDest = ptrStart + sizeof(char*);
        std::cout << "DESER REF start=" << reinterpret_cast<int*>(ptrStart) << ", dest=" << reinterpret_cast<int*>(ptrDest) << "\n";
        const bool ret = Deserialize<Bare<T>>(reinterpret_cast<Bare<T>*>(ptrDest), buffer);
        memcpy(ptrStart, &ptrDest, sizeof(ptrStart));
        ret;
    });
    
    //Fundamental
    SERIALIZER((MEMCOPYABLE(T)), {
        buffer->write(obj, sizeof(T));
    });
    DESERIALIZER((MEMCOPYABLE(T)), {
        const bool ret = buffer->read(obj, sizeof(T));
        //std::cout << "Deser fundamental: " << *obj << " got " << ret << "\n";
        ret;
    });
    
    //Pointer
    SERIALIZER((std::is_pointer<Bare<T>>::value), {
        buffer->write(obj, sizeof(T));
    });
    DESERIALIZER((std::is_pointer<Bare<T>>::value), {
        buffer->read(obj, sizeof(T));
    });
    
    //Serializable
    SERIALIZER((std::is_base_of<SynObject, T>::value), {
        const_cast<T*>(obj)->template serialize<T>(buffer);
    });
    DESERIALIZER((std::is_base_of<SynObject, T>::value), {
        const_cast<T*>(obj)->template deserialize<T>(buffer);
    });
    
    //Deferred serialization
    SERIALIZER((std::is_base_of<SynDeferred, T>::value), {
        //const_cast<T*>(obj)->template serialize<T>(buffer);
        bool ret = true;
        //std::cout << __PRETTY_FUNCTION__ << "\n";
        //std::cout << "\tbegin deferred serialization, buffer sz = " << buffer->size() << ", handlers: " <<  SynSub<Bare<T>>::serializers.size() << "\n\n";
        //std::cout << "\ttotal serializers: " << ::Spinster::SynSub<T>::serializers.size() << " at " << &::Spinster::SynSub<T>::serializers << "\n";
        //sleep(1);
        for (auto& fn : SynSub<Bare<T>>::serializers) {
            ret &= fn(const_cast<void*>(reinterpret_cast<const void*>(obj)), buffer);
            //std::cout << "\tinvoked serializer fn, got buffer sz=" << buffer->size() << "\n";
        }
        ret;
    });
    DESERIALIZER((std::is_base_of<SynDeferred, T>::value), {
        bool ret = true;
        //std::cout << __PRETTY_FUNCTION__ << "\n";
        for (auto& fn : SynSub<Bare<T>>::deserializers) {
            ret &= fn(const_cast<void*>(reinterpret_cast<const void*>(obj)), buffer);
            std::cout << "\tinvoked deserializer fn, got buffer remain=" << buffer->remaining() << "\n";
        }
        ret;
    });
    
    //SafeRefs
    SERIALIZER((std::is_base_of<SafeRef_Root, T>::value), {
        //typename Bare<T>::value_type* objPtr = obj->operator*();
        const decltype(obj->operator&()) objPtr = obj->operator&();
        Serialize<decltype(obj->operator&())>(&objPtr, buffer);
    });
    DESERIALIZER((std::is_base_of<SafeRef_Root, T>::value), {
        decltype(obj->operator&()) objPtr = nullptr;
        const bool ret = Deserialize<decltype(obj->operator&())>(&objPtr, buffer);
        obj->operator=(objPtr);
        ret;
    });
    
    //MemBuffers
    SERIALIZER((std::is_base_of<MemBuffer, T>::value), {
        unsigned int bufferLen = obj->size();
        Serialize<decltype(bufferLen)>(&bufferLen, buffer);
        buffer->write(obj->data, bufferLen);
    });
    DESERIALIZER((std::is_base_of<MemBuffer, T>::value), {
        unsigned int bufferLen = 0;
        bool ret = false;
        //std::cout << "writing mbuff to " << obj << "\n";
        new(obj) Bare<T>();
        
        Deserialize<decltype(bufferLen)>(&bufferLen, buffer);
        //std::cout << "deserialize mbuff of sz=" << bufferLen << " from packetBuff with " << buffer->remaining() << " bytes left\n";
        if(bufferLen > 0) {
            if(bufferLen > 1024*1024) return;
            //obj->clear();
            obj->resize(bufferLen);
            ret = buffer->read(obj->data, bufferLen);
        } else {
            ret = true;
        }
        ret;
    });
    
    //Strings
    SERIALIZER((std::is_same<T, std::string>::value), {
        buffer->write(obj->c_str(), (obj->size() + 1) * sizeof (typename T::value_type));
    });
    DESERIALIZER((std::is_same<T, std::string>::value), {
        new(obj) Bare<T>();
        const size_t strSize = strnlen(buffer->reading<char>(), buffer->remaining());
        obj->resize(strSize);
        bool ret = buffer->read(&((*obj)[0]), (strSize+1) * sizeof(typename T::value_type));
        //std::cout << "deserial string \"" << *obj << "\"\n";
        ret;
    });
    
    //Vectors [trivial]
    SERIALIZER((std::is_same<T, std::vector<typename T::value_type>>::value
    && MEMCOPYABLE(typename T::value_type)), {
        const std::size_t sz = obj->size();
        const bool ret = Serialize<decltype(sz)>(&sz, buffer);
        ret && buffer->write(&((*obj)[0]), obj->size() * sizeof(typename T::value_type));
    });
    DESERIALIZER((std::is_same<T, std::vector<typename T::value_type>>::value
    && MEMCOPYABLE(typename T::value_type)), {
        size_t sz;
        if(!Deserialize<decltype(sz)>(&sz, buffer)) return;
        new(obj) Bare<T>();
        obj->resize(sz);
        buffer->read(&((*obj)[0]), sz * sizeof(typename T::value_type));
    });
    
    //Vectors [nontrivial]
    SERIALIZER((std::is_same<T, std::vector<typename T::value_type>>::value
    && !MEMCOPYABLE(typename T::value_type)), {
        std::size_t sz = obj->size();
        bool ret = Serialize<decltype(sz)>(&sz, buffer);
        for(std::size_t i = 0; i < obj->size(); i++) {
            ret &= Serialize(&obj[i], buffer);
        }
        ret;
    });
    DESERIALIZER((std::is_class<T>::value && std::is_same<T, std::vector<typename T::value_type>>::value
    && !MEMCOPYABLE(typename T::value_type)), {
        size_t sz;
        if(!Deserialize<decltype(sz)>(&sz, buffer)) return;
        new(obj) Bare<T>();
        
        bool ret = true;
        obj->resize(sz);
        for(std::size_t i = 0; i < obj->size(); i++) {
            ret &= Deserialize(&obj[i], buffer);
        }
        ret;
    });
    /*
    template<typename T>
    bool Serialize(T *const& obj, SafeRef<MemBuffer> buffer);
    template<typename T>
    bool Deserialize(T *const& obj, SafeRef<MemBuffer> buffer);
     */

    /*
    template<typename T, bool Enable = true>
    inline typename std::enable_if<std::is_pointer<Bare<T>>::value && Enable, bool>::type Serialize(T *const& obj, SafeRef<MemBuffer> buffer) {
        //For now, just copy the pointer
        return Serialize<uint64_t>(reinterpret_cast<uint64_t*>(obj), buffer);
        //return Serialize<decltype(*obj)>(*obj);
    }
    template<typename T>
    inline bool Serialize(T & obj, SafeRef<MemBuffer> buffer, 
            typename std::enable_if<std::is_const<T>::value, const T *const&>::type = 0x0) {
        return Serialize(&const_cast<typename std::add_lvalue_reference<Bare<T>>::type>(obj), buffer);
    }
    
    template<typename T>
    inline bool Serialize(const T *const& obj, SafeRef<MemBuffer> buffer, 
            typename std::enable_if<MEMCOPYABLE(T), T *const&>::type = 0x0) {
        return buffer->write(obj, sizeof(T));
    }
    template<typename T>
    inline bool Deserialize(T *const& obj, SafeRef<MemBuffer> buffer, 
            typename std::enable_if<MEMCOPYABLE(T), T *const&>::type = 0x0) {
        return buffer->read(obj, sizeof(T));
    }
    
    template<typename T>
    inline bool Serialize(T *const& obj, SafeRef<MemBuffer> buffer, 
            typename std::enable_if<!std::is_same<void, decltype(&T::serialize)>::value, T *const&>::type = 0x0) {
        return obj->serialize(buffer);
    }
    template<typename T>
    inline bool Deserialize(T *const& obj, SafeRef<MemBuffer> buffer, 
            typename std::enable_if<!std::is_same<void, decltype(&T::serialize)>::value, T *const&>::type = 0x0) {
        return obj->deserialize(buffer);
    }
    
    //Strings
    template<typename T>
    inline bool Serialize(T *const& obj, SafeRef<MemBuffer> buffer, 
            typename std::enable_if<std::is_same<Bare<T>, std::string>::value, T *const&>::type = 0x0) {
        return buffer->write(obj->c_str(), (obj->size()+1) * sizeof(typename T::value_type));
    }
    template<typename T>
    inline bool Deserialize(T *const& obj, SafeRef<MemBuffer> buffer, 
            typename std::enable_if<std::is_same<Bare<T>, std::string>::value, T *const&>::type = 0x0) {
        const size_t strSize = strnlen(buffer->reading<char>(), buffer->remaining());
        obj->resize(strSize);
        return buffer->write(&((*obj)[0]), (strSize) * sizeof(typename T::value_type));
    }
    
    //Vector serialization
    template<typename T>
    inline bool Serialize(T *const& obj, SafeRef<MemBuffer> buffer, 
            typename std::enable_if<std::is_same<T, std::vector<typename T::value_type>>::value
    && MEMCOPYABLE(typename T::value_type), T *const&>::type = 0x0) {
        const std::size_t sz = obj->size();
        const bool ret = Serialize<decltype(sz)>(&sz, buffer);
        return ret && buffer->write(&((*obj)[0]), obj->size() * sizeof(typename T::value_type));
    }
    
    template<typename T>
    inline bool Serialize(T *const& obj, SafeRef<MemBuffer> buffer, 
            typename std::enable_if<std::is_class<T>::value && std::is_same<T, std::vector<typename T::value_type>>::value
    && !MEMCOPYABLE(typename T::value_type), T *const&>::type = 0x0) {
        std::size_t sz = obj->size();
        bool ret = Serialize<decltype(sz)>(&sz, buffer);
        for(std::size_t i = 0; i < obj->size(); i++) {
            ret &= Serialize(&obj[i], buffer);
        }
        return ret;
    }
    template<typename T>
    inline bool Deserialize(T *const& obj, SafeRef<MemBuffer> buffer, 
            typename std::enable_if<std::is_same<T, std::vector<typename T::value_type>>::value
    && MEMCOPYABLE(typename T::value_type), T *const&>::type = 0x0) {
        size_t sz;
        if(!Deserialize<decltype(sz)>(&sz, buffer)) return false;
        obj->resize(sz);
        return buffer->read(&((*obj)[0]), sz * sizeof(typename T::value_type));
    }
    
    template<typename T>
    inline bool Deserialize(T *const& obj, SafeRef<MemBuffer> buffer, 
            typename std::enable_if<std::is_class<T>::value && std::is_same<T, std::vector<typename T::value_type>>::value
    && !MEMCOPYABLE(typename T::value_type), T *const&>::type = 0x0) {
        size_t sz;
        if(!Deserialize<decltype(sz)>(&sz, buffer)) return false;
        
        bool ret = true;
        obj->resize(sz);
        for(std::size_t i = 0; i < obj->size(); i++) {
            ret &= Deserialize(&obj[i], buffer);
        }
        return ret;
    }
    
    //Remote pointer construction TODO
    template<typename T>
    inline bool Serialize(T *const& obj, SafeRef<MemBuffer> buffer, 
            typename std::enable_if<std::is_same<T, SafeRef<typename T::value_type>>::value, T *const&>::type = 0x0) {
        return Serialize<typename T::value_type>(&(*obj));
    }
    template<typename T>
    inline bool Deserialize(T *const& obj, SafeRef<MemBuffer> buffer, 
            typename std::enable_if<std::is_same<T, SafeRef<typename T::value_type>>::value, T *const&>::type = 0x0) {
        return Deserialize<typename T::value_type>(&(*obj));
    }*/

    template<class = void>
    inline bool SerializeTo(SafeRef<MemBuffer> buffer) {
        _unused(buffer);
        return true;
    }

    template<typename T>
    inline bool SerializeTo(SafeRef<MemBuffer> buffer, const T& obj) {
        return Serialize<Bare<T/*decltype(*(&obj))*/>>(std::addressof(obj), buffer);
    }

    template<typename T, typename U, typename ...Args>
    inline bool SerializeTo(SafeRef<MemBuffer> buffer, const T& obj, const U& obj2, const Args&... args) {
        return Serialize<Bare<T/*decltype(*(&obj))*/>>(std::addressof(obj), buffer) && SerializeTo(buffer, obj2, args...);
    }
    
    template<class = void>
    inline bool DeserializeTo(SafeRef<MemBuffer> buffer) {
        _unused(buffer);
        return true;
    }
    
    template<typename T, typename K>
    Bare<T>* addrof(K&& field) {
        if constexpr(std::is_reference<T>::value) {
            struct Helper {
                uint64_t *start;
                K&& v;
                uint64_t *end;
            };
            volatile Helper helper{(uint64_t*)(1), field, (uint64_t*)(3)};
            ((void)helper.v);
            std::cout << "Helper addr= " << &helper.start << ", sizeof=" << sizeof(Helper) << "\n";
            Bare<T>* ret;
            for(size_t i = 0; i < 3; i++) {
                std::cout << "\thelper " << i << ": " << *(&helper.start + i) << "\n";
            }
            //memcpy(&ret, const_cast<void*>(reinterpret_cast<volatile void*const>(&helper)), sizeof(uint64_t*));
            
            memcpy(&ret, const_cast<uint64_t*>(*((&helper.start) + 1)), 8);
            std::cout << "helper retrieval: " << reinterpret_cast<int*>(ret) << "\n";
            return ret;
            //const char *off = *reinterpret_cast<const char*const*>(&helper);
            //return reinterpret_cast<Bare<T>*>(const_cast<char*>(off));
        } else {
            return const_cast<Bare<T>*>(std::addressof(field));
        }
    }

    template<typename T>
    inline bool DeserializeTo(SafeRef<MemBuffer> buffer, T& obj) {
        //std::cout << "\taddrof: " << addrof<T>(obj) << "\n";
        return Deserialize<T/*decltype(*(&obj))*/>(addrof<T>(obj), buffer);
    }

    template<typename T, typename U, typename ...Args>
    inline bool DeserializeTo(SafeRef<MemBuffer> buffer, T& obj, U& obj2, Args&... args) {
        //std::cout << "\taddrof: " << addrof<T>(obj) << "\n";
        return Deserialize<T/*decltype(*(&obj))*/>(addrof<T>(obj), buffer) && DeserializeTo<U, Args...>(buffer, obj2, args...);
    }
    
    template<typename ...T>
    inline bool DeserializeTuple(SafeRef<MemBuffer> buffer, std::tuple<T...> &tup) {
        //return DeserializeTo<T...>(buffer, tup...);
        //auto args = std::tuple_cat(std::forward_as_tuple(buffer), tup);
        //DBG_EXCL(std::cout << "Deserializing tuple from buffer sz=" << buffer->size() << "\n");
        bool worked = std::apply([&](T&... lmArgs) -> bool {
            return DeserializeTo<T...>(buffer, lmArgs...);
        }, tup);
        //if constexpr(std::tuple_size<Bare<decltype(tup)>>::value > 0) std::cout << "tup[0] = " << std::get<0>(tup) << "\n";
        return worked;
        //return std::apply(DeserializeTo<T...>, args);
    }
    /*
    template<typename T, typename K = std::enable_if<std::is_trivially_copyable<T>::value, void>>
    inline bool Serialize(T *const& obj, SafeRef<MemBuffer> buffer) {
        return buffer->write(obj, sizeof(T));
    }*/
    //template<typename T> 
    //bool Deserialize(T *const& obj, SafeRef<MemBuffer> buffer);
    /*
    template<typename T>
    bool Serialize(T *const& obj, SafeRef<MemBuffer> buffer);
    template<typename T>
    bool Deserialize(T *const& obj, SafeRef<MemBuffer> buffer);*/
}

#endif /* SERIALIZERS_H */

