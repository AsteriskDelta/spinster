#ifndef REMOTE_H
#define REMOTE_H
#include "SpinsterInclude.h"
#include "Resolver.h"
#include "Defer.h"
#include "MemberID.h"

namespace Spinster {

    template<typename T>
    class Remote : public Defer<T, RemoteResolver<T>> {
    public:
        Remote();
        Remote(const Remote& orig);
        ~Remote();
        
        MemberID owner;
        SymIDT symbol;
        union {
        struct {//8 bytes for transfer settings and state
            bool dirty : 1;
            bool lazy : 1;
            bool cache : 1;
            int padd : 5;
            uint64_t time : 56;
        } state;
        uint64_t rawState;
        };
    private:

    };
}

#endif /* REMOTE_H */

