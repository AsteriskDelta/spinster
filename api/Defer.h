#ifndef DEFER_H
#define DEFER_H
#include "SpinsterInclude.h"
#include "Resolver.h"

namespace Spinster {

    template<typename T, typename RSV = Resolver<T>>
    class Defer {
    public:
        typedef Defer<T, RSV> Self;
        typedef uint64_t Idt;
        inline static constexpr Idt FlagMask = ~Idt(0x1);
        typedef RSV Resolver_t;
        
        inline Defer() : dat{0x0, false} {};
        inline Defer(const Idt& nID) : Defer<T, RSV>() {
            this->setID(nID);
        }
        inline Defer(T *const& ptr) : Defer<T, RSV>() {
            this->setPtr(ptr);
        };
        inline Defer(const Defer& orig) : dat(orig.dat) {
            if(this->resolved()) this->reserve();
        }
        inline ~Defer() {
            if(resolved()) this->release();
        }
        
        mutable struct {
            Idt raw : 63;
            bool resolved : 1;
        } dat;
        
        inline Idt id() const {
            if(!resolved()) return this->dat.raw & FlagMask;
            else return this->ptr()->id();
        }
        inline void setID(const Idt& newID) {
            if(resolved()) this->release();
            dat.raw = newID;
        }
        inline void setPtr(T *const& ptr, bool existing = false) {
            if(resolved()) this->release();
            dat.raw = reinterpret_cast<Idt>(ptr);
            if(!existing) this->reserve();
        }
        
        inline operator T*() const {
            if(resolved()) return reinterpret_cast<T*>(
                (*reinterpret_cast<uint64_t*>(&dat)) & uint64_t(FlagMask)
                );
            else return const_cast<Self*>(this)->resolve();
        }
        inline T* ptr() const {
            return operator T*();
        }
        
        inline T* resolve() {
            T *const nPtr = Resolver_t::Execute(this->id());
            if(nPtr == nullptr) return nullptr;
            else {
                this->setPtr(nPtr, true);
            }
        }
        inline void reserve() {
            this->setPtr(Resolver_t::Reserve(this->id()));
            dat.resolved = true;
        }
        inline void release() {
            //dat.raw = this->id();
            //dat.resolved = false;
            dat.raw = Resolver_t::Release(*this);
            dat.resolved = false;
        }
        
        inline bool resolved() const {
            return dat.resolved;
        }
        inline explicit operator bool() const {
            return dat.raw != 0x0;
        }

        inline T* operator&() const {
            return this->ptr();
        };
        inline T* operator->() const {
            return this->ptr();
        };
        inline T& operator*() const {
            return *this->ptr();
        };
        inline bool operator==(const Self& o) const {
            return id() == o.id();
        }
        inline bool operator!=(const Self& o) const {
            return !(*this == o);
        }
    private:

    };
};

#endif /* DEFER_H */

