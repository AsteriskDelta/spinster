#ifndef HANDLER_H
#define HANDLER_H
#include "SpinsterInclude.h"
#include "Invoker.h"

namespace Spinster {
    namespace Obj {

        class Handler : public Invoker {
        public:
            inline Handler() {
                
            }
            inline Handler(const Handler& orig) : Invoker(orig) {
                
            }
            inline ~Handler() {
                
            }


        private:

        };
    }

    class Handler : public SafeRef<Obj::Handler> {
    public:
        typedef SafeRef<Obj::Handler> Parent;

        inline Handler() : Parent() {
        };
        using Parent::Parent;
    };
    SPINSTER_SAFEREF_NULL(Handler);
};

#endif /* HANDLER_H */

