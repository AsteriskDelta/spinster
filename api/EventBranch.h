#ifndef EVENT_H
#define EVENT_H
#include "SpinsterInclude.h"
#include "Invokee.h"
#include "SynObject.h"
#include "LocalGroup.h"
#include <functional>
#include <tuple>

namespace Spinster {
    class MemBuffer;
    
    //Event DispatchEvent(Obj::Event *evObj, SafeRef<MemBuffer>);
    namespace Obj {
    template<typename ...Args>
        class EventWrapper;
    };
    template<typename ...Args> using Event = SafeRef<Obj::EventWrapper<Args...>>;
    template<typename ...Args> using EventInternalFN = std::function<Event<Args...> (decltype(Event<Args...>::obj) ,void*)>;
    template<typename ...Args> using EventHandler = std::function<void(Event<Args...> )>;
    
    template<typename T>
    inline SafeRef<Obj::Event<T>> DispatchEvent(Obj::Event<T> *evObj, SafeRef<MemBuffer> data) {
        if(evObj == nullptr) return nullptr;
        
        if(!evObj->deserialize(data)) {
            //TODO: handle err
        }
        return evObj;
    }
    
    namespace Obj {
        struct Empty{};
        struct EventDataRoot{};
        
        template<class DATA_T>
        class Event<DATA_T> ://*, typename std::enable_if<std::is_class<DATA_T>::value, void>::type*/> : 
                public Invokee<decltype(&DispatchEvent<DATA_T>), Event<DATA_T>*, SafeRef<MemBuffer>>, 
                public DATA_T {
        public:
            typedef Invokee<decltype(&DispatchEvent<DATA_T>), Event<DATA_T>*, SafeRef<MemBuffer>> Parent;
            /*Event();
            Event(const Event& orig);
            virtual ~Event();*/
           
            
            inline Event() : Parent(), DATA_T() {
                this->init();
            }

            inline Event(const Event<DATA_T>& orig) : Parent(orig), DATA_T(orig) {
                this->init();
            }
            
            inline Event(const DATA_T& dat) : Parent(), DATA_T(dat) {
                this->init();
            };

            inline virtual ~Event() {
            }
            
            _shared();
        private:
            inline void init() {
                Parent::func = DispatchEvent;
                std::get<0>(Parent::data) = this;
                Parent::taskGroup(Local->eventTaskGroup());
                Parent::persist();
            }
        };
        
        //template<typename ...T>
        //using EventData = std::tuple<T...>;
        
        template<typename ...T>
        struct EventData : public std::tuple<T...>, public SynObject, public EventDataRoot {
            
        };
        template<> struct EventData<> : public SynObject, public EventDataRoot {};
        
        template<typename ...Args>
        class EventWrapper ://<A, Args.../*, typename std::enable_if<!std::is_class<A>::value || (sizeof...(Args) != 0), void>::type */> : 
        public Event<EventData<Args...>> {
        public:
            typedef Event<EventData<Args...>> Parent;
            EventWrapper(const Args&... args) : Parent(EventData<Args...>{args...}) {
                
            }
            EventWrapper() : Parent() {};
            inline void emit() {
                this->submit();
            }
            template<class=void>
            inline void emit(const Args&... args) {
                (*static_cast<std::tuple<Args...>*>(this)) = {args...};
                this->submit();
            }
        };
    }
    template<typename ...T>
    using EventData = Obj::EventData<T...>;
};

#endif /* EVENT_H */

