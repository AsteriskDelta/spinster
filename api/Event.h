#ifndef SPIN_EVENT_H
#define SPIN_EVENT_H
#include "SpinsterInclude.h"
#include "Invokee.h"
#include "SynObject.h"
#include <functional>
#include <tuple>

namespace Spinster {
    class MemBuffer;

    //Event DispatchEvent(Obj::Event *evObj, SafeRef<MemBuffer>);

    template<typename ...Args> using Event = Obj::Event<Args...>;//SafeRef<Obj::Event<Args...>>;
    template<typename ...Args> using EventInternalFN = std::function<Event<Args...>* (Obj::Event<Args...> *,void*)>;
    template<typename ...Args> using EventHandler = std::function<void(Event<Args...>* )>;

    template<typename T>
    inline Event<T>* DispatchEvent(Event<T> *evObj, SafeRef<MemBuffer> data) {
        if(evObj == nullptr) return nullptr;

        if(!evObj->deserialize(data)) {
            //TODO: handle err
        }
        return evObj;
    }

    template<typename T>
    inline void HandledEvent(Event<T> *event) {
        if(!event) return;
        event->free();
    }

    namespace Obj {
        struct Empty{};

        template<class DATA_T>
        class Event<DATA_T/*, typename std::enable_if<std::is_class<DATA_T>::value, void>::type*/> :
        public Invokee<decltype(&DispatchEvent<DATA_T>), Event<DATA_T>*, SafeRef<MemBuffer>>, public DATA_T {
        public:
            typedef Invokee<decltype(&DispatchEvent<DATA_T>), Event<DATA_T>*, SafeRef<MemBuffer>> Parent;
            /*Event();
            Event(const Event& orig);
            virtual ~Event();*/


            inline Event() : Parent(), DATA_T() {
                this->init();
            }

            inline Event(const Event<DATA_T>& orig) : Parent(orig), DATA_T(orig) {
                this->init();
            }

            inline Event(const DATA_T& dat) : Parent(), DATA_T(dat) {
                this->init();
            };

            inline virtual ~Event() {
            }

            using Parent::operator();
            inline Event* operator()() {
                this->emit();
                return this;
            }

            inline void release() {
                released = true;
                if(this->outstanding <= 0) delete this;
            }

            inline void use() {
                outstanding++;
            }
            inline void free() {
                outstanding--;
                if(this->released && outstanding <= 0) delete this;
            }

            template<class=void>
            inline auto& value() {
                return *static_cast<DATA_T*>(this);
            }
            template<unsigned int i>
            inline auto& value() {
                return std::get<i>(*static_cast<DATA_T*>(this));
            }

            _shared();
        protected:
            int outstanding;
            bool released;

            inline void init() {
                outstanding = 0;
                released = false;

                Parent::func = DispatchEvent<DATA_T>;
                std::get<0>(Parent::data) = this;
                //Parent::taskGroup(Local->eventTaskGroup());
                Parent::persist();
                Parent::priority.noReply();
                Parent::on(&HandledEvent<DATA_T>);
            }
        };

        //template<typename ...T>
        //using EventData = std::tuple<T...>;

        template<typename ...T>
        struct EventData : public std::tuple<T...>, public SynObject {

        };
        template<> struct EventData<> : public SynObject {};

        template<typename A, typename ...Args>
        class Event<A, Args.../*, typename std::enable_if<!std::is_class<A>::value || (sizeof...(Args) != 0), void>::type */> :
        public Event<EventData<A, Args...>> {
        public:
            inline void emit() {
                this->use();
                this->submit();
            }
            template<class=void>
            inline void emit(const A& a, const Args&... args) {
                (*static_cast<std::tuple<A, Args...>*>(this)) = {a, args...};
                this->use();
                this->submit();
            }
        };
    }
    template<typename ...T>
    using EventData = Obj::EventData<T...>;

    //using Obj::Event;
};

#endif /* EVENT_H */
