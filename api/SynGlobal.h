#ifndef SYNGLOBAL_H
#define SYNGLOBAL_H
#include "SpinsterInclude.h"
#include <vector>
#include "Expose.h"
#include "SafeRef.h"
#include <cstddef>
#include <utility>
#include <functional>

#include "Serializers.h"

#define _global_sym_ll(sym) \
__attribute__((used)) __attribute((weak)) void SPIN_PST(_global,SPIN_ESC(__COUNTER__))(\
 ::Spinster::SynGlobalProxy<decltype(sym), &sym, CT_STR(QUOTE(sym))>){};\

#define _global_sym(sym) _global_sym_ll(sym)
#define _global( ...) APPLYXn(_global_sym, __VA_ARGS__);
    
namespace Spinster {
    
    template<typename T, T* PTR, typename NAME>
    class SynGlobalChild {
        public:
            SynGlobalChild() {
                //std::cout << "syn child created for " << NAME::c_str() << " at +" << OFF << "\n";
                
                SymbolLookupIndex lookupIns;
                lookupIns.type = SymbolType::Variable;
                lookupIns.name = NAME::c_str();
                lookupIns.returns = typestring<T>();
                lookupIns.address = PTR;
                
                T *const lmAddr = PTR;
                const auto serializer = [lmAddr](void *const& base) -> bool {
                    SafeRef<MemBuffer> buffer = reinterpret_cast<MemBuffer*>(base);
                    T *const objPtr = reinterpret_cast<T*>(lmAddr);
                    return Serialize<T>(objPtr, buffer);
                };
                const auto deserializer = [lmAddr](void *const& base) -> bool {
                    SafeRef<MemBuffer> buffer = reinterpret_cast<MemBuffer*>(base);
                    T *const objPtr = reinterpret_cast<T*>(lmAddr);
                    return Deserialize<T>(objPtr, buffer);
                };
                const auto getPtr = [lmAddr](void * nullArgs, void * ptrDest) -> bool {
                    _unused(nullArgs);
                    (*reinterpret_cast<T**>(ptrDest)) = lmAddr;
                    return true;
                };

                SymbolLookupHandler lookupHandler;
                lookupHandler.returnSize = sizeof(T*);
                lookupHandler.serializer() = serializer;
                lookupHandler.deserializer() = deserializer;
                lookupHandler.invocation = getPtr;

                RegisterSymbol(MemberID::Local(), lookupIns, lookupHandler);
            }
    };
    
    template<typename T, T* PTR, typename NAME>
    class SynGlobalProxy {
        public:
            //extern template struct Reflect<SELF,OFF>;
        SynGlobalProxy() {
        }
        protected:
            static SynGlobalChild<T,PTR,NAME> __attribute__((used)) _childObject;
    };
    template<typename T, T* PTR, typename NAME>
    SynGlobalChild<T,PTR,NAME> SynGlobalProxy<T,PTR,NAME>::_childObject;
    
}

#endif /* SYNGLOBAL_H */

