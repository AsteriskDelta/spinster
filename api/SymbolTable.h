#ifndef SYMBOLTABLE_H
#define SYMBOLTABLE_H
#include "SpinsterInclude.h"
#include "Expose.h"
#include "SafeRef.h"
#include "MemberID.h"
#include <unordered_map>
#include <deque>

#define SPINSTER_RESERVED_SYMBOLS 64

namespace Spinster {

    namespace Obj {

        struct SymbolEntry : public $::Common {
            SymIDT id;
            SymbolLookupHandler handler;

            SymbolLookupIndex index;
            inline SymbolEntry() : id(0), handler(), index() {};
            SymbolEntry(const SymIDT& i, const SymbolLookupIndex& idx, const SymbolLookupHandler& handle);
            
            inline size_t argSize() const {
                return handler.argumentSize;
            }
            inline size_t retSize() const {
                return handler.returnSize;
            }
            
            inline SymbolEntry& operator=(const SymbolEntry &o) {
                id = o.id;
                handler = o.handler;
                index = o.index;
                //std::cout << "COPY HANDLER\n";
                return *this;
            }
            
            std::string str() const;
        };
    };

    typedef SafeRef<Obj::SymbolEntry> SymbolEntry;
    namespace Obj {
        class Group;
        class SymbolTable {
        public:
            SymbolTable();
            SymbolTable(SafeRef<Group> par);
            virtual ~SymbolTable();
            
            $::SymbolEntry add(const SymbolLookupIndex& idx, const SymbolLookupHandler& handle);
            //bool remove(const SymbolLookupIndex& idx);
            
            SymIDT getID(const SymbolLookupIndex& idx);
            $::SymbolEntry get(const SymbolLookupIndex& idx, MemberLink *context = nullptr);
            $::SymbolEntry at(const SymIDT& id, MemberLink *context = nullptr);
            $::SymbolEntry addr(void *const& ptr);
            
            bool knows(const SymIDT& id);
            SymIDT convert(const SymIDT& localID);
            void conversion(const SymIDT& local, const SymIDT& remote);
            inline SymbolLookupIndex& lookupOf(const SymIDT& id) {
                $::SymbolEntry entry = this->at(id);
                if(entry) return entry->index;
                else return entries[0].index;
            }
            
            $::SymbolEntry request(const SymbolLookupIndex& idx);//Yields (ie, blocks active task)
            void prefetch(const SymbolLookupIndex& idx);//Nonblocking
            
            void mapID(const SymIDT& from, const SymIDT& to);
            
            SafeRef<Group> parent;
        protected:
            SymIDT addEntry(const SymbolLookupIndex& idx, const SymbolLookupHandler& handle);
            
            std::deque<SymbolEntry> entries;
            std::unordered_map<SymbolLookupIndex, SymIDT> lookups;
            std::unordered_map<void*, SymIDT> addresses;
            SymIDT nextID;
        };
    };
    
    typedef SafeRef<Obj::SymbolTable> SymbolTable;
}

#endif /* SYMBOLTABLE_H */

