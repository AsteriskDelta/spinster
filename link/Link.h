#ifndef LINK_H
#define LINK_H
#include "SpinsterInclude.h"
#include "SafeRef.h"
#include "MemberID.h"
#include "Time.h"

namespace Spinster {
    namespace Obj {
        class Member;
        class Group;
    }

    class Link {
    public:
        Link();
        virtual ~Link();

        
        SafeRef<Obj::Group> other(Obj::Group *const& curr) const;
        inline SafeRef<Obj::Group> other(SafeRef<Obj::Group> &currentMember) const {
            return this->other(&currentMember);
        }
        SafeRef<Obj::Group> other() const;

        RRef<Obj::Group> members[2];
        
        inline RRef<Obj::Group>& from() {
            return members[0];
        }
        inline RRef<Obj::Group>& to() {
            return members[1];
        }
        inline void setMembers(const RRef<Obj::Group>& a, const RRef<Obj::Group>& b) {
            this->from() = a;
            this->to() = b;
        }
        
        virtual Time latency();
        virtual unsigned long throughput();
        
        inline bool complete() const {
            return members[0] && members[1];
        }
        virtual void unlink();
        
        inline void added() {
            refCount++;
        }
        inline void removed() {
            if(refCount == RefCountDestroy) return;
            
            refCount--;
            //if(refCount == 0) this->destroy();
        }
        void destroy();
        
        inline static constexpr const unsigned short RefCountDestroy = 65535;
        inline bool destroyed() const {
            return refCount == RefCountDestroy;
        }
    protected:
        unsigned short refCount;
    };

};

#endif /* LINK_H */

