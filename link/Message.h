#ifndef MESSAGE_H
#define MESSAGE_H
#include "SpinsterInclude.h"
#include "MemberID.h"
#include "Time.h"
#include "MemBuffer.h"
#include <unordered_map>
#include <set>
#include <list>
#include <memory>
#include "Priority.h"

namespace Spinster {
    namespace Obj {
        class MemberLink;
        class MemberPipe;
        class RemoteTask;
    }
    class Packet;
    struct SubPacket;

    class Message {
    public:
        Message();
        Message(MemBuffer *mb);
        Message(const Message& orig);
        virtual ~Message();

        //void to()
        
        typedef unsigned int Idt;
        struct Header {
            MemberID from, to;
            union {
                Time time;
                struct {
                    unsigned short frameSize;
                    unsigned short p1;
                    unsigned int p2;
                };
            };
            Idt localID;
            Priority priority;
            SymIDT symbol;
        } header;
        
        inline Idt& id() {
            return header.localID;
        }
        inline bool continuous() const {
            return !header.priority.mortal();
        }
        
        inline MemberID& from() {
            return header.from;
        }
        inline MemberID& to() {
            return header.to;
        }
        inline Time& time() {
            return header.time;
        }
        inline Priority& priority() {
            return header.priority;
        }
        
        inline size_t headerSize() const {
            return sizeof(Header);
        }
        inline SymIDT& symbolID() {
            return header.symbol;
        }
        
        bool allRead() const;
        size_t remaining() const;
        size_t blockSize() const;
        size_t readOffset() const;
        size_t size() const;
        
        inline bool complete() const {
            return (this->remaining() == 0 && writeCount == 0) || (writeCount == writeTarget && writeCount > 0) ||
                    (header.priority.immortal() && frames.size() > 0);
        }
        
        void wasRead(size_t bytes);
        bool write(const SubPacket *const& sub);
        bool frame(MemBuffer *buf);
        
        
        const char* dataPtr() const;
        
        SafeRef<MemBuffer> data;
        
        void clear();
        //bool complete();
        
        inline void spSent() {
            outstandingSP++;
        }
        inline void spRecv() {
            if(outstandingSP > 0) outstandingSP--;
        }
        inline const size_t& spCount() const {
            return outstandingSP;
        }
        inline bool relevant() {//When this returns false, the link is free to delete this object
            //Make sure we don't want a reply, or have non-ACK'd packets, or have yet to be fully sent
            return (header.priority.expectsReply() && ( outstandingSP > 0 || !this->complete() )) || header.priority.immortal();
        }
        
        struct Frame {
            size_t start;
            size_t size;
        };
        void recvFrame(size_t start, size_t len);
        void sendFrame(size_t start, size_t len);
        bool nextFrame();
        
    protected:
        size_t rrOffset, writeCount, writeTarget, outstandingSP;
    public:
        Obj::MemberPipe *owner;
        Obj::RemoteTask *remoteTask;
    private:
        bool hasEnd;
        std::list<Frame> frames;
        unsigned int newFrames;
    };
};

#endif /* MESSAGE_H */

