#ifndef NETPIPE_H
#define NETPIPE_H
#include "SpinsterInclude.h"
#include "MemberPipe.h"
#include "MemBuffer.h"

namespace Spinster {
    namespace Net {
        
        struct PipeImpl;

        class NetPipe : public $::Obj::MemberPipe {
        public:
            NetPipe($::Obj::MemberLink *par, unsigned char mode);
            virtual ~NetPipe();
            
            virtual bool open(const $::Address& addr, const $::Address& lcl) override;
            virtual bool close() override;
            virtual bool redirectTo(const $::Address& addr) override;
            
            virtual bool update() override;
            
            virtual bool valid() override;
            virtual unsigned int mtu() override;
            
            virtual bool writePacket(Packet *packet) override;
            virtual Packet* readPacket() override;
            virtual size_t packetWritesLeft() const override;
            virtual size_t packetReadsLeft() const override;
        private:
            PipeImpl *impl;
            MemBuffer buffer;
            Packet* queuedPacket;
        };
    }
}


#endif /* NETPIPE_H */

