#include "NetPipe.h"
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include "Packet.h"
#include "MemberLink.h"
#include "Spinster.h"
#include "Forwarding.h"
#include "Protocol.h"
#include "Net.h"

#define MIN_NP_SIZE 8192

namespace Spinster {
    namespace Net {

        struct PipeImpl {
            struct sockaddr_in local, remote;
            int socketID = -1;
        };

        NetPipe::NetPipe($::Obj::MemberLink *par, unsigned char mode) : $::Obj::MemberPipe(par, mode), impl(nullptr), queuedPacket(nullptr) {
            tmpPacket.setPipe(this);
        }

        NetPipe::~NetPipe() {
            impl = reinterpret_cast<PipeImpl*>(parent->sharedImpl);
            this->close();
            if (parent && parent->sharedImpl != nullptr) {
                delete reinterpret_cast<PipeImpl*> (parent->sharedImpl);
                parent->sharedImpl = nullptr;
            }
        }
        
        bool NetPipe::redirectTo(const $::Address& addr) {
            //impl->remote.sin_addr.s_addr = htonl(addr.ip.ip);
            impl->remote.sin_port = htons(addr.ip.port);
            //addr.reconstruct();
            int ret = 1;//inet_aton(reinterpret_cast<const char*>(addr.base.c_str()), &impl->remote.sin_addr);
            //std::cout << std::hex << "ATON " << impl->remote.sin_addr.s_addr << "\n";
            //std::cout << std::hex << "ADDR " << addr.ip.ip << "\n" << std::dec;
            impl->remote.sin_addr.s_addr = addr.ip.ip;
            //if(inet_aton(addr.base.c_str(), &impl->remote.sin_addr) == 0) return false;
            
            if(!isServer && impl->local.sin_addr.s_addr != addr.ip.ip) {
                impl->local.sin_addr.s_addr = addr.ip.ip;
                /*if(impl->socketID !=)
                if (::bind(impl->socketID, reinterpret_cast<sockaddr*> (&(impl->local)), sizeof (impl->local)) == -1) {
                    std::cout << "redirection to " << addr << " failed, #" << errno << "!\n";
                    return false;
                }*/
                this->close();
                this->open(addr, DataAddress());
            }
            
            std::cout << "\tredirecting to " << addr << ", ret=" << ret << "\n";
            return (ret != 0) && MemberPipe::redirectTo(addr);
        }

        bool NetPipe::open(const $::Address& addr, const $::Address& lcl) {
            //const $::Address *useAddr = &addr;
            if (addr.type != $::Address::IP && addr) {
                std::cerr << "Netpipe given non-IP address\n";
                return false;
            } else if (!addr) {
                //std::cerr << "sending netpipe given bad address " << addr << "\n";
                //return false;
                //useAddr = &lcl;
                isServer = true;
            } else {
                currentTargetAddr = addr;
            }

            buffer.ensure(MIN_NP_SIZE);
            //buffer.zero();
            queuedPacket = nullptr;
            if (parent->sharedImpl == nullptr) {
                this->impl = new PipeImpl;
                if ((impl->socketID = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
                    std::cerr << "socket failed\n";
                    return false;
                }

                if (fcntl(impl->socketID, F_SETFL, O_NONBLOCK) < 0) {
                    std::cerr << "fnctl nonblock failed\n";
                    return false;
                }

                const int reuseBool = 1;
                setsockopt(impl->socketID, SOL_SOCKET, SO_REUSEADDR, &reuseBool, sizeof (reuseBool));

                std::cout << "connect to " << addr << " via socket " << impl->socketID << " on " << lcl << "\n";

                memset((char *) &(impl->local), 0, sizeof (impl->local));
                impl->local.sin_family = AF_INET;
                impl->local.sin_port = htons(lcl.ip.port);
                
                memset((char *) &(impl->remote), 0, sizeof (impl->remote));
                impl->remote.sin_family = AF_INET;
                //if (this->receiver()) {
                if (!addr) {
                    impl->local.sin_addr.s_addr = htonl(INADDR_ANY);
                    //impl->remote.sin_port = htons(lcl.ip.port);
                    //inet_aton(reinterpret_cast<const char*>(&addr.ip.ip), &impl->remote.sin_addr);
                } else {
                    //impl->local.sin_addr.s_addr = htonl(addr.ip.ip);
                    //impl->remote.sin_port = htons(addr.ip.port);
                    //inet_aton(reinterpret_cast<const char*>(&addr.ip.ip), &impl->remote.sin_addr);
                    //inet_aton(reinterpret_cast<const char*>(&lcl.ip.ip), &impl->local.sin_addr);
                    //impl->remote.sin_addr.s_addr = htonl(addr.ip.ip);
                    impl->local.sin_addr.s_addr = addr.ip.ip;
                    this->redirectTo(addr);
                }
                if (::bind(impl->socketID, reinterpret_cast<sockaddr*> (&(impl->local)), sizeof (impl->local)) == -1) {
                    std::cerr << "bind failed, #" << errno << "\n";
                    return false;
                }
                //}
                /*if (this->sender() && addr) {
                    if (inet_aton(addr.base.c_str(), &impl->local.sin_addr) == 0) {
                        std::cerr << "inet_aton failed\n";
                        return false;
                    }
                }*/
                parent->sharedImpl = reinterpret_cast<void*> (this->impl);
                //parent->fdHandle = impl->socketID;
            } else {
                this->impl = reinterpret_cast<PipeImpl*> (parent->sharedImpl);
                //this->impl->socketID = parent->fdHandle;
            }

            return $::Obj::MemberPipe::open(addr, lcl);
        }

        bool NetPipe::close() {
            //Closing stuff goes here
            if (impl != nullptr && impl->socketID >= 0) {
                ::close(impl->socketID);
                impl->socketID = 0;
            }

            return $::Obj::MemberPipe::close();
        }

        bool NetPipe::valid() {
            return impl != nullptr && impl->socketID > 0;
        }

        unsigned int NetPipe::mtu() {
            return 1500;//512;
        }

        bool NetPipe::update() {
            if (queuedPacket != nullptr) this->writePacket(queuedPacket);
            MemberPipe::update();

            return true;
        }

        bool NetPipe::writePacket(Packet *packet) {
            if (queuedPacket != nullptr && !this->writePacket(queuedPacket)) {
                std::cerr << "writepacket called despite netpipe being busy!\n";
                return false;
            }

            //this->setPacketState(packet->sequenceID(), Packet::Ignored);
            this->buffer.clear();
            packet->write(&buffer);

            std::cout << "Sending packet of size " << packet->size() << " on " << this->impl->socketID << "\n";
            buffer.dbgDump();
            //if (isServer) {
                if (sendto(impl->socketID, buffer, buffer.size(), 0, reinterpret_cast<sockaddr*> (&impl->remote), sizeof (decltype(impl->remote))) >= 0) {

                    queuedPacket = nullptr;
                    return true;
                }
            /*} else {
                if (::send(impl->socketID, buffer, buffer.size(), 0) >= 0) {

                    queuedPacket = nullptr;
                    return true;
                }
            }*/

            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                queuedPacket = packet;
                return false;
            }

            std::cerr << "sendto failed, #" << errno << "\n";
            return false;
        }

        Packet* NetPipe::readPacket() {
            //Read into tmpPacket, then assign to packet[sequenceID] and return its pointer
            int packetLen = 0;
            sockaddr_in packetAddr;
            socklen_t addrLen = sizeof (decltype(packetAddr));
            buffer.clear();
            
            if ((packetLen = recvfrom(impl->socketID, buffer.c_str(), buffer.maxSize(), 0, reinterpret_cast<sockaddr*> (&packetAddr), &addrLen)) <= 0) {
                if (errno == EAGAIN || errno == EWOULDBLOCK) {
                    return nullptr; //No new data
                } else {
                    std::cerr << "recvfrom failed #" << errno << "\n";
                    return nullptr;
                }
            }

            buffer.skip(packetLen);
            printf("Received packet sz=%i from %s:%d\nData: %.*s\n", packetLen, inet_ntoa(packetAddr.sin_addr), ntohs(packetAddr.sin_port), int(packetLen), buffer.c_str());
            std::cout << "socket info: #" << impl->socketID << " addr " << parent->address() << "\n";
            buffer.dbgDump();
            
            
            Packet *packet = this->getPacketObject();
            packet->clear();
            const bool unpacked = packet->read(&buffer);

            if (!unpacked || packet->subPackets.empty()) {
                std::cerr << "failed to unpack\n";
                return nullptr;
            }
            
            if(isServer) {//Short circuit if we'e a server, just redirect
                Address connAddr(Address::IP);
                connAddr.ip.ip = packetAddr.sin_addr.s_addr;
                connAddr.ip.port = ntohs(packetAddr.sin_port);
                std::cout << "SERVER got packet from " << connAddr << "\n";
                if(connAddr == LocalAddress()) return nullptr;
                
                Message redirectMsg;
                redirectMsg.header.time = time(nullptr);
                redirectMsg.header.from = $::Local->id();
                redirectMsg.header.symbol = $::Protocol::Redirect;
                redirectMsg.header.priority.noReply();
                
                const unsigned short wrPort = Net::DataPort();//ntohs(impl->local.sin_port);
                redirectMsg.data->write(&wrPort, sizeof(wrPort));
                //std::cout << "\tredirected client to " << wrPort << ", msg data size = " << redirectMsg.data->size() << " for total size " << redirectMsg.size() << "\n";
                
                MemberID connID; 
                SubPacket *sub = &packet->subPackets[0];
                if(sub->dataSize() < sizeof(MemberID)) {
                    std::cerr << "client didn't give member ID!\n";
                    return nullptr;
                }
                memcpy(&connID, sub->data, sizeof(MemberID));
                connAddr.reconstruct();//Reconstruct the 'base' string
                
                Forward::Add(connAddr, connID);
                
                impl->remote = packetAddr;
                packet->clear();
                packet->add(&redirectMsg);
                this->writePacket(packet);
                packet->clear();
                return nullptr;
            }

            return (packet);
        }

        size_t NetPipe::packetWritesLeft() const {
            return (queuedPacket != nullptr) ? 0 : 1;
        }

        size_t NetPipe::packetReadsLeft() const {
            return 1;
        }
    }
}

