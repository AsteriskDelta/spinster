#ifndef NET_H
#define NET_H
#include "SpinsterInclude.h"
#include "NetPipe.h"
#include "NetLink.h"

namespace Spinster {
    class Address;
    namespace Net {
        //implicitly thread-local
        unsigned short Port();
        unsigned short DataPort();
        
        const $::Address& LocalAddress();
        const $::Address& DataAddress();
        
        //Begin "server"
        void Expose(unsigned short port, unsigned short dataPort = 0);
    };
};

#endif /* NET_H */

