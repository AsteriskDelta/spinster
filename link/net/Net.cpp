#include "Net.h"
#include <iostream>
#include "Address.h"
#include "Spinster.h"

namespace Spinster {

    namespace Net {
        unsigned short g_Port = 0, g_DataPort = 0;
        $::Address g_localAddress, g_dataAddress;

        unsigned short Port() {
            return g_Port;
        }
        unsigned short DataPort() {
            return g_DataPort;
        }
        
        const $::Address& LocalAddress() {
            return g_localAddress;
        }
        const $::Address& DataAddress() {
            return g_dataAddress;
        }

        void Expose(unsigned short port, unsigned short dp) {
            g_Port = port;
            
            if(dp == 0) dp = port + 1;
            g_DataPort = dp;
            g_localAddress = $::Address("127.0.0.1:"+std::to_string(port));
            g_dataAddress = $::Address("127.0.0.1:"+std::to_string(dp));
            
            
            std::cout << "start server on " << port << ", dataport=" << dp << "\n";
            Spinster::Connect($::Address(), g_localAddress);//Nonspecific handler at the requested port
        }
    };

};
