#ifndef NETLINK_H
#define NETLINK_H
#include "SpinsterInclude.h"
#include "MemberLink.h"
#include "NetPipe.h"

namespace Spinster {
    namespace Net {
        typedef MemberLinkTpl<NetPipe> NetLink;
    }
}


#endif /* NETLINK_H */

