#include "Address.h"
#include <sstream>
#include <iomanip>
#include <cstring>

namespace Spinster {
    Address::Address() : type(Invalid), base(), raw(0x0) {
        
    }
    Address::Address(const std::string& src) : Address() {
        this->parse(src);
    }
    Address::Address(const Type& tp, const RawType& rawSrc) : type(tp), base(), raw(rawSrc) {
        if(rawSrc != 0x0) this->reconstruct();
    }
    Address::Address(const Address& orig) : type(orig.type), base(orig.base), raw(orig.raw) {
        
    }
    Address::~Address() {
        
    }
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
    bool Address::parse(const std::string& src) {
        try {
            uint8_t cVals[4]; unsigned int sVals[2] = {0x0,0x0};
            char fakeChar;
            std::stringstream ss;
            
            if(sscanf(src.c_str(), "%u.%u.%u.%u:%u", &cVals[0], &cVals[1], &cVals[2], &cVals[3], &sVals[0]) >= 5) {
                type = IP;
                ss << int(cVals[0])<<"."<<int(cVals[1])<<"."<<int(cVals[2])<<"."<<int(cVals[3]);
                base = ss.str();
                
                if(sVals[0] != 0) this->ip.port = sVals[0];
                ip.ip = *reinterpret_cast<uint32_t*>(&cVals[0]);
                std::cout << "\tAddress::parse gave " << this->str() << " from \"" << src << "\"\n";
                return true;
            } else if(sscanf(src.c_str(), "localhos%c:%u", &fakeChar, &sVals[0]) >= 1 && fakeChar == 't') {
                type = IP;
                base = "127.0.0.1";
                
                if(sVals[0] != 0) this->ip.port = sVals[0];
                ip.a = 127; ip.b = 0; ip.c = 0; ip.d = 1;
                return true;
            } else {
                std::cerr << "failed to parse " << src << "\n";
                return false;
            }
        } catch(...) {
            return false;
        }
    }
#pragma GCC diagnostic pop
    std::string Address::str() const {
        std::stringstream ss;
        switch(type) {
            case IP:
                if(base.size() == 0) this->reconstruct();
                ss << base << ":" << ip.port;
                break;
            case Pipe:
                break;
            default: [[fallthrough]];
            case Invalid:
                return "[Invalid]";
        }
        return ss.str();
    }
    
    bool Address::reconstruct() const {
        static thread_local char rBuffer[128];
        size_t rSize = 0;
        switch(type) {
            case IP:
                rSize = snprintf(rBuffer, 128, "%u.%u.%u.%u", uint32_t(ip.a), uint32_t(ip.b), uint32_t(ip.c), uint32_t(ip.d));
            case Pipe:
                break;
            default: [[fallthrough]];
            case Invalid:
                return false;
        }
        
        if(rSize <= 0) return false;
        
        base = std::string(rBuffer);
        return true;
    }
    
    bool Address::isLocal() const {
        switch(type) {
            case IP:
                return ip.a == 127 && ip.b == 0 && ip.c == 0 && ip.d == 1;
            default: [[fallthrough]];
            case Invalid:
                return false;
        };
    }
}
