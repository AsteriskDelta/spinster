#include "Forwarding.h"
#include <unordered_map>
#include <queue>
#include "MemberLink.h"
#include <cassert>
#include <algorithm>
#include "Group.h"

//Link types
#include "net/Net.h"
#include "Routing.h"

#include "LocalGroup.h"

namespace Spinster {
    namespace Forward {
        std::unordered_map<$::Address, SafeRef<$::Obj::MemberLink>> table;
        std::priority_queue<$::Obj::MemberLink::Watchdog*> watchdogs;
        std::vector<SafeRef<$::Obj::MemberLink>> servers;
        
        
        std::list<SafeRef<$::Obj::MemberLink>> GetLinks() {
            std::list<SafeRef<$::Obj::MemberLink>> ret;
            for(auto it = table.begin(); it != table.end(); ++it) {
                ret.push_back(it->second);
            }
            return ret;
        }
        

        SafeRef<$::Obj::MemberLink> Get(const $::Address& addr) {
            auto it = table.find(addr);
            if (it == table.end()) return nullptr;
            else return it->second;
        }

        bool Has(const $::Address& addr) {
            auto it = table.find(addr);
            if (it == table.end()) return false;
            else return true;
        }
        
        bool Redirect(const $::Address& from, const $::Address& to) {
            auto it = table.find(from);
            if(it == table.end()) return false;
            
            table[to] = table[from];
            table.erase(it);
            return true;
        }

        SafeRef<$::Obj::MemberLink> Add(const $::Address& addr, SafeRef<$::Obj::MemberLink> obj) {
            if(Has(addr)) Remove(addr);
            table[addr] = obj;
            watchdogs.push(new Obj::MemberLink::Watchdog(&obj));
            
            return obj;
        }
        SafeRef<$::Obj::MemberLink> Add(const $::Address& addr, const MemberID& id) {
            if(!id || !addr) return nullptr;
            
            $::Obj::MemberLink *link = nullptr;
            switch(addr.type) {
                case $::Address::IP:
                    link = new $::Net::NetLink();
                    link->setMembers($::Local->asGroup(), Routing::Get(id, true));
                    link->setIntroduced(true);
                    std::cout << "trying to add conn to " << addr << " for " << id << "\n";
                    if(!link->open(addr, $::Net::DataAddress(), false)) {
                        std::cerr << "\tFAILED\n";
                        delete link;
                        return nullptr;
                    }
                break;
                default: [[fallthrough]];
                case $::Address::Invalid:
                    std::cerr << "Attempted to add invalid address for id " << id << "!\n";
                    return nullptr;
                    break;
            };
            
            if(link == nullptr) return nullptr;
            
            Routing::Add(link->other());
            Routing::AddLink(link);
            
            return Add(addr, link);
        }

        bool Remove(const $::Address& addr) {
            auto it = table.find(addr);
            std::cout << "FORWARD REMOVE " << addr << "\n";
            if(it == table.end()) return false;
            std::cout << "\tFound ^_^\n";
            //it->second->destroy();
            table.erase(it);
            return true;
        }
        
        bool Complete(const SafeRef<$::Obj::MemberLink> lnk) {
            Routing::Add(lnk->other());
            Routing::AddLink(lnk);
            return true;
        }

        void AddServer(const SafeRef<$::Obj::MemberLink> srv) {
            servers.push_back(srv);
        }

        bool RemoveServer(const SafeRef<$::Obj::MemberLink> srv) {
            auto it = std::find(servers.begin(), servers.end(), srv);
            if (it == servers.end()) return false;

            delete &srv;
            servers.erase(it);
            return true;
        }

        void Update() {
            const unsigned long long currentTime = time(nullptr);

            if (!watchdogs.empty()) {
                auto * watchdog = watchdogs.top();
                while (watchdog->killTime < currentTime) {
                    $::Obj::MemberLink *link = watchdog->link;
                    if(link == nullptr) {//Already destroyed, just cull the watchdog
                        delete watchdogs.top();
                        watchdogs.pop();
                    } else if (link->lastPacketTime() < watchdog->lastCheck) {//kill with fire? 
                        std::cout << "watchdog killing link, lastCheck=" << watchdog->lastCheck << " > " << link->lastPacketTime() << "\n";
                        link->destroy();
                        delete watchdogs.top();
                        watchdogs.pop();
                    } else {//We got a packet, let them live
                        watchdogs.pop();
                        assert(watchdog->killDelay != 0);
                        const_cast<Bare<decltype(*watchdog)>*> (watchdog)->heartbeat();
                        watchdogs.push(watchdog);
                    }

                    if (watchdogs.empty()) break;
                    watchdog = watchdogs.top();
                }
            }

            for (auto& serverLink : servers) {
                serverLink->update();
            }
            
            for(auto it = table.begin(); it != table.end(); ) {
                auto next = std::next(it);
                it->second->update();//May delete it
                it = next;
            }

            return;
        }
        
        void Cleanup() {
            //std::cout << "Forwarding cleanup...\n";
            for(auto it = table.begin(); it != table.end(); ) {
                auto next = std::next(it);
                it->second->destroy();
                it = next;
            }
            for(auto it = servers.begin(); it != servers.end(); ) {
                auto next = std::next(it);
                (*it)->destroy();
                it = next;
            }
            while(!watchdogs.empty()) {
                delete watchdogs.top();
                watchdogs.pop();
            }
        }
    };
};

