#ifndef LINKCONF_H
#define LINKCONF_H
#include "SpinsterInclude.h"

namespace Spinster {
class LinkConf {
public:
    LinkConf();
    LinkConf(const LinkConf& orig);
    virtual ~LinkConf();
    
    //Encryption, erasure coding, correction rates, etc.
private:

};
};

#endif /* LINKCONF_H */

