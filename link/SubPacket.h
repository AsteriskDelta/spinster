#ifndef SUBPACKET_H
#define SUBPACKET_H
#include "SpinsterInclude.h"

namespace Spinster {
    class MemBuffer;
    
    struct SubPacket {

        struct ShortHeader {
            unsigned int messageID = 0;

            struct {
                bool extended : 1;
                unsigned int offset : 20;
                unsigned int size : 10;
                bool end : 1;
            };
        };

        struct ExtHeader : ShortHeader {
            unsigned int extendedOffset = 0;
        };
        
        ExtHeader header;
        inline SubPacket() : header(), data(nullptr) {
            header.extended = false;
            header.size = 0;
            header.offset = 0;
        }

        inline size_t headerSize() const {
            return (header.extended ? sizeof(ExtHeader) : sizeof(ShortHeader));
        }

        inline size_t dataSize() const {
            return header.size;
        }

        inline size_t size() const {
            return this->headerSize() + this->dataSize();
        }

        bool write(MemBuffer* buffer);
        bool read(MemBuffer *buffer);

        inline void setData(const char *const& ptr, size_t size) {
            data = const_cast<char*> (ptr);
            header.size = size;
        }
        inline void setMessageID(const unsigned int& newID) {
            header.messageID = newID;
        }
        
        inline void setOffset(size_t bytes) {
            if(bytes >= 2048) {//2^11
                header.extended = true;
                header.offset = bytes & size_t(2048 - 1);
                header.extendedOffset = bytes >> 11;
            } else {
                header.extended = false;
                header.offset = bytes;
            }
        }
        inline size_t getOffset() const {
            size_t ret = header.offset;
            if(header.extended) {//2^11
                ret += header.extendedOffset << 11;
            }
            return ret;
        }

        const char* data;
        
        inline unsigned int messageID() const {
            return header.messageID;
        }
        inline const char* getData() const {
            return data;
        }
        
        inline static size_t MaxHeaderSize() {
            return sizeof(ExtHeader);
        }
    };
};
#endif /* SUBPACKET_H */

