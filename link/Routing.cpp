#include "Routing.h"
#include "Member.h"
#include "MemberLink.h"
#include "Group.h"
#include "NGraph.h"
#include "Spinster.h"
#include <memory>
#include "Forwarding.h"

namespace Spinster {
    namespace Routing {
        static $::Obj::NGraph *network = nullptr;
        void Initialize() {
            if(network != nullptr) return;
            
            network = new $::Obj::NGraph();
        }
        
        void Cleanup() {
            Forward::Cleanup();
            auto *delNet = network;
            network = nullptr;
            if(delNet != nullptr) delete delNet;
        }
        
        bool AddLink(SafeRef<Obj::MemberLink> link) {
            if(network == nullptr) return false;
            //std::cout << "addlink got " << &link->members[0] << ", " << &link->members[1] << "\n";
            //std::cout << "\tbools:" << bool(link) << ", "<< bool(link->members[0]) << ", " << bool(link->members[1]) << " compl " << link->complete() << "\n";
            if(link && link->complete()) {
                link->members[0]->addLink(link);
                link->members[1]->addLink(link);
                network->addLink(link);
                return true;
            } else {
                std::cerr << "incomplete link\n";
                return false;
            }
        }
        
        bool RemoveLink(SafeRef<Obj::MemberLink> link) {
            if(network == nullptr) return false;
            
            if(link/* && link->complete() */&& network->hasLink(link)) {
                const auto oldAddr = link->address();
                if(link->members[0]) link->members[0]->removeLink(link);
                if(link->members[1]) link->members[1]->removeLink(link);
                
                Forward::Remove(oldAddr);
                //std::cout << "removing link internals...\n";
                return network->removeLink(link);
            } else {
                //std::cerr << "incomplete/unindexed link\n";
                return false;
            }
        }

        SafeRef<Obj::MemberLink> LinkTo(const MemberID& id) {
            auto group = Get(id);
            if(!group) return nullptr;
            
            if(group->proxyID) return LinkTo(group->proxyID);
            //std::cout << "LinkTo using " << group->id() << "\n";
            
            if(!$::Local->connects(id)) {
                for(auto link : $::Local->links) {
                    bool linkHas = Yield(&link->other()->invoke(Protocol::Sym::Route, id));
                    if(linkHas) {
                        group->proxyID = link->other()->id();
                        return link;
                    }
                }
                std::cerr << "second order link couldn't be resolved!!!\n";
                return nullptr;
            }
            //std::cout << "returing " << &$::Local->linkTo(id) << " bool=" << bool($::Local->linkTo(id)) << "\n";
            
            return $::Local->linkTo(id);
        }
        
        SafeRef<Obj::Group> Get(const MemberID& id, const bool& force) {
            if(network == nullptr) return nullptr;
            return network->get(id, force);
        }
        
        bool Add(const MemberID& id, SafeRef<Obj::Group> group) {
            if(network == nullptr) return false;
            if(!group) group = new Obj::Group();
            
            group->raw.id = id;
            return network->add(group);
        }
        bool Add(SafeRef<Obj::Group> group) {
            if(network == nullptr) return false;
            //std::cout << "Routing add #" << group->id() << "\n";
            return network->add(group);
        }
        
        bool Changed(const MemberID &id) {
            return network->changed(id);
        }
        
        bool Remove(const MemberID& id) {
            if(network == nullptr) return false;
            auto group = Get(id);
            if(!group) return true;
            else return Remove(group);
        }
        bool Remove(SafeRef<Obj::Group> group) {
            if(network == nullptr) return false;
            //std::cout << "Removing group " << group->id() << "\n";
            
            return network->unlink(group->id());
        }
        
        
        Obj::NGraph* Network() {
            return network;
        }
    };

};
