#include "Message.h"
#include "SubPacket.h"
#include <cstring>

namespace Spinster {

    Message::Message() : header(), data(new MemBuffer()), rrOffset(0), writeCount(0), writeTarget(0), outstandingSP(0), owner(nullptr), hasEnd(false), newFrames(0) {
    }

    Message::Message(MemBuffer *mb) : header(), data(mb), rrOffset(0), writeCount(0), writeTarget(0), outstandingSP(0), owner(nullptr), hasEnd(false), newFrames(0) {
    }

    Message::Message(const Message& orig) : header(orig.header), data(orig.data), rrOffset(orig.rrOffset),
    writeCount(orig.writeCount), writeTarget(orig.writeTarget), outstandingSP(0), owner(orig.owner), hasEnd(orig.hasEnd), frames(orig.frames), newFrames(orig.newFrames) {
    }

    Message::~Message() {
        delete &data;
    }

    void Message::clear() {
        data->clear();
        hasEnd = false;
        rrOffset = 0;
        writeCount = writeTarget = 0;
        outstandingSP = 0;
        owner = nullptr;
        header = decltype(header) ();
        newFrames = 0;
        frames.clear();
    }

    /*bool Message::complete() {
        return data->complete();
    }*/

    bool Message::allRead() const {
        return this->remaining() == 0; //rrOffset >= this->size();
    }

    size_t Message::remaining() const {
        if (header.priority.immortal()) {
            //std::cout << "\timmortal check remaining via frame sz=" << frames.size() << "\n";
            return this->frames.size() * this->header.frameSize + (rrOffset < this->headerSize()? this->headerSize() : 0);
        } else return writeCount == 0 ? (this->size() - this->readOffset()) : (writeTarget < writeCount ? 1 : (writeTarget - writeCount));
    }

    size_t Message::blockSize() const {
        if (rrOffset < this->headerSize()) {
            return this->headerSize() - rrOffset;
        } else return 0xFFFF;
    }

    size_t Message::readOffset() const {
        return rrOffset;
    }

    size_t Message::size() const {
        if (header.priority.immortal()) return frames.size() * header.frameSize;
        else if (header.priority.lazy()) return writeTarget + this->headerSize();
        else return std::max(this->headerSize() + data->size(), writeTarget);
    }

    void Message::wasRead(size_t bytes) {
        rrOffset += bytes;
        if (rrOffset > this->headerSize()) {
            data->ignore(bytes);
            if (header.priority.immortal()) for (size_t b = 0; b < bytes && !frames.empty(); b += header.frameSize) frames.pop_front();
        }
        std::cout << "read " << bytes << " bytes for rrOffset = " << rrOffset << " >= " << this->size() << "?\n";

        //if(header.priority.lazy() && rrOffset) ;
    }

    const char* Message::dataPtr() const {
        if (rrOffset < this->headerSize()) {
            return reinterpret_cast<const char*> (&header) + rrOffset;
        } else {
            if (header.priority.immortal()) {
                if (this->newFrames > 0) return data->c_str() + frames.front().start;
                else return data->writing<char>();
            } else {
                return data->reading<char>();
            }
        }
    }

    bool Message::write(const SubPacket * const& sub) {
        if (sub->dataSize() == 0) return false;
        rrOffset = sub->getOffset();

        if (rrOffset >= this->headerSize()) data->ensure(rrOffset + sub->dataSize() - this->headerSize() + 1);
        char* wData = const_cast<char*> (this->dataPtr());
        std::cout << "write " << sub->dataSize() << " bytes to " << reinterpret_cast<const int*> (wData) << " from " << reinterpret_cast<const int*> (sub->getData()) << "\n";
        memcpy(wData, sub->getData(), sub->dataSize());
        if (rrOffset >= this->headerSize()) data->skip(sub->dataSize());

        rrOffset += sub->dataSize();
        writeCount += sub->dataSize();

        if (sub->header.end) {
            if (header.priority.immortal()) {
                if (sub->getOffset() >= this->headerSize()) {//Only add frames if we have a complete header
                    for (size_t off = 0; off < sub->dataSize(); off += header.frameSize) {
                        this->recvFrame(sub->getOffset() + off, header.frameSize);
                    }
                }
            } else {
                this->writeTarget = rrOffset;
            }
        }

        return true;
    }

    void Message::recvFrame(size_t start, size_t len) {
        frames.push_back(Frame{start, len});
        newFrames++;
    }

    void Message::sendFrame(size_t start, size_t len) {
        std::cout << "\t sending msg frame from " << start << " to " << (start + len) << "\n";
        this->recvFrame(start, len);
        if(start + len > 16) {//Loop over to constrain recorded frames
            data->edge.write = 0;
        }
    }

    bool Message::nextFrame() {
        if (newFrames == 0) return false;

        newFrames--;
        frames.pop_front();
        return !frames.empty();
    }

    bool Message::frame(MemBuffer *buf) {
        writeTarget -= writeCount;
        writeCount = 0;

        if (data->writeOffset() + buf->size() > 1024 * 16) data->edge.write = 0;

        data->write(buf);
        writeTarget += buf->size();

        return true;
    }
}

