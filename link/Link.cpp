#include "Link.h"
#include "Group.h"
#include "Spinster.h"

namespace Spinster {

    Link::Link() : members{nullptr, nullptr}, refCount(0) {
    }

    Link::~Link() {
    }
    
    SafeRef<Obj::Group> Link::other(Obj::Group *const& curr) const {
        //std::cout << "finding other between " << (bool(members[0])? members[0]->id() : MemberID()) << " and " << (bool(members[1])? members[1]->id() : MemberID()) << "\n";
        if(curr->id() == members[0]->id()) return members[1];
        else return members[0];
    }
    SafeRef<Obj::Group> Link::other() const {
        return this->other(&Local);
    }
    
    void Link::unlink() {
        this->~Link();
        new(this) Link();
    }

    Time Link::latency() {
        return Time(0);
    }

    unsigned long Link::throughput() {
        return 0;
    }
    
    void Link::destroy() {
        if(refCount == RefCountDestroy) return;
        delete this;
    }

};