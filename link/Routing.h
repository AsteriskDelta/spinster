#ifndef ROUTING_H
#define ROUTING_H
#include "SpinsterInclude.h"
#include "SafeRef.h"
#include "MemberID.h"

namespace Spinster {
    namespace Obj {
        class MemberLink;
        class Group;
        class NGraph;
    };
    
    namespace Routing {
        void Initialize();
        void Cleanup();
        
        bool AddLink(SafeRef<Obj::MemberLink> link);
        bool RemoveLink(SafeRef<Obj::MemberLink> link);
        SafeRef<Obj::MemberLink> LinkTo(const MemberID& id);
        
        bool Add(const MemberID& id, SafeRef<Obj::Group> group = nullptr);
        bool Add(SafeRef<Obj::Group> group);
        SafeRef<Obj::Group> Get(const MemberID& id, const bool& force = false);
        
        bool Changed(const MemberID &id);
        
        bool Remove(const MemberID& id);
        bool Remove(SafeRef<Obj::Group> group);
        
        Obj::NGraph* Network();
    };

};

#endif /* ROUTING_H */

