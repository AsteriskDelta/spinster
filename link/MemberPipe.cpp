#include "MemberPipe.h"
#include "FIDVector.impl.h"
#include "MemberLink.h"
#include "Spinster.h"
#include "unistd.h"
#include <sys/time.h>
#include "Protocol.h"
#include "Serializers.h"
#include "Event.h"

namespace Spinster {
    template class FIDVector<Message*, Message::Idt>;

    namespace Obj {

        MemberPipe::MemberPipe(MemberLink *par, unsigned char mode) : parent(par), in((mode & In) != 0x0), out((mode & Out) != 0x0),
        isServer(false), currentTargetAddr(), remoteAddr(), lazyCount(0) {
        }

        MemberPipe::~MemberPipe() {
            for (size_t i = 0; i < messages.size(); i++) {
                Message *msg = messages[i];
                if (messages[i] != nullptr) delete msg;
            }
        }

        bool MemberPipe::valid() {
            return false;
        }

        unsigned int MemberPipe::mtu() {
            return 512;
        }

        //Must be overridden

        bool MemberPipe::writePacket(Packet *packet) {
            this->setPacketState(packet->sequenceID(), Packet::Ignored);
            return false;
        }

        Packet* MemberPipe::readPacket() {
            //Read into tmpPacket, then assign to packet[sequenceID] and return its pointer
            return nullptr;
        }

        size_t MemberPipe::packetWritesLeft() const {
            return 0;
        }

        size_t MemberPipe::packetReadsLeft() const {
            return 0;
        }

        bool MemberPipe::open(const $::Address& addr, const $::Address& lcl) {
            _unused(addr, lcl);
            return true;
        }

        bool MemberPipe::close() {
            //std::cout << "closing memberpipe\n";
            return true;
        }

        bool MemberPipe::redirectTo(const $::Address& addr) {
            this->currentTargetAddr = addr;
            if(this->out) {
                for(size_t i = 0; i < messages.size(); i++) {
                    if(messages[i] != nullptr) {
                        delete messages[i];
                        messages[i] = nullptr;
                    }
                }
            }
            return true;
        }

        Message* MemberPipe::recieve() {
            if (!this->in) return nullptr;

            Message *msg = messages.getFresh();
            if (msg == nullptr) return nullptr;

            messages.unfresh(msg->id());
            /*if (msg->continuous() && !isServer) {
                msg->clear();
            } else {
                //messages[msg->id()] = nullptr;//Ownership is passed along
                //messages.unuse(msg->id());
            }*/
            msg->owner = this;
            return msg;
        }
        void MemberPipe::reply(Message *msg, SafeRef<MemBuffer> returnVal) {
            std::cout << "SEND REPLY FOR MSG#" << msg->id() << " on " << parent->address() << " to " << msg->from() << "\n";
            const auto msgID = msg->id();
            const auto msgFrom = msg->from();
            messages.unuse(msgID);
            messages[msgID] = nullptr;
            msg->clear();
            
            msg->header.symbol = $::Protocol::Reply;
            msg->header.priority.noReply();
            msg->header.to = msgFrom;
            
            $::SerializeTo(msg->data.get(), msgID);
            if(returnVal) {
                $::SerializeTo(msg->data.get(), *returnVal);
                //msg->data->write(returnVal->c_str(), returnVal->size());
            } else $::SerializeTo(msg->data.get(), MemBuffer::Empty());
            
            //Don't delete msg, because we've repurpused it for our reply
            parent->send(msg);
            //_unused(returnVal);
        }
        void MemberPipe::returned($::Message::Idt msgID) {
            std::cout << "RETURN for msg #" << msgID << "\n";
            //messages[msg->id()].clear();
            //TODO: OOB conditions
            delete messages[msgID];
            messages[msgID] = nullptr;
            messages.unuse(msgID);
        }

        bool MemberPipe::send(Message* msg) {
            if (!this->out) return false;

            Message::Idt mID;
            if(msg->priority().immortal() && msg->owner == this) mID = msg->header.localID;//Don't reallocate persistant messages
            else {
                mID = messages.push(msg);
                msg->header.localID = mID;
                msg->owner = this;
            }
            
            if (!msg->header.from) {
                if(!msg->priority().immortal()) msg->header.time = time(nullptr);//Otherwise, time doesn't matter
                msg->header.from = $::Local->id();
            }
            
            std::cout << "sending msg as #" << mID << " from " << msg->from() << " to " << msg->to() << ", datasize=" << msg->remaining() << "\n";
            
            if(!msg->priority().lazy() || msg->remaining() > 0 ) {
                if(messages.fresh(mID) && msg->priority().lazy()) this->lazyCount++;//Only increment if we're not already marked lazy
            }
            
            return true;
        }

        bool MemberPipe::send(Message* msg, const Address& addr) {
            if (!this->out) return false;
            else if (!this->isServer) {
                std::cerr << "Specific pipe called nonspecific send!\n";
            }

            if (this->currentTargetAddr != addr) {
                if (!this->redirectTo(addr)) {
                    std::cerr << "Couldn't redirect pipe to " << addr << "\n";
                    return false;
                }
            }

            return this->send(msg);
        }

        bool MemberPipe::update() {
            this->handleIO();
            return true;
        }

        void MemberPipe::handleIO() {
            Packet *packet;
            if (this->out) {
                while (this->packetWritesLeft() > 0 && (packet = this->constructPacket()) != nullptr) {
                    this->handlePacket(packet);
                }
            } else {//in
                while (this->packetReadsLeft() > 0 && (packet = this->readPacket()) != nullptr) {
                    this->handlePacket(packet);
                }
            }
            
            struct timeval tp;
            gettimeofday(&tp, NULL);
            double ioTime = tp.tv_sec + double(tp.tv_usec)/1000.0/1000.0;
            
            auto next = expected.begin();
            for(auto it = expected.begin(); it != expected.end(); it = next) {
                next = std::next(it);
                if(it->timeout > ioTime) {//Probably lost, send a NACK
                    this->setPacketState(it->id, Packet::Missing);
                    //expected.erase(it);
                }
            }
        }

        Packet* MemberPipe::constructPacket() {
            //std::cout << "constructPacket, fresh=" << messages.count() << "\n";
            //if(messages.getFresh()) std::cout << "\tfresh sym=" << messages.getFresh()->symbolID() << "\n";
            if (messages.available() == 0 || !this->out || 
                    (!parent->introduced && !isServer && (messages.getFresh()->symbolID() != 0))//Don't send any protocol messages until after handshake
                    ) return nullptr;
            
            //If all we have are lazy packets, be, well, lazy.
            //if(messages.available() > 0) std::cout << "Considering " << messages.available() << " of " << this->lazyCount << " lazy\n";
            if(messages.available() == this->lazyCount) return nullptr;

            Packet *ret = this->getPacketObject();
            Message* msg;
            while (!ret->full() && (msg = messages.getFresh()) != nullptr) {
                ret->add(msg);
                //std::cout << " constructing from MSG#" << msg->header.localID << ", " << msg->remaining() << " remain\n";
                /*sleep(1);
                std::cout << "FRESH: ";
                for (const auto& mid : messages.freshIDs) std::cout << mid << ", ";
                std::cout << "\n";*/
                if (msg->allRead()) {
                    if (isServer) messages.erase(msg->id());
                    else {
                        if(msg->priority().lazy() && lazyCount > 0) this->lazyCount--;
                        messages.unfresh(msg->id());
                    }
                }
            }

            this->setPacketState(ret->sequenceID(), Packet::Queued);
            return ret;
        }

        void MemberPipe::handlePacket(Packet *packet) {
            if (this->out) {
                std::cout << "writing packet " << packet->header.seqID << "\n";
                if (!this->writePacket(packet)) {
                    std::cerr << "Couldn't write packet, marking as completed to not leak...\n";
                    this->setPacketState(packet->sequenceID(), Packet::Ignored);
                } else {
                    this->setPacketState(packet->sequenceID(), Packet::Sent);
                }
            }
            if (this->in) {//implicitly in
                
                for (const SubPacket &sub : *packet) {
                    Message *msg = this->getMessage(sub.messageID());
                    if (msg == nullptr) {
                        std::cerr << "Got bad message ID #" << sub.messageID() << "\n";
                        continue;
                    }

                    bool written = true;
                    //written &= msg->data->seek(sub.getOffset());
                    //written &= msg->data->write(sub.getData(), sub.size());
                    written &= msg->write(&sub);
                    if (!written) {
                        std::cerr << "For packet #" << sub.messageID() << " couldn't write " << sub.size() << " bytes to offset " << sub.getOffset() << "\n";
                        continue;
                    }
                    
                    std::cout << "MSG #"<<sub.messageID() << ": Got bytes " << sub.getOffset() << " -> " << (sub.getOffset()+sub.dataSize()) << " of msg #" << msg->id() << ", " << msg->remaining() << " remain of " << msg->size() << "\n";

                    if (msg->complete()) {
                        std::cout << "Got message for symbol ID , sym " << msg->symbolID() << ", dataSize=" << msg->data->size() << "\n";
                        this->messages.fresh(sub.messageID());
                    }
                }
                std::cout << "Setting packet #" << packet->sequenceID() << " to recieved:\n";
                this->setPacketState(packet->sequenceID(), Packet::Recieved);
            }
        }

        void MemberPipe::setPacketState(Packet::Idt id, Packet::State state) {
            //Don't update an existing, identical status
            std::cout << "\t\tSET P STATE #" << id << " to " << state << " from " << packets[id].state() << ", out=" << this->out << "\n";
            if(id < packets.size() && packets[id].state() == state) return;
            std::cout << "\t\t\tCONTINUED\n";
            if (out) {
                if (state == Packet::Queued) packets.fresh(id);
                else if (state == Packet::Missing) {
                    this->writePacket(&packets[id]);
                } else if (state == Packet::Recieved) {
                    std::cout << "\tcount=" << packets[id].count() << "\n";
                    for(auto& sub : packets[id].subPackets) {
                        std::cout << "\t\tsub has msg # " << sub.messageID() << "\n";
                        Message *refMsg = messages[sub.messageID()];
                        if(refMsg == nullptr) {
                            std::cerr << "Subpacket reply message #"<<sub.messageID() << " not found!\n";
                            continue;
                        }
                        refMsg->spRecv();
                        std::cout << "\tmsg #" << refMsg->id() << " dec, sp=" << refMsg->spCount() << ", needsReply=" << refMsg->priority().expectsReply() << ", complete=" << refMsg->complete() << "\n";
                        if(!refMsg->relevant()) {//If all our packets have been received, and we don't want a reply, free the message
                            this->returned(refMsg->id());
                        }
                    }
                    packets.erase(id);
                } else if (state == Packet::Ignored) {
                    parent->ignore(id);
                }
            } else {//in
                if(id >= packets.size()) packets.insert(id, Packet());
                
                if(packets[id].state() == Packet::Expected) {//Remove from the expected list
                    auto it = expected.begin();
                    for(; it != expected.end(); ++it) {
                        if(it->id == id) break;
                    }
                    if(it != expected.end()) expected.erase(it);
                }
                
                if (state == Packet::Recieved) {
                    packets.erase(id); //Mark as free
                    parent->ack(id);
                    
                    //Detect any potential missing packets
                    //For any missing packets, set the timeout to send a NACK
                    const Packet::Idt& pktID = id;
                    for (const Packet::Idt& missingID : packets.unusedIDs) {
                        if (missingID >= pktID) break;
                        this->setPacketState(missingID, Packet::Expected);
                        packets.use(id);
                    }
                } else if (state == Packet::Ignored) {
                    packets.erase(id);
                } else if (state == Packet::Expected && packets[id].state() != Packet::Missing) {
                    this->expect(id);
                } else if (state == Packet::Missing) {
                    parent->missed(id);
                }
            }
            
            if(id < packets.size()) packets[id].state() = state;
        }

        Packet* MemberPipe::getPacketObject() {
            if(this->isServer) return &this->tmpPacket;
            else {
                Packet * const ret = &packets[packets.push()];
                ret->state() = Packet::Invalid;
                ret->setPipe(this);
                return ret;
            }
        }

        Message* MemberPipe::getMessage(const Message::Idt& mID) {
            if (!in) {
                if (mID >= messages.size()) return nullptr;
                else return messages[mID];
            } else {
                Message *ret = nullptr;
                if (mID < messages.size()) ret = messages[mID];

                if (ret == nullptr) {//New message, see if it's reasonable
                    ret = new Message();
                    messages.insert(mID, ret);
                    ret->header.localID = mID;
                }
                
               // std::cout << "returning msg with data=" << ret->data.get() << "\n";
                return ret;
            }
        }

        void MemberPipe::expect(const Packet::Idt& id) {
            std::cerr << "Expect packet #" << id << "\n";
            struct timeval tp;
            gettimeofday(&tp, NULL);
            double timeout = tp.tv_sec + double(tp.tv_usec)/1000.0/1000.0;
            timeout += 0.25;//Default timeout
            expected.push_back(ExpectedPacket{timeout, id});
        }
    };

};
