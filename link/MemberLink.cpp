#include "MemberLink.h"
#include "Group.h"
#include "Routing.h"
#include "SymbolTable.h"
#include "Invokee.h"
#include "Invocation.h"
#include "LocalGroup.h"
#include "Protocol.h"
#include "Forwarding.h"

namespace Spinster {

    namespace Obj {

        inline static bool FakePacketFn(Packet::Idt id) {
            _unused(id);
            return true;
        }
        struct MemberProtoInvoke {
            typedef Invokee<decltype(&FakePacketFn), Packet::Idt> PacketEvent;
            union {
                struct {
                    PacketEvent ack, nack, ignore;
                } send;
                PacketEvent peRaw[3];
            };
            bool created;
            MemberProtoInvoke() : created(false) {
                
            };
            ~MemberProtoInvoke() {
                if(created) this->destroy();
            }
            
            void destroy() {
                for(uint8_t i = 0; i < 3; i++) peRaw[i].~PacketEvent();
            }
            
            bool redirect(MemberLink *link) {
                if(created) this->destroy();
                for(uint8_t i = 0; i < 3; i++) new(&peRaw[i]) PacketEvent();
                created = true;
                
                std::cout << "\nSET REDIRECT\n\n";
                
                for(uint8_t i = 0; i < 3; i++) {
                    peRaw[i].persist();
                    peRaw[i].priority.noReply();
                    peRaw[i].priority.set($::Priority::Mortal, false);
                    peRaw[i].priority.set($::Priority::Lazy, true);
                    peRaw[i].sender = MyID();
                    peRaw[i].target = link->other()->id();
                }
                send.ack.func = Protocol::Sym::Ack;
                send.nack.func = Protocol::Sym::Nack;
                send.ignore.func = Protocol::Sym::Ignore;
                return true;
            }
        };
        
        MemberLink::MemberLink() : connected(this), disconnected(),
                rx(nullptr), tx(nullptr), fdHandle(0), sharedImpl(nullptr), introduced(false), lastPacketTimestamp(0), 
                watcher(nullptr), m_shouldClose(false), invoke(new MemberProtoInvoke()) {
            //connected.allocate(this);
            //disconnected.allocate();
        }

        MemberLink::~MemberLink() {
            this->refCount = Link::RefCountDestroy;
            
            if(this->valid()) this->close();
            if(rx != nullptr) delete rx;
            if(tx != nullptr) delete tx;
            
            if(watcher != nullptr) watcher->destroy();//Which auto-deletes
            delete this->invoke;
            
            //connected->release();
            //disconnected->release();
        }

        void MemberLink::setIntroduced(bool st) {
            if (!this->invoke->created) this->invoke->redirect(this);

            if (!introduced && st) this->connected();
            else if (introduced && !st) this->disconnected();
            introduced = st;
        }
        
        bool MemberLink::open(const $::Address& addr, const $::Address& lcl, bool introduce) {
            bool ret = rx->open(addr, lcl) && tx->open(addr, lcl);
            if(ret) {
                /*std::cout << "sending message to " << addr << ", local=" << lcl << "\n";
                Message *testMsg = new Message();
                if(!addr) this->send(testMsg, lcl);
                else this->send(testMsg) || (std::cerr << "SEND failed\n");//took ownership
            */
                if(addr && introduce) {
                    Message *testMsg = new Message();
                    this->send(testMsg) || (std::cerr << "SEND failed\n");//took ownership
                    //his->tx->returned(testMsg->id());
                }
             } else std::cerr << "Memberlink open failed\n";
            
            return ret;
        }
        bool MemberLink::redirect(const $::Address& addr) {
            return this->valid() && rx->redirectTo(addr) && Forward::Redirect(this->address(), addr) && tx->redirectTo(addr);
        }
        bool MemberLink::close() {
            if(!this->valid()) return true;//Already closed
            std::cout << "memberlink close: introduced=" << introduced << "\n";
            if(introduced) {
                if(!m_shouldClose) this->sendDisconnectMsg();
                disconnected.emit(this->address(), this->other()->id());
            }
            
            if(rx) rx->close();
            if(tx) tx->close();
            
            
            if(introduced) Routing::RemoveLink(this);
            introduced = false;
            
            return true;
        }
        
        void MemberLink::sendDisconnectMsg() {
            if(!tx) return;
            std::cout << "sending disconnect msg\n";
            Message msg;
            msg.header.time = time(nullptr);
            msg.header.from = $::Local->id();
            msg.header.symbol = $::Protocol::Disconnect;
            msg.header.priority.noReply();

            tx->tmpPacket.clear();
            tx->tmpPacket.add(&msg);
            tx->writePacket(&tx->tmpPacket);
            tx->tmpPacket.clear();
        }
        
        bool MemberLink::valid() {
            return rx != nullptr && tx != nullptr && rx->valid() && tx->valid();
        }

        bool MemberLink::send(Message * const& msg) {
            if(tx == nullptr || !(*tx)) return false;
            else return tx->send(msg);
        }
        bool MemberLink::send(Message *const& msg, const Address& addr) {
            if(tx == nullptr || !(*tx)) return false;
            else return tx->send(msg, addr);
        }

        Message* MemberLink::recieve(bool async) {
            _unused(async);
            if(rx == nullptr || !(*rx)) return nullptr;
            else return rx->recieve();
        }
        
        bool MemberLink::update() {
            if(!this->valid()) return false;
            
            if(this->m_shouldClose) {
                this->close();
                this->destroy();
                return false;
            }
            $::CurrentLink = this;
            
            
            //if(watcher != nullptr) std::cout << "Updating link with ttl=" << (this->lastPacketTime() - watcher->lastCheck) << "\n";
            if(this->other() && watcher != nullptr && watcher->inGracePeriod(this->lastPacketTime())) {
                std::cout << "sending keepalive ping\n";
                this->other()->invoke($::Protocol::Sym::Ping);
                watcher->usedGracePeriod();
            }
            
            if(!(rx->update() && tx->update())) return false;
            
            Message *msg = nullptr;
            while((msg = rx->recieve()) != nullptr) {
                std::cout << "HANDLE SYM #" << msg->header.symbol << ", size=" << msg->data->size() << "\n";
                if($::MemberOwned(msg->header.to)) {
                    //SafeRef<SymbolEntry> entry = this->getSymbol(msg->header.symbol);
                    SafeRef<SymbolEntry> sym = $::Local->symbols()->at(msg->header.symbol, this);
                    QueueSymInvocation(sym, msg);
                } else {
                    std::cerr << "asked to forward, to " << msg->header.to << ", bailing out...\n";
                    rx->returned(msg->id());
                    auto nextLink = $::Routing::LinkTo(msg->header.to);
                    if(!nextLink) {
                        
                        continue;
                    }
                }
            }
            return true;
        }
        
        unsigned long long MemberLink::lastPacketTime() const {
            return this->lastPacketTimestamp;
        }
        
        void MemberLink::ack(Packet::Idt id) {
            this->lastPacketTimestamp = time(nullptr);
            if(!this->invoke->created) return;
            std::cout << "\nACKP " << id << "\n\n";
            this->invoke->send.ack.emit(id);
        }
        void MemberLink::missed(Packet::Idt id) {
            if(!this->invoke->created) return;
            std::cout << "\nMISS " << id << "\n\n";
            this->invoke->send.nack.emit(id);
        }
        void MemberLink::ignore(Packet::Idt id) {
            if(!this->invoke->created) return;
            std::cout << "\nIGNR " << id << "\n\n";
            this->invoke->send.ignore.emit(id);
        }
        
        bool MemberLink::setEndpointID(const MemberID& id) {
            if(this->other()) return false;
            
            this->members[1] = Routing::Get(id, true);
            std::cout << "setEndPointID got " << &members[1] << " from " << id << "\n";
            this->invoke->redirect(this);
            return bool(this->other());
        }
    };
};