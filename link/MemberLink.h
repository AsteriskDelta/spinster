#ifndef MEMBERLINK_H
#define MEMBERLINK_H
#include "SpinsterInclude.h"
#include "SafeRef.h"
#include "Message.h"
#include "Packet.h"
#include "Time.h"
#include "FIDVector.h"
#include "Link.h"
#include "MemberPipe.h"
#include "Address.h"
#include "Event.h"

namespace Spinster {
    class Message;

    namespace Obj {
        class MemberPipe;
        class MemberLink;
        
        struct MemberProtoInvoke;
        
        class MemberLink : public $::Link {
        public:
            struct Watchdog {
                MemberLink *link = nullptr;
                unsigned long long killTime, lastCheck;
                unsigned short killDelay = 0xFFFF;
                bool usedGrace = false;
                
                inline Watchdog(MemberLink *par) : link(par), killDelay($::LinkTimeout) {
                    this->heartbeat();
                    lastCheck = 0;
                    link->setWatchdog(this);
                }
                
                inline bool operator<(const Watchdog& o) const {
                    return killTime > o.killTime;
                }
                inline void heartbeat() {
                    lastCheck = time(nullptr);
                    killTime = lastCheck + killDelay;
                    usedGrace = false;
                }
                inline void destroy() {
                    link = nullptr;
                }
                
                inline bool inGracePeriod(const unsigned long long t) const {
                    if(usedGrace || lastCheck == 0) return false;
                    
                    if(t <= $::LinkTimeoutGrace + lastCheck) return true;
                    else return false;
                }
                inline void usedGracePeriod() {
                    usedGrace = true;
                }
            };
            
            MemberLink();
            virtual ~MemberLink();
            
            bool open(const $::Address& addr, const $::Address& lcl, bool introduce = true);
            bool redirect(const $::Address& addr);
            bool close();
            void sendDisconnectMsg();
            
            $::Event<SafeRef<MemberLink>> connected;
            $::Event<$::Address, $::MemberID> disconnected;
            
            $::Obj::MemberPipe *rx, *tx;

            bool update();
            
            bool send(Message *const& msg);
            bool send(Message *const& msg, const Address& addr);//NOT error/loss corrected (just raw message)
            
            Message* recieve(bool async = true);
            bool valid();
            
            bool setEndpointID(const MemberID& id);
            
            $::Address address() const {
                return tx->currentTargetAddr;
            }
            
            void ack(Packet::Idt id);
            void missed(Packet::Idt id);
            void ignore(Packet::Idt id);
            
            unsigned long long lastPacketTime() const;

            unsigned int fdHandle;
            void *sharedImpl;
            
            void setIntroduced(bool st);
            bool introduced;
            
            inline void shouldClose() {
                m_shouldClose = true;
            }
            inline void setWatchdog(Watchdog *watch) {
                watcher = watch;
            }
        protected:
            unsigned long long lastPacketTimestamp;
            Watchdog* watcher;
            bool m_shouldClose;
            
            MemberProtoInvoke *invoke;
            //std::unordered_map<unsigned int, Packet*> livePackets;
            //std::unordered_set<unsigned int> anticipatedPackets;
        };
    };

    class MemberLink : public SafeRef<Obj::MemberLink> {
    public:
        typedef SafeRef<Obj::MemberLink> Parent;

        inline MemberLink() : Parent() {
        };
        using Parent::Parent;
    };
    SPINSTER_SAFEREF_NULL(MemberLink);
    
    template<typename PIPE_T>
    class MemberLinkTpl : public Obj::MemberLink {
    public:
        inline MemberLinkTpl() {
            this->rx = new PIPE_T(this, 0x1);
            this->tx = new PIPE_T(this, 0x2);
        }
    };
};

#endif /* MEMBERLINK_H */

