#include "Packet.h"
#include "MemberPipe.h"

namespace Spinster {

    Packet::Packet() : header(), subPackets(), dataSizeCnt(0), dataSizeMax(0), m_state(Invalid) {
    }
    
    Packet::Packet(std::nullptr_t nullp) : header(), subPackets(), dataSizeCnt(0), dataSizeMax(0), m_state(Invalid) {
        _unused(nullp)
    }

    Packet::Packet(const Packet& orig) : header(orig.header), subPackets(orig.subPackets), dataSizeCnt(orig.dataSizeCnt), dataSizeMax(orig.dataSizeMax), m_state(orig.m_state) {
        
    }

    Packet::~Packet() {
    }
    
    void Packet::clear() {
        subPackets.clear();
        dataSizeCnt = 0;
        dataSizeMax = 0;
        header = decltype(header)();
    }
    
    void Packet::setPipe(Obj::MemberPipe *const& pipe) {
        dataSizeMax = pipe->mtu();
    }
    
    bool Packet::add(Message * const& msg) {
        if(msg == nullptr || this->full()) return false;
        else if(msg->allRead()) return true;
        
        do {
            subPackets.push_back(SubPacket());
            SubPacket *packet = &subPackets.back();
            packet->setOffset(msg->readOffset());
            packet->setMessageID(msg->id());
            
            //Force packet data align to 4 bytes (for ARM et al)
            const size_t dSize = std::min(((this->remaining() - packet->headerSize())/4)*4, std::min(msg->remaining(), msg->blockSize()));
            const char *const dPtr = msg->dataPtr();//msg->read(dSize, dOffset);
            
            packet->setData(dPtr, dSize);
            msg->wasRead(dSize);
            msg->spSent();
            
            this->dataSizeCnt += packet->size();
            if(msg->allRead() || msg->priority().immortal()) packet->header.end = true;
        } while(!this->full() && !msg->allRead());
        
        return true;
    }

    bool Packet::write(MemBuffer *const& buffer) {
        try {
            bool ret = true;
            ret &= buffer->write(&(this->header), this->headerSize());
            
            for(SubPacket& sub : subPackets) ret &= sub.write(buffer);
            return ret;
        } catch(...) {
            return false;
        }
    }
    bool Packet::read(MemBuffer *const& buffer) {
        try {
            bool ret = true;
            ret &= buffer->read(&(this->header), this->headerSize());
            
            SubPacket packet;
            while(packet.read(buffer)) subPackets.push_back(packet);
            return ret;
        } catch(...) {
            return false;
        }
    }
}