#ifndef MEMBERPIPE_H
#define MEMBERPIPE_H
#include "SpinsterInclude.h"
#include "FIDVector.h"
#include "SafeRef.h"
#include "Message.h"
#include "Packet.h"
#include "Address.h"

namespace Spinster {
    namespace Obj {
        class MemberLink;

        class MemberPipe {
            friend class MemberLink;
        public:

            enum IOF : unsigned char {
                In = 0x1,
                Out = 0x2
            };
            
            struct ExpectedPacket {
                double timeout;
                Packet::Idt id;
            };

            MemberPipe(MemberLink *par, unsigned char mode);
            virtual ~MemberPipe();
            
            virtual bool open(const $::Address& addr, const $::Address& lcl);
            virtual bool close();
            virtual bool redirectTo(const $::Address& addr);//For servers only!! Or very, very brave client programmers...
            
            virtual bool update();

            Message* recieve();//STILL OWNED, don't delete!!!
            void reply(Message *msg, SafeRef<MemBuffer> returnVal);//Because this deletes it for you, NOTE ownership of returnVal is passed to this function!!
            void returned($::Message::Idt msgID);//Task completion, free up ID again
            
            bool send(Message* msg);
            bool send(Message* msg, const Address& addr);
            
            virtual bool valid();
            virtual unsigned int mtu();
            
            inline explicit operator bool() {
                return this->valid();
            }

            void handleIO();

            Packet* constructPacket();
            void handlePacket(Packet *packet);
            
            virtual bool writePacket(Packet *packet);
            virtual Packet* readPacket();
            virtual size_t packetWritesLeft() const;
            virtual size_t packetReadsLeft() const;

            void expect(const Packet::Idt& id);
            void setPacketState(Packet::Idt id, Packet::State state);

            FIDVector<Message*, Message::Idt> messages;
            FIDVector<Packet, Packet::Idt> packets;
            
            Message* getMessage(const Message::Idt& mID);

            MemberLink *parent;
            bool in, out;

            inline bool receiver() const { return in; };
            inline bool sender() const { return out; };
        protected:
            Packet* getPacketObject();
            Packet tmpPacket;
            
            std::vector<ExpectedPacket> expected;
            
            bool isServer;
            $::Address currentTargetAddr, remoteAddr;
            unsigned int lazyCount;
        };
    };
};

#endif /* MEMBERPIPE_H */

