#ifndef PACKET_H
#define PACKET_H
#include "SpinsterInclude.h"
#include "MemBuffer.h"
#include "MemberID.h"
#include "Time.h"
#include "Priority.h"
#include "SubPacket.h"

namespace Spinster {
    class MemberLink;
    class Message;
    namespace Obj {
        class MemberPipe;
    };
    
    class Packet {
    public:

        enum State : char {
            Queued,
            Sent,
            Recieved,
            Missing,
            Ignored,
            Expected,
            Invalid
        };

        Packet();
        Packet(std::nullptr_t nullp);
        Packet(const Packet& orig);
        virtual ~Packet();

        typedef unsigned int Idt;

        struct Header {
            Idt seqID;
        } header;
        std::vector<SubPacket> subPackets;

        void setPipe(Obj::MemberPipe *const& pipe);
        
        bool add(Message * const& msg);
        bool write(MemBuffer *const& buffer);
        bool read(MemBuffer *const& buffer);
        
        void clear();
        
        inline auto begin() const {
            return subPackets.begin();
        }
        inline auto end() const {
            return subPackets.end();
        }

        inline size_t headerSize() const {
            return sizeof (Header);
        }

        inline size_t dataSize() const {
            return dataSizeCnt;
        }

        inline size_t size() const {
            return headerSize() + dataSize();
        }
        
        inline size_t count() const {
            return subPackets.size();
        }

        inline size_t remaining() const {
            return dataSizeMax - dataSizeCnt - this->headerSize();
        }

        inline bool full() const {
            return this->remaining() < (2 * SubPacket::MaxHeaderSize());
        }
        
        inline Idt sequenceID() const {
            return header.seqID;
        }
        
        inline State& state() {
            return m_state;
        }

    protected:
        size_t dataSizeCnt, dataSizeMax;
        State m_state;
    };
};

#endif /* PACKET_H */

