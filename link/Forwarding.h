#ifndef FORWARDING_H
#define FORWARDING_H
#include "SpinsterInclude.h"
#include "Address.h"
#include <list>

namespace Spinster {
    namespace Obj {
        class MemberLink;
    }
    namespace Forward {
        SafeRef<$::Obj::MemberLink> Get(const $::Address& addr);
        bool Has(const $::Address& addr);
        
        SafeRef<$::Obj::MemberLink> Add(const $::Address& addr, SafeRef<$::Obj::MemberLink> obj = nullptr);
        SafeRef<$::Obj::MemberLink> Add(const $::Address& addr, const MemberID& id);
        bool Remove(const $::Address& addr);
        bool Redirect(const $::Address& from, const $::Address& to);
        
        bool Complete(const SafeRef<$::Obj::MemberLink> lnk);
        
        void AddServer(const SafeRef<$::Obj::MemberLink> srv);
        bool RemoveServer(const SafeRef<$::Obj::MemberLink> srv);
        
        bool Heartbeat(const $::Address& addr);
        
        void Update();
        
        void Cleanup();
        
        std::list<SafeRef<$::Obj::MemberLink>> GetLinks();
    };
};

#endif /* FORWARDING_H */

