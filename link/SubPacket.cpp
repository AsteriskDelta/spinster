#include "SubPacket.h"
#include "MemBuffer.h"

namespace Spinster {

    bool SubPacket::write(MemBuffer* buffer) {
        std::cout << "\tSubpacket writing " << this->headerSize() << " + " << this->dataSize() << " bytes\n";
        std::cout << "\tfrom " << sizeof(ShortHeader) << " and l=" << sizeof(ExtHeader) << "\n";
        return buffer->write(&(this->header), this->headerSize()) &&
               (this->dataSize() == 0 || buffer->write(this->data, this->dataSize()));
    }

    bool SubPacket::read(MemBuffer *buffer) {
        bool ret = true;
        ret &= buffer->read(&(this->header), sizeof(ShortHeader));//If we're extended, read in full offset value
        if(ret && this->header.extended) ret &= buffer->read(&header.extendedOffset, sizeof(decltype(header.extendedOffset)));
       
        this->data = buffer->reading<char>();
        return ret && buffer->ignore(this->dataSize());
    }
};
