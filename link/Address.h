#ifndef ADDRESS_H
#define ADDRESS_H
#include "SpinsterInclude.h"
#include "SynObject.h"

namespace Spinster {

    class Address : public Common, public SynObject {
    public:
        typedef uint64_t RawType;
        enum Type : unsigned char {
            IP,
            COM,
            USB,
            File,
            Pipe,
            Invalid
        };

        Address();
        Address(const std::string& src);
        Address(const Type& tp, const RawType& rawSrc = 0x0);
        Address(const Address& orig);
        ~Address();

        bool parse(const std::string& src);
        std::string str() const;
        
        bool reconstruct() const;//Reconstructs 'base' from lower-level data

        inline bool valid() const {
            return this->type != Invalid;
        }

        inline operator std::string() const {
            return this->str();
        }

        inline explicit operator bool() const {
            return this->valid();
        }
        
        Type type;
        mutable std::string base;
        
        inline bool operator==(const Address& o) const {
            return type == o.type && raw == o.raw && ((type == File || type == COM)? (base == o.base) : true);
        }
        inline bool operator!=(const Address& o) const {
            return !(*this == o);
        }
        
        bool isLocal() const;

        union {
            struct {
                union {
                    unsigned int ip;
                    struct { uint8_t a,b,c,d; };
                };
                unsigned short port;
            } ip;
            struct {
                unsigned short devID;
            } com;
            struct {
                unsigned short pipeID;
            } pipe;
            RawType raw;
        };
    };
    
    _share(Address, type, base, raw);

};

namespace std {

    template<>
    struct hash<Spinster::Address> {
        std::size_t operator()(const Spinster::Address& rr) const noexcept {
            return ((rr.type == Spinster::Address::File || rr.type == Spinster::Address::COM)? std::hash<std::string>{}(rr.base) : 0x0)
            ^ static_cast<unsigned char>(rr.type) ^ (rr.raw);
        }
    };

}

#endif /* ADDRESS_H */

