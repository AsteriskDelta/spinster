#ifndef DEMANGLE_H
#define DEMANGLE_H
#include <string>
#include <typeinfo>

namespace Spinster {
    std::string demangle(const char* name);

    template <class T>
    std::string readtype(const T& t = T()) {
        return demangle(typeid (t).name());
    }

};

#endif /* DEMANGLE_H */

