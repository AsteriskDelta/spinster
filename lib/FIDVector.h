#ifndef FIDVECTOR_H
#define FIDVECTOR_H
#include "SpinsterInclude.h"
#include <vector>
#include "SafeRef.h"

//Include FIDVector.impl.h to instantiate

namespace Spinster {

    template<typename VT, typename IDT = unsigned int>
    class FIDVector {
    public:
        typedef VT Value;
        typedef IDT Idt;
        
        FIDVector(size_t reserveSize = 16);
        FIDVector(const FIDVector& orig);
        virtual ~FIDVector();
        
        SafeRef<VT> get(IDT id);
        
        inline VT& operator[](const IDT& id) {
            return *(this->get(id));
        }
        
        IDT push();
        IDT push(const VT& dat);
        
        bool insert(const IDT& id, const VT& dat);
        
        bool fresh(IDT id);
        bool unfresh(IDT id);
        bool use(IDT id);
        bool unuse(IDT id);
        bool erase(IDT id);
        
        bool resize(IDT sz);
        
        IDT size() const;//Total size
        IDT count() const;//"In use" size
        IDT available() const;//"fresh" size
        
        std::vector<VT> data;
        
        VT& getFresh();
        VT takeFresh();
        
        //IDT edge;//From here on, all values are NOT in use
        std::vector<IDT> unusedIDs;//IDs less than edge not in use
        std::vector<IDT> freshIDs;//IDs that require attention by the user application
    private:
        static VT nullValue;
    };

};

#endif /* FIDVECTOR_H */

