#ifndef COMMON_H
#define COMMON_H
#include <type_traits>
#include <iostream>

namespace Spinster {
    struct Common {
        
    };
    
    template<typename genType>
    typename std::enable_if<std::is_base_of<::Spinster::Common, genType>::value, std::ostream&>::type 
    inline operator<<(std::ostream& out, const genType& g) {
        return out << g.str();
    }
};

#endif /* COMMON_H */

