#ifndef MEMBUFFER_H
#define MEMBUFFER_H
#include <cstdlib>
#include "SpinsterInclude.h"

namespace Spinster {

    class MemBuffer {
    public:
        MemBuffer();
        MemBuffer(void *const& dt, size_t len, bool fxd, bool cns);
        MemBuffer(const void *const& dt, size_t len);
        MemBuffer(size_t len);//fixed version
        MemBuffer(const MemBuffer& orig);
        ~MemBuffer();

        size_t size() const;
        size_t maxSize() const;

        bool ensure(size_t minSize);
        inline bool reserve(size_t minSize) {
            return this->ensure(minSize);
        }
        inline bool ensureRel(size_t dSize) {
            return this->ensure(this->size() + dSize);
        }

        template<typename T>
        inline bool write(const T *d, size_t len) {
            return this->write(reinterpret_cast<const void*> (d), len);
        }
        
        inline bool write(const MemBuffer *const& otherBuffer) {
            return this->write(otherBuffer->data, otherBuffer->size());
        }

        template<typename T>
        inline bool read(T *d, size_t len) {
            return this->read(reinterpret_cast<void*> (d), len);
        }
        
        template<typename T = void>
        inline typename std::add_const<T>::type * reading() const {
            return reinterpret_cast<typename std::add_const<T>::type*>((data + edge.read));
        }
        
        template<typename T = void>
        inline T* writing() const {
            return reinterpret_cast<T*>(const_cast<unsigned char*>(data + edge.write));
        }

        bool seek(signed long long pos);//read
        bool seekw(signed long long pos);//write

        bool write(const void *d, size_t len);
        size_t space() const;
        
        bool read(void *d, size_t len);
        size_t remaining() const;
        
        inline bool allRead() const {
            return this->remaining() == 0;
        }
        inline bool allWritten() const {
            return fixed && this->space() == 0;
        }
        inline bool complete() const {
            return fixed && written == edge.max;
        }

        bool ignore(size_t len);
        bool skip(size_t len);
        bool set(unsigned char val, size_t len);

        void clear();
        void zero();
        
        inline size_t readOffset() const {
            return edge.read;
        }
        inline size_t writeOffset() const {
            return edge.write;
        }
        
        inline unsigned char& at(const size_t& idx) {
            static unsigned char badChar = 0x0;
            if(idx > this->size() || data == nullptr) return badChar;
            else return data[idx];
        }
        inline unsigned char& operator[](const size_t& idx) {
            return this->at(idx);
        }
        inline const unsigned char& at(const size_t& idx) const {
            static const unsigned char badChar = 0x0;
            if(idx > this->size() || data == nullptr) return badChar;
            else return data[idx];
        }
        
        inline operator unsigned char*() {
            return data;
        }
        inline char* c_str() {
            return reinterpret_cast<char*>(data);
        }
        
        inline bool resize(size_t sz) {
            this->clear();
            return this->ensure(sz) && this->skip(sz);
        }

        unsigned char *data;
        struct {
            size_t read = 0;
            size_t write = 0;
            size_t max = 0;
            bool created = false;
        } edge;
        bool fixed, constant;
        size_t written;
        
        void dbgDump(const unsigned int columns = 32) const;
        
        static constexpr size_t allocMinSize = 512;
        inline static MemBuffer& Empty() {
            static MemBuffer emptyInstance = MemBuffer();
            emptyInstance.clear();
            return emptyInstance;
        }
    protected:
        size_t nextSize(size_t min);

    };

};

#endif /* MEMBUFFER_H */

