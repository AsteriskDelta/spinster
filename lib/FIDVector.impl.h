#ifndef FIDVECTOR_IMPL_H
#define FIDVECTOR_IMPL_H
#include "FIDVector.h"
#include <algorithm>

namespace Spinster {
    template<typename VT, typename IDT>
    VT FIDVector<VT,IDT>::nullValue = VT(nullptr);

    template<typename VT, typename IDT>
    FIDVector<VT,IDT>::FIDVector(size_t reserveSize) : data() {
        data.reserve(reserveSize);
    }

    template<typename VT, typename IDT>
    FIDVector<VT,IDT>::FIDVector(const FIDVector& orig) : data(orig.data), unusedIDs(orig.unusedIDs), freshIDs(orig.freshIDs) {
    }

    template<typename VT, typename IDT>
    FIDVector<VT,IDT>::~FIDVector() {
    }

    template<typename VT, typename IDT>
    SafeRef<VT> FIDVector<VT,IDT>::get(IDT id) {
        if(id >= data.size()) return nullptr;
        else return &data[id];
    }

    template<typename VT, typename IDT>
    IDT FIDVector<VT,IDT>::push() {
        return this->push(VT());
    }

    template<typename VT, typename IDT>
    IDT FIDVector<VT,IDT>::push(const VT& dat) {
        IDT newID;
        if(unusedIDs.size() > 0) {
            newID = unusedIDs.back();
            unusedIDs.pop_back();
            data[newID] = dat;
        } else {
            newID = data.size();
            data.push_back(dat);
        }
        //freshIDs.push_back(newID);
        return newID;
    }
    
    template<typename VT, typename IDT>
    bool FIDVector<VT,IDT>::insert(const IDT& id, const VT& dat) {
        if(id >= this->size()) {
            const auto prevSize = this->size();
            this->resize(id+1);
            for(auto i = prevSize; i < id; i++) {
                this->unusedIDs.push_back(i);
                data[i] = 0;
            }
        }
        data[id] = dat;
        return true;
    }
    
    template<typename VT, typename IDT>
    bool FIDVector<VT,IDT>::fresh(IDT id) {
        if(id >= data.size()) return false;
        else if(std::find(freshIDs.begin(), freshIDs.end(), id) == freshIDs.end()) {
            freshIDs.push_back(id);
            return true;
        } else return false;
    }
    
    template<typename VT, typename IDT>
    bool FIDVector<VT,IDT>::unfresh(IDT id) {
        if(id >= data.size()) return false;
        
        auto it = std::find(freshIDs.begin(), freshIDs.end(), id);
        if(it != freshIDs.end()) {
            freshIDs.erase(it);
            return true;
        } else return false;
    }
    
    template<typename VT, typename IDT>
    bool FIDVector<VT,IDT>::use(IDT id) {
        if(id >= data.size()) return false;
        
        auto it = std::find(unusedIDs.begin(), unusedIDs.end(), id);
        if(it == unusedIDs.end()) return true;
        else {
            unusedIDs.erase(it);
            return true;
        }
    }
    
    template<typename VT, typename IDT>
    bool FIDVector<VT,IDT>::unuse(IDT id) {
        if(id >= data.size()) return false;
        else if(std::find(unusedIDs.begin(), unusedIDs.end(), id) != unusedIDs.end()) return true;
        else {
            auto it = std::find(freshIDs.begin(), freshIDs.end(), id);
            if(it != freshIDs.end()) freshIDs.erase(it);
            unusedIDs.push_back(id);
            data[id] = VT(nullptr);
            return true;
        }
    }

    template<typename VT, typename IDT>
    bool FIDVector<VT,IDT>::erase(IDT id) {
        if(id >= data.size()) return false;
        
        this->unuse(id);
        return true;
    }

    template<typename VT, typename IDT>
    bool FIDVector<VT,IDT>::resize(IDT sz) {
        data.resize(sz);
        auto outOfRange = [sz](const IDT& id) -> bool {
            return id < sz;
        };
        std::remove_if(unusedIDs.begin(), unusedIDs.end(), outOfRange);
        std::remove_if(freshIDs.begin(), freshIDs.end(), outOfRange);
        return true;
    }

    template<typename VT, typename IDT>
    IDT FIDVector<VT,IDT>::size() const { //Total size
        return data.size();
    }

    template<typename VT, typename IDT>
    IDT FIDVector<VT,IDT>::count() const { //"In use" size
        return data.size() - unusedIDs.size();
    }
    
    template<typename VT, typename IDT>
    IDT FIDVector<VT,IDT>::available() const { //"Fresh" size
        return freshIDs.size();
    }
    
    template<typename VT, typename IDT>
    VT& FIDVector<VT,IDT>::getFresh() {
        if(freshIDs.size() == 0) return nullValue;
        else return data[freshIDs.back()];
    }
    
    template<typename VT, typename IDT>
    VT FIDVector<VT,IDT>::takeFresh() {
        if(freshIDs.size() == 0) return nullValue;
        else {
            const IDT retID = freshIDs.back();
            freshIDs.pop_back();
            VT ret = data[retID];
            this->erase(retID);
            return ret;
        }
    }
};

#endif /* FIDVECTOR_IMPL_H */

