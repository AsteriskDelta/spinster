#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H
#include <queue>
#include <vector>

namespace Spinster {

    template<class T, class C = std::vector<T>, class P = std::less<typename C::value_type> >
    struct PriorityQueue : std::priority_queue<T, C, P> {

        typename C::iterator begin() {
            return std::priority_queue<T, C, P>::c.begin();
        }

        typename C::iterator end() {
            return std::priority_queue<T, C, P>::c.end();
        }
    };

};
#endif /* PRIORITYQUEUE_H */

