#ifndef SAFEREF_H
#define SAFEREF_H
#include "SpinsterInclude.h"
#include <iostream>

#define SPIN_SAFE_NULL reinterpret_cast<T*>(_nullObject)

namespace Spinster {

    struct SafeRef_Root {
    };
    
    constexpr unsigned int _maxSafeObjectSize = 8 * 1024;
    extern unsigned char _nullObject[_maxSafeObjectSize];

    template<typename T>
    class SafeRef : public SafeRef_Root {
    public:
        typedef SafeRef<T> Self;
        typedef T Type;
        typedef T value_type;
        typedef T type;
        
        inline SafeRef() : obj(/*SPIN_SAFE_NULL*/nullptr) {
        };

        inline SafeRef(T& ref) : obj(&const_cast<T&> (ref)) {
        };

        inline constexpr SafeRef(const SafeRef<T>& orig) : obj(const_cast<T*> (orig.obj)) {
        };
        template<class=void>//Disambiguate for void/null ptrs
        inline constexpr SafeRef(const SafeRef<T> *origPtr) : obj(const_cast<T*> (origPtr->obj)) {
        };
        
        template<typename OT>
        inline constexpr SafeRef(typename std::enable_if<std::is_base_of<typename OT::Type, T>::type, const OT&>::type orig) : obj(const_cast<T*> (orig.obj)) {
        };

        inline constexpr SafeRef(T * const& rPtr) : obj((rPtr == nullptr) ? /*SPIN_SAFE_NULL*/nullptr : reinterpret_cast<T*> (rPtr)) {
        };

        inline ~SafeRef() {
        };

        template<typename TA, typename ...Args>
        inline SafeRef(typename std::enable_if<!std::is_pointer<TA>::type, const TA&>::type first, const Args&... args) : obj(Resolve<T>(first, args...)) {
        }
        
        inline SafeRef(std::nullptr_t nil) : obj(nullptr) {
            ((void)nil);
        }

        T* obj;

        inline T* operator->() const {
            return obj;
        };

        /*inline const T* operator->() const {
            return obj;
        };*/

        inline T& operator*() const {
            return *obj;
        };

        /*inline const T& operator*() const {
            return *obj;
        };*/

        inline explicit operator bool() const {
            return obj != SPIN_SAFE_NULL && obj != nullptr;
        }
        inline bool valid() const {
            return this->operator bool();
        }

        inline T* operator&() const {
            return this->operator->();
        };

        /*inline const T* operator&() const {
            return this->operator->();
        };*/
        
        inline explicit operator T*() const {
            return obj;
        }
        /*inline explicit operator const T*() const {
            return obj;
        }*/
        
        inline auto get() const {
            return const_cast<typename std::remove_const<T>::type *>(obj);
        }
        /*
        inline static void* operator new(std::size_t sz) {
            _unused(sz);
            //static_assert(sz == 1, "May not allocate an array of safe references at once");
            return new T();
        }
        inline static void operator delete(void *const ptr, std::size_t sz) {
            _unused(sz);
            //static_assert(sz == 1, "May not delete an array of safe references at once");
            if(ptr == nullptr || !bool(*reinterpret_cast<SafeRef<T>*>(ptr))) return;
            else {
                delete reinterpret_cast<SafeRef<T>*>(ptr);
            }
        }*/
        
        template<typename ...Args>
        inline void allocate(const Args&... args) {
            obj = new T(args...);
        }
        inline void deallocate() {
            if(bool(*this)) delete obj;
        }

        template<class = void> inline auto begin() {
            return obj->begin();
        };

        template<class = void> inline auto begin() const {
            return obj->begin();
        };

        template<class = void> inline auto end() {
            return obj->end();
        };

        template<class = void> inline auto end() const {
            return obj->end();
        };
        
        inline bool operator==(const Self& o) const {
            return obj == o.obj;
        }
        inline bool operator!=(const Self& o) const {
            return !(*this == o);
        }
        
        template<class=void>
        inline bool operator<(const Self& o) const {
            return *(*this) < *o;
        }
        
        template<typename ...Args>
        inline auto operator()(const Args&... args) {
            return (*obj).T::operator()(args...);
        }
    private:
        
    };
    //template<typename T> T SafeRef<T>::nullObject = T();
    template<typename T>
    using RRef = SafeRef<T>;

    template<typename genType>
    typename std::enable_if<std::is_base_of<::Spinster::SafeRef_Root, genType>::value, std::ostream&>::type
    inline operator<<(std::ostream& out, const genType& g) {
        if(!g) return out << "null";
        else return out << (*g);
    }

}

namespace std {

  template <typename ST>
  struct hash<Spinster::SafeRef<ST>>
  {
    std::size_t operator()(const Spinster::SafeRef<ST>& rr) const noexcept {
          return reinterpret_cast<std::size_t>(rr.obj);//hash<std::size_t>()(reinterpret_cast<std::size_t>(rr.obj));
    }
  };

}

#endif /* SAFEREF_H */

