#include "MemBuffer.h"
#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <sstream>

namespace Spinster {
    
    constexpr size_t MemBuffer::allocMinSize;
    
    static inline int nextPow2(int n) { 
        if ( n <= 1 ) return n;
        double d = n-1; 
        return 1 << ((((int*)&d)[1]>>20)-1022); 
    } 

    MemBuffer::MemBuffer() : data(nullptr), edge(), fixed(false), constant(false), written(0) {
    }
    
    MemBuffer::MemBuffer(void *const& dt, size_t len, bool fxd, bool cns) : 
    data(reinterpret_cast<unsigned char*>((dt)) ), edge(), fixed(fxd), constant(cns), written(len) {
        edge.max = len;
    }
    
    MemBuffer::MemBuffer(const void *const& dt, size_t len) : 
    data(reinterpret_cast<unsigned char*>(const_cast<void*>(dt)) ), edge(), fixed(true), constant(true), written(len) {
        edge.max = len;
    }

    MemBuffer::MemBuffer(const MemBuffer& orig) : data(nullptr), edge(), fixed(false), constant(false), written(0) {
        this->write(orig.data, orig.size());
        edge = orig.edge;
    }
    
    MemBuffer::MemBuffer(size_t len) : MemBuffer() {
        data = reinterpret_cast<unsigned char*>(malloc(len));
        edge.max = len;
        edge.created = true;
        fixed = true;
    }

    MemBuffer::~MemBuffer() {
        if ((edge.created || !fixed) && data != nullptr) free(data);
    }

    size_t MemBuffer::size() const {
        return edge.write;
    }
    size_t MemBuffer::maxSize() const {
        return edge.max;
    }
    
    size_t MemBuffer::remaining() const {
        if(edge.read > edge.write) return 0;
        return edge.write - edge.read;
    }
    size_t MemBuffer::space() const {
        if(edge.write > edge.max) return 0;
        return edge.max - edge.write;
    }

    bool MemBuffer::ensure(size_t minSize) {
        if(this->maxSize() >= minSize) return true;
        else if(this->fixed || this->constant) return false;
        
        size_t newSize = this->nextSize(minSize);
        unsigned char* newData = reinterpret_cast<unsigned char*>(malloc(newSize));
        if(newData == nullptr) return false;
        
        if(data != nullptr) {
            memcpy(newData, data, this->size());
            free(data);
        }
        data = newData;
        edge.created = true;
        edge.max = newSize;
        
        return true;
    }

    bool MemBuffer::seek(signed long long pos) {
        if(pos >= 0) {
            edge.read = std::min(static_cast<decltype(pos)>(edge.write), pos);
            if(static_cast<decltype(pos)>(edge.read) != pos) return false;
        } else if(pos < 0) {
            pos = edge.write - 1 + pos;
            edge.read = std::max(static_cast<decltype(pos)>(0), std::min(static_cast<decltype(pos)>(edge.write), pos));
            if(static_cast<decltype(pos)>(edge.read) != pos) return false;
        }
        return true;
    }
    bool MemBuffer::seekw(signed long long pos) {
        if(pos >= 0) {
            edge.write = std::min(static_cast<decltype(pos)>(edge.max), pos);
            if(static_cast<decltype(pos)>(edge.write) != pos) return false;
        } else if(pos < 0) {
            pos = edge.max - 1 + pos;
            edge.write = std::max(static_cast<decltype(pos)>(0), std::min(static_cast<decltype(pos)>(edge.max), pos));
            if(static_cast<decltype(pos)>(edge.write) != pos) return false;
        }
        return true;
    }

    bool MemBuffer::write(const void *d, size_t len) {
        if(!this->ensure(this->size() + len) || this->constant) return false;
        memcpy(this->writing<char>(), reinterpret_cast<const unsigned char*>(d), len);
        edge.write += len;
        written += len;
        return true;
    }
    bool MemBuffer::read(void *d, size_t len) {
        std::cout << "read " << len << " bytes\n";
        std::cout << "comp edges read=" << edge.read << ", write=" << edge.write << "\n";
        if(edge.read+len > edge.write) return false;
        memcpy(d, this->reading<char>(), len);
        edge.read += len;
        return true;
    }

    bool MemBuffer::ignore(size_t len) {
        const auto desEdge = edge.read+len;
        edge.read = std::min(edge.write, desEdge);
        return edge.read == desEdge;
    }
    bool MemBuffer::skip(size_t len) {
        const auto desEdge = edge.write+len;
        if(!this->ensure(desEdge)) return false;
        
        edge.write = desEdge;
        return true;
    }
    bool MemBuffer::set(unsigned char val, size_t len) {
         if(!this->ensure(this->size() + len) || this->constant) return false;
         memset(&data[edge.write], val, len);
         return true;
    }

    void MemBuffer::clear() {
        edge.write = edge.read = 0;
        written = 0;
    }
    void MemBuffer::zero() {
        if(this->maxSize() == 0 || this->constant) return;
        memset(data, 0x0, this->maxSize());
    }

    size_t MemBuffer::nextSize(size_t min) {
        if(min == 0) return 0;
        else if(this->maxSize() == 0) return std::max(allocMinSize, (size_t)nextPow2(min));
        
        size_t reqMult = 1 + (min / this->maxSize());
        return std::max(allocMinSize, (size_t)nextPow2(reqMult * this->maxSize()));
    }

    void MemBuffer::dbgDump(unsigned int columns) const {
        DBG_EXCL(std::cout << "membuffer dump, sz=" << this->size() << ":\n" << std::hex;
        for (unsigned int i = 0; i < this->size(); i++) {
            if (this->at(i) < 16) std::cout << 0;
            std::cout << std::hex << static_cast<uint16_t> (this->at(i)) << " ";
            if (i != 0 && i % columns == 0) std::cout << "\n";
        }
        std::cout << std::dec << "\n";);
    }
};