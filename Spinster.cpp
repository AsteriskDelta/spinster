#include "Spinster.h"
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <unistd.h>
#include <memory>
#include "Thread.h"
#include "MainThread.h"
#include "Routing.h"
#include "TaskSet.h"

//TEMP
#include "Net.h"
#include "NetLink.h"
#include "Forwarding.h"
#include "Protocol.h"
#include <mutex>

namespace Spinster {
    thread_local Task task;
    thread_local SafeRef<MemberInfo> info = nullptr;
    thread_local SafeRef<Obj::MemberLink> CurrentLink = nullptr;
    thread_local SafeRef<Message> CurrentMessage = nullptr;
    
    unsigned short LinkTimeout = 8, LinkTimeoutGrace = 4;
    
    _dalloc_pre() std::ios_base::Init _forceStreamInit;
    _dalloc_body() LocalGroup Local = new Obj::LocalGroup();
    _dalloc_post() thread_local Member member = &Local;
    
    std::mutex SpinningMutex;
    
    bool MemberOwned(const MemberID& id) {
        if(id.isLocal()) return true;
        
        auto grp = GetGroup(id);
        if(!grp) return false;
        else if(grp->owned()) return true;
        
        return false;
    }
    
    SafeRef<Obj::Member> GetMember(const MemberID& id) {
        //if(id.group == MemberID::LocalGroup && id.member == MemberID::LocalMember) return Local->asMember();
        auto grp = GetGroup(id);
        if(!grp) return nullptr;
        else {
            auto member = grp->get(id)->asMember();
            if(member) return member;
            else return grp->asMember();
        }
        //DBG_EXCL(std::cerr << "GetMember " << id << "\n");
        //return Member();
    }
    SafeRef<Obj::Group> GetGroup(const MemberID& id) {
        Obj::Group *grp = nullptr;
        if(id.group == MemberID::LocalGroup) grp = &Local;
        else {
            grp = &Routing::Get(id);
        }
        
        //DBG_EXCL(std::cerr << "GetGroup " << id << "\n");
        return grp;
    }
    
    bool AssignToMember(const MemberID& id, Task tsk) {
        //std::cout << "Assign to ID #" << id << "\n";
        return GetMember(id)->assign(tsk);
    }
    
    SymIDT RegisterSymbol(const MemberID &memb, const SymbolLookupIndex &lk, const SymbolLookupHandler &hand) {
        auto grp = GetGroup(memb);
        if(!grp) return SymIDT_None;
        /*
        if(lk.type == SymbolType::Function) {
            std::cout << "\tREGSYMBOL\n";
            //hand.constructArgs(nullptr, nullptr);
        }*/
        return grp->symbols()->add(lk, hand)->id;
    }
    SymbolEntry LookupSymbol(const MemberID& memb, const SymbolLookupIndex& idx) {
        auto grp = GetGroup(memb);
        //std::cout << "lookup on " << grp << " for " << idx.name << "\n";
        if(!grp || !grp->symbols()) return nullptr;
        
        return grp->symbols()->get(idx);
    }
    SymbolEntry LookupSymbol(const MemberID& memb, const SymIDT& id) {
        auto grp = GetGroup(memb);
        //std::cout << "lookup on " << grp << " for #" << id << "\n";
        if(!grp || !grp->symbols()) return nullptr;
        
        return grp->symbols()->at(id);
    }
    
    SymIDT LookupSymbolIDAddr(void *ptr) {
        SymbolEntry sym = Local->symbols()->addr(ptr);
        if(!sym) return SymIDT_None;
        else return sym->id;
    }
    
    std::mutex ControlThreadMtx, ControlThreadActiveMtx;
    int SpinsterDispatcher() {
        ControlThreadActiveMtx.lock();
        //DBG_EXCL(std::cout << "DISPATH THREAD BEGIN\n");
        while(!ControlThreadMtx.try_lock()) {
            //DBG_EXCL(std::cout << "DISPATCH THREAD LOOP\n");
            Spinster::Forward::Update();
            Dispatch();
            usleep(10);
        }
        //DBG_EXCL(std::cout << "DISPATH THREAD END\n");
        ControlThreadMtx.unlock();
        ControlThreadActiveMtx.unlock();
        return 0;
    }
    _expose(SpinsterDispatcher);
    
    void Initialize(const MemberID& id) {
        Local->raw.id = id;
        Initialize(id, MemberInfo(&Local->asMember()));
    }
    void Initialize(const MemberID& id, const MemberInfo& defInfo) {
        SpinningMutex.lock();
        Obj::SignalInit();
        int exitHandler = std::atexit(&Terminate);
        _unused(exitHandler);
        //std::cout << "Init got exitHandler=" << exitHandler << "\n";
        
        Routing::Initialize();
        
        Local->info = defInfo;
        Local->raw.id = id;
        Local->initialize();
        Protocol::Initialize();
        
        Routing::Add(&Local);
        //std::cout << "Spinster init... ";
        //std::cout.flush();
        
        ControlThreadMtx.lock();
        Local->thread(SpinsterDispatcher);
        while(ControlThreadActiveMtx.try_lock()) {//Dispatch until the dedicated thread engages
            Dispatch();
            ControlThreadActiveMtx.unlock();
            usleep(400);
        }
        //std::cout << "Init complete\n";
    }
    void Terminate() {
        //DBG_EXCL(std::cout << "Spinster terminate\n";);
        SpinningMutex.unlock();
        //Tell the dispatcher thread to quit, and wait until it does
        ControlThreadMtx.unlock();
        ControlThreadActiveMtx.lock();
        ControlThreadActiveMtx.unlock();
        
        Routing::Cleanup();
        delete &Local;
    }
    /*
    SafeRef<Obj::MemberLink> Connect(const Address& linkIden, const MemberID& forceID) {
        DBG_EXCL(std::cout << "connect to: " << linkIden << "\n");
        
        std::unique_ptr<Net::NetLink> lnk(new Net::NetLink());
        lnk->members[0] = &Local;
        if(forceID) {
            lnk->members[1] = Routing::Get(forceID, true);
        } else {
            std::cerr << "connect not passed id\n";
            return nullptr;
        }
        
        if(!lnk->open(linkIden, Net::LocalAddress()) ) {
            std::cerr << "Failed to connect to " << linkIden << "\n";
            return nullptr;
        }
        
        if(!Routing::AddLink(lnk.get()))  {
            std::cout << "couldn't add link!\n";
            return nullptr;
        }
        
        Obj::MemberLink *retLink = static_cast<Net::NetLink*>(lnk.get());
        lnk.release();
        return retLink;
    }*/
    SafeRef<Obj::MemberLink> Connect(const Address& linkIden, Address lcl) {
        //DBG_EXCL(std::cout << "connect to: " << linkIden << "\n");
        
        if(!linkIden) {//Server on lcl
            if(!lcl) lcl = Net::LocalAddress();
            
            std::unique_ptr<Net::NetLink> lnk(new Net::NetLink());
            lnk->members[0] = &Local;
  
            if(!lnk->open(linkIden, lcl) ) {
                std::cerr << "Failed to start forwarder on " << lcl << "\n";
                return nullptr;
            }
            
            Obj::MemberLink *retLink = static_cast<Net::NetLink*>(lnk.get());
            lnk.release();
            
            Forward::AddServer(retLink);
            return retLink;
        } else {//Normal endpoint-to-endpoint
            SafeRef<Obj::MemberLink> existing = Forward::Get(linkIden);
            if(existing) return existing;
            
            if(!lcl) lcl = Net::DataAddress();
            std::unique_ptr<Net::NetLink> lnk(new Net::NetLink());
            lnk->members[0] = &Local;
            
            if(!lnk->open(linkIden, lcl) ) {
                std::cerr << "Failed to start endpoint-comm on " << lcl << "\n";
                return nullptr;
            }
            
            Obj::MemberLink *retLink = static_cast<Net::NetLink*>(lnk.get());
            lnk.release();
            
            Forward::Add(linkIden, retLink);
            return retLink;
        }
    }
    
    Group Create(const GroupCredentials& creds) {
        auto grp = new Obj::Group(creds);
        auto grpLink = new Obj::MemberLink();
        grpLink->members[0] = &Local;
        grpLink->members[1] = grp;
        Routing::AddLink(grpLink);
        return grp;
    }
    /*
    Group Join(const GroupCredentials& creds) {
        return Join(new Obj::Group(creds));
    }*/
    
    Group DoJoin(GroupCredentials groupCreds) {
        for(auto& link : Forward::GetLinks()) {
            if(!link->other()) continue;
            
            MemberID resID = MemberID();
            {
                //auto invok = ;
                resID = Yield(&link->other()->invoke(&Protocol::Sym::GroupLookup, groupCreds));
            }
            if(resID) {
                bool setCreds = !Routing::Get(resID);
                Group ret = Routing::Get(resID, true);
                if(setCreds) {
                    ret->info.name = groupCreds.name;
                    ret->info.changed();
                }
                //std::cout << "JOIN GROUP " << ret->info.name << " at ID #" << resID << "\n";
                
                bool joined = Yield(&ret->invoke(Protocol::Sym::Join, resID));
                //std::cout << "\tJoin ret = " << joined << "\n";
                if(!joined) ret = nullptr;
                
                return ret;
            }
        }
        //std::cout << "FAILED TO JOIN GROUP\n";
        return nullptr;
    }
    Invokee<decltype(&DoJoin), GroupCredentials> Join(const GroupCredentials& groupCreds) {
        return Local->invoke(&DoJoin, groupCreds);//Copy ellided
    }
    Group Join(Group grp) {
        bool joined = Yield(&grp->invoke(Protocol::Sym::Join, grp->id()));
        //std::cout << "\tJoin grp ret = " << joined << "\n";
        if(!joined) grp = nullptr;
        
        return grp;
    }
    void Leave(Group grp) {
        grp->invoke(Protocol::Sym::Leave, grp->id())->noReturn();
    }
    
    void Trust(Group grp) {
        _unused(grp);
    }
    void CompleteTrust(Group grp) {
        _unused(grp);
    }
    
    struct Dispatch_CallInfo;
    bool Dispatch(Dispatch_CallInfo callInfo) {
        _unused(callInfo);
        Local->scheduler->dispatch();
        return true;
    }
    
    void Yield(TaskSet& tasks) {
        tasks.yield();
    }
    void YieldFor(const double& t) {
        thread->setStatus(MemberStatus::Yielded);
        usleep(std::round(t*1000*1000));
        thread->setStatus(MemberStatus::Executing);
    }
    
    static Obj::Group tgr;
    template<>Obj::Group& Resolve(const std::string& iden) {
        _unused(iden);
        //DBG_EXCL(std::cout << "resolve group " << iden << "\n");
        return tgr;
    }
    
    const MemberID& MyID() {
        return (member->id());
    }
    
    SafeRef<Obj::Member> MyMember() {
        return member;
    }
};
