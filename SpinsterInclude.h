#ifndef SPINSTERINCLUDE_H
#define SPINSTERINCLUDE_H
#include <cstdlib>
#include <string>
#include <type_traits>
#include <functional>

#include <mutex>

#include "SpinsterPP.h"

namespace detail {

    template<typename T> struct declval_helper {
        static T value;
    };

    template<typename T, typename Z, Z T::*MPtr>
    struct offset_helper {
        using TV = declval_helper<T>;
        char for_sizeof[
        (char *) & reinterpret_cast<char&> ((TV::value.*MPtr)) -
        (char *) & reinterpret_cast<char&> (TV::value)
        ];
    };

}

template <typename T>
using Bare = typename std::remove_cv<typename std::remove_reference<T>::type>::type;

template<typename T, typename Z, Z T::*MPtr>
constexpr int offset_of() {
    return sizeof (detail::offset_helper<T, Z, MPtr>::for_sizeof);
}

template<class U>
auto check(U const&) -> typename U::value_type;

namespace Spinster {
    template<typename T> T& Resolve(const std::string& iden);
    template<typename T> class SafeRef;
}


namespace std {

    template <class F>
    struct return_type;

    template <class R, class... A>
    struct return_type<R(*)(A...)> {
        typedef R type;
    };

    template <class R, class... A>
    struct return_type<R(*)(A..., ...)> {
        typedef R type;
    };

    template<typename T>
    struct function_traits;

    template<typename R, typename ...Args>
    struct function_traits<std::function<R(Args...)>> {
        typedef std::tuple<Args...> type;
        static const size_t nargs = sizeof...(Args);

        typedef R result_type;

        template <size_t i>
                struct arg {
            typedef typename std::tuple_element<i, std::tuple<Args...>>::type type;
        };
    };
    
    /*template <class T, class = void>
    struct is_complete: std::false_type {};

    template <class T>
    struct is_complete< T, decltype(void(sizeof(T))) > : std::true_type {};*/
    template< typename t >
typename std::enable_if< sizeof (t), std::true_type >::type
is_complete_fn( t * );

std::false_type is_complete_fn( ... );

template< typename t, bool value = decltype( is_complete_fn( (t *) nullptr ) )::value >
struct is_complete : std::integral_constant< bool, value > {};
};

#include "SafeRef.h"

#define DECL_NAMESPACE_PARENT(NAM, PAR) \
namespace NAM {\
    namespace $ = PAR;\
};
#define DECL_NAMESPACE_SPINSTER(NAM) DECL_NAMESPACE_PARENT(NAM, ::Spinster)

namespace Spinster {
    class MemBuffer;
    
    DECL_NAMESPACE_SPINSTER(Obj);
    DECL_NAMESPACE_SPINSTER(Links);
    DECL_NAMESPACE_SPINSTER(Tasks);
    DECL_NAMESPACE_SPINSTER(Net);
    DECL_NAMESPACE_SPINSTER(Routing);
    DECL_NAMESPACE_SPINSTER(Forward);
    DECL_NAMESPACE_SPINSTER(Protocol);

    struct MCopyable {
    };
    struct Serializable {
    };
    struct SynDeferred {
    };
    template<typename T>
        struct SynSub;
    
    typedef unsigned long SymIDT;
    constexpr const SymIDT SymIDT_None = 0xFFFFFFFF;
    class SymbolLookupHandler;
    class SymbolLookupIndex;
    class MemberID;
    class MemberInfo;
    class Message;
    class Packet;
    class SubPacket;
    class TaskSet;
    namespace Obj {
        class SymbolEntry;
        class Member;
        class LocalGroup;
        
        template<typename... Args> class Event;
        ///template<typename DATA_T, typename = std::enable_if<std::is_class<DATA_T>::value>> class Event<DATA_T, void>;
        //template<typename ...Args> class Event;
        
        class Group;
        class Scheduler;
        class Task;
        class Link;
        class MemberLink;
    };
    
    typedef SafeRef<Obj:: Member> Member;
    class LocalGroup;
    class Task;

    SymIDT RegisterSymbol(const MemberID& member, const SymbolLookupIndex& lk, const SymbolLookupHandler& hand);
    SafeRef<Obj::SymbolEntry> LookupSymbol(const MemberID& member, const SymbolLookupIndex& idx);
    SafeRef<Obj::SymbolEntry> LookupSymbol(const MemberID& member, const SymIDT& id);
    SymIDT LookupSymbolIDAddr(void *ptr);
    
    void MapSymbol(SymIDT from, SymIDT to);//Low-level calls ONLY- use at your own risk
    template<typename T>
    inline void MapSymbolAddr(const SymIDT& sID, T *const& ptr) {
        return MapSymbol(sID, LookupSymbolIDAddr(reinterpret_cast<void*>(ptr)));
    }

    const MemberID& MyID();
    SafeRef<Obj::Member> MyMember();
    
    bool MemberOwned(const MemberID& id);

    extern std::recursive_mutex dbgMtx;
    
    extern thread_local Task task;
    extern thread_local Member member;
    extern thread_local SafeRef<MemberInfo> info;
    extern thread_local SafeRef<Obj::MemberLink> CurrentLink;
    extern thread_local SafeRef<Message> CurrentMessage;
    extern unsigned short LinkTimeout, LinkTimeoutGrace;
    
    extern LocalGroup Local;
    extern SymIDT InitForceSymbolID;
    
    extern unsigned int CurrentActiveThreads, MaxActiveThreads, CurrentBusyThreads;
}

#define SPINSTER_SAFEREF_NULL(TYPE) 
//template<> Obj::TYPE SafeRef<Obj::TYPE>::nullObject;

#include <tuple>
#include <utility> 

namespace std {

    template<std::size_t I = 0, typename FuncT, typename... Tp>
    inline typename std::enable_if<I == sizeof...(Tp), void>::type
    for_each(std::tuple<Tp...> &, FuncT) // Unused arguments are given no names.
    {
    }

    template<std::size_t I = 0, typename FuncT, typename... Tp>
    inline typename std::enable_if<I < sizeof...(Tp), void>::type
    for_each(std::tuple<Tp...>& t, FuncT f) {
        f(std::get<I>(t));
        for_each < I + 1, FuncT, Tp...>(t, f);
    }
}

#define DBG_EXCL(...) {\
std::lock_guard<std::recursive_mutex> _mtxlck(::Spinster::dbgMtx);\
__VA_ARGS__ ;\
};

#include "Common.h"
#include "ProtoID.h"

#endif /* SPINSTERINCLUDE_H */

