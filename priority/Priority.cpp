#include "Priority.h"

namespace Spinster {
    
    
    std::string Priority::str() const {
        if(sublevel != 0) return "["+this->str_level()+":"+std::to_string(this->sublevel)+ "]";
        else return "["+this->str_level()+"]";
    }
    
    std::string Priority::str_level() const {
        switch(this->level) {
            case Invalid: return "Invalid";
            
            case Lowest: return "Lowest";
            case Low: return "Low";
            case Normal: return "Medium";
            case High: return "High";
            case Highest: return "Highest";
            
            case Realtime: return "Realtime";
            case Exclusive: return "Exclusive";
            case Immediate: return "Immediate";
            
            default:
                return "#"+std::to_string(this->level);
        }
    }
}