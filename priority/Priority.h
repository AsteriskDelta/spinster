#ifndef SPINSTER_PRIORITY_H
#define SPINSTER_PRIORITY_H
#include "SpinsterInclude.h"

namespace Spinster {

    struct Priority : public Common, public MCopyable {
        enum Level : char {
            Invalid = 0,
            Lowest = 1,
            Low = 2,
            Normal = 3, Medium = 3,
            High = 4,
            Highest = 5,
            
            Realtime = 100,
            OverrideThreadLimits = 112,
            Exclusive = 126,
            Immediate = 127
        };
        enum Flags : char {
            Spoils = 0b1,
            Reliable =  0b10,
            Ordered =   0b100,
            Replies =   0b1000,
            Lazy =      0b10000,
            Mortal =    0b100000,
            DefaultFlags = Reliable | Ordered | Replies | Mortal,
            MaskFlags = 127
        };
        short sublevel;
        Level level;
        union {
            struct {
                bool spoils : 1;
                bool reliable : 1;
                bool ordered : 1;
                bool expectReply : 1;
                bool lazy : 1;
                bool mortal : 1;
                char padd : 2;
            } flags;
            unsigned char flags_raw;
        };
        
        inline constexpr Priority() : sublevel(0), level(Invalid), flags_raw(DefaultFlags) {};
        inline constexpr Priority(const Level& lvl, const unsigned char& flg = DefaultFlags, const short& sub = 0) : sublevel(sub), level(lvl), flags_raw(flg) {};
        
        inline bool operator<(const Priority& o) const {
            return level < o.level || (level == o.level && sublevel < o.sublevel);
        }
        inline bool operator>(const Priority& o) const {
            return level > o.level || (level == o.level && sublevel > o.sublevel);
        }
        inline bool operator==(const Priority& o) const {
            return level == o.level && sublevel == o.sublevel && (flags_raw & MaskFlags) == (o.flags_raw & MaskFlags);
        }
        inline bool operator!=(const Priority& o) const { return !(*this == o); };
        
        inline void set(const unsigned char flg, bool state) {
            if(state) flags_raw |= flg;
            else flags_raw &= ~flg;
        }
        inline void set(const Level& l) { level = l; };
        
        inline void setLifetime(short ms) {
            sublevel = ms;
            this->set(Spoils, true);
        }
        inline bool reliable() const { return flags.reliable; };
        inline bool ordered() const { return flags.ordered; };
        inline bool mortal() const { return flags.mortal; };
        inline bool immortal() const { return !this->mortal(); };
        inline bool spoils() const { return flags.spoils; };
        inline bool realtime() const { return char(level) > char(OverrideThreadLimits); };
        
        inline bool expectsReply() const { return flags.expectReply; }
        inline void noReply() { flags.expectReply = false; };
        inline void needReply() { flags.expectReply = true; };
        
        inline short lifetime() const { return this->spoils()? sublevel : -1; };
        
        inline bool lazy() const { return flags.lazy; };
        
        std::string str_level() const;
        std::string str() const;
        
        inline constexpr static Priority Invoked() {
            return Priority(Realtime, DefaultFlags, 0x0);
        }
        inline constexpr static Priority Loop() {
            return Priority(Exclusive, DefaultFlags, 0x0);
        }
    };
};

#endif /* PRIORITY_H */

