#ifndef PRIORITYHOLDER_H
#define PRIORITYHOLDER_H
#include "SpinsterInclude.h"
#include "Priority.h"

namespace Spinster {

class PriorityHolder {
public:
    Priority priority;
    
    inline PriorityHolder() : priority() {};
    inline PriorityHolder(const Priority& p) : priority(p) {};
    inline PriorityHolder(const PriorityHolder& orig) : priority(orig.priority) {};
    //inline ~PriorityHolder() {};
private:

};

};

#endif /* PRIORITYHOLDER_H */

