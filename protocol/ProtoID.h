#ifndef PROTOID_H
#define PROTOID_H

namespace Spinster {
    namespace Protocol {
        enum {
            Invalid = 0,
            Redirect,
            Introduce,
            Disconnect,
            SymLookup,
            Reply,
            Delta,
            Ping,
            Ack,
            Nack,
            Ignore,
            GroupLookup,
            Join,
            Leave,
            Solicit,
            Route
        };
    }
}

#endif /* PROTOID_H */

