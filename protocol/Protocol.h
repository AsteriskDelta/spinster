#ifndef PROTOCOL_H
#define PROTOCOL_H
#include "SpinsterInclude.h"
#include "ProtoID.h"
#include "GroupCredentials.h"
#include "MemberID.h"

//#include "Introduction.h"
//#include "Disconnection.h"
//#include "SymLookup.h"
//#include "Message.h"

namespace Spinster {
    namespace Protocol {
        void Initialize();
        namespace Sym {
            bool Reply(unsigned int msgID, MemBuffer repBuffer);
            int Ping();
            int Invalid();
            
            void* Redirect(unsigned short port);
            void* Introduce();
            
            bool Disconnect();
            
            bool Ack(unsigned int packetID);
            bool Nack(unsigned int packetID);
            bool Ignore(unsigned int packetID);
            
            SymIDT SymLookup(SymbolLookupIndex index);
            
            MemberID GroupLookup(GroupCredentials creds);
            
            bool Join(MemberID groupID);
            bool Leave(MemberID groupID);
            bool Solicit(MemberID targetID, MemberID srcID);
            
            bool Route(MemberID target);
        }
    }
}


#endif /* PROTOCOL_H */

