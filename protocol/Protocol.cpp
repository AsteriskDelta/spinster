#include "Protocol.h"
#include "MemberLink.h"
#include "Address.h"
#include "Forwarding.h"
#include "Group.h"
#include "LocalGroup.h"
#include "Routing.h"
#include "NGraph.h"

namespace Spinster {
    namespace Protocol {
        void Initialize() {
            $::MapSymbolAddr(Invalid,       &Sym::Invalid);
            $::MapSymbolAddr(Introduce,     &Sym::Introduce);
            $::MapSymbolAddr(Disconnect,    &Sym::Disconnect);
            $::MapSymbolAddr(Redirect,      &Sym::Redirect);
            $::MapSymbolAddr(Reply,         &Sym::Reply);
            $::MapSymbolAddr(Ping,          &Sym::Ping);
            $::MapSymbolAddr(SymLookup,     &Sym::SymLookup);
            $::MapSymbolAddr(Ack,           &Sym::Ack);
            $::MapSymbolAddr(Nack,          &Sym::Nack);
            $::MapSymbolAddr(Ignore,        &Sym::Ignore);
            $::MapSymbolAddr(GroupLookup,   &Sym::GroupLookup);
            $::MapSymbolAddr(Join,          &Sym::Join);
            $::MapSymbolAddr(Leave,         &Sym::Leave);
            $::MapSymbolAddr(Solicit,       &Sym::Solicit);
            $::MapSymbolAddr(Route,         &Sym::Route);
        }
        
        namespace Sym {
            bool Reply(Message::Idt msgID, MemBuffer repBuffer) {
                std::cout << "GOT REPLY for " << msgID << ":\n";
                repBuffer.dbgDump();
                //std::cout << "END REPLY\n";
                
                $::Message *msg = $::CurrentLink->tx->getMessage(msgID);
                if(msg == nullptr) {
                    std::cerr << "null message for given id\n";
                    return false;
                }
                
                if(msg->remoteTask != nullptr) msg->remoteTask->onReturn(repBuffer.c_str());
                $::CurrentLink->tx->returned(msgID);
                return true;
            }
            
            int Ping() {
                int val = rand();
                std::cout << "\n++got pinged! " << val << "\n";
                return val;
            }
            
            int Invalid() {
                return -1;
            }
            
            void* Redirect(unsigned short port) {
                $::Address addr = $::CurrentLink->address();
                addr.ip.port = port;
                std::cout << "INVOKED REDIRECT SYM with port=" << port << " for addr " << addr << "\n";
                $::CurrentLink->redirect(addr);
                $::CurrentLink->setEndpointID($::CurrentMessage->from());
                
                $::CurrentLink->other()->addLink($::CurrentLink);
                
                $::CurrentLink->setIntroduced(true);
                Forward::Complete($::CurrentLink);
                return nullptr;
            }
            void* Introduce() {
                
                return nullptr;
            }
            
            bool Disconnect() {
                std::cout << "Got disconnection request from " << $::CurrentMessage->from() << "\n";
                if($::CurrentMessage->from() != $::CurrentLink->other()->id()) {
                    std::cerr << "\tdisconnection ID doesn't match link ID=" << $::CurrentLink->other()->id() << "\n";
                    return false;
                }
                
                $::CurrentLink->shouldClose();
                return true;
            }
            
            bool Ack(unsigned int packetID) {
                std::cout << "\n\nGOT ACK FOR " << packetID << "\n\n";
                $::CurrentLink->tx->setPacketState(packetID, Packet::Recieved);
                return true;
            }
            bool Nack(unsigned int packetID) {
                std::cout << "\n\nGOT N-ACK FOR " << packetID << "\n\n";
                $::CurrentLink->tx->setPacketState(packetID, Packet::Missing);
                return true;
            }
            bool Ignore(unsigned int packetID) {
                $::CurrentLink->rx->setPacketState(packetID, Packet::Ignored);
                _unused(packetID);
                return true;
            }
            
            SymIDT SymLookup(SymbolLookupIndex index) {
                std::cout << "\tSymLookup given " << index.str() << "\n";
                SymIDT ret = $::Local->symbols()->getID(index);
                std::cout << "\t\tgot id=" << ret << "\n";
                return $::Local->symbols()->getID(index);
            }
            
            MemberID GroupLookup(GroupCredentials creds) {
                MemberID ret;// = MemberID();
                std::cout << "\tGroupLookup \"" << creds.name << "\"\n";
                ret = Routing::Network()->getByName(creds.name);
                return ret;//Not found, let them know
            }
            
            
            bool Join(MemberID groupID) {
                std::cout << "\t"<<$::CurrentMessage->from() << " wishes to join group #" << groupID << "\n";
                auto group = $::Routing::Get(groupID);
                if(!group || !group->owned()) return false;
                
                group->addMember($::Routing::Get($::CurrentMessage->from()));
                
                return true;
            }
            bool Leave(MemberID groupID) {
                std::cout << "\t"<<$::CurrentMessage->from() << " wishes to LEAVE group #" << groupID << "\n";
                auto group = $::Routing::Get(groupID);
                if(!group || !group->owned()) return false;
                
                group->removeMember($::Routing::Get($::CurrentMessage->from()));
                return true;
            }
            bool Solicit(MemberID targetID, MemberID srcID) {
                _unused(targetID, srcID);
                return false;
            }
            
            bool Route(MemberID target) {
                if(Routing::LinkTo(target)) return true;
                else return false;
            }
            
            _expose(Reply, Ping, Invalid, Disconnect, Redirect, Introduce, Disconnect, 
                    Ack, Nack, Ignore, SymLookup, GroupLookup, Join, Leave, Solicit
                    );
            _expose(Route);
        }
    }
}