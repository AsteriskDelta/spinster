#ifndef SCHEDULER_H
#define SCHEDULER_H
#include "SpinsterInclude.h"
#include "SafeRef.h"
#include "Member.h"
#include <mutex>
#include <queue>
#include <vector>
#include <unordered_set>
#include "PriorityQueue.h"

namespace Spinster {
    namespace Obj {
        class Task;
        class Group;
        class TaskGroup;
        
        class Scheduler {
        public:
            Scheduler();
            Scheduler(SafeRef<Group> grp);
            virtual ~Scheduler();

            virtual bool schedule(SafeRef<Task> task, SafeRef<TaskGroup> taskGroup = nullptr);
            virtual void dispatch();
            virtual bool waitFor($::Member member);
            virtual SafeRef<Task> take($::Member member);
            
            virtual void forceNotify();
            
            //virtual void addIdle($::Member member);
            //virtual void removeIdle($::Member member);
            virtual void updateMemberStatus($::Member member,const MemberStatus &status, const MemberStatus& oldStatus);
            virtual bool addThreads(int cnt);
            
            SafeRef<TaskGroup> defaultTaskGroup();
            SafeRef<TaskGroup> reservedTaskGroup();
            SafeRef<TaskGroup> eventTaskGroup();
            
            SafeRef<TaskGroup> topTaskGroup();
            
            SafeRef<Group> group;
            std::recursive_mutex inUseMtx;
            //PriorityQueue<SafeRef<Task>> queue;
            std::vector<SafeRef<TaskGroup>> taskGroups;
            
            std::unordered_set<$::Member> idles, workers, sleepers, yielders, wakers, reserved, unqualified;
            std::unordered_set<$::Member>* getStatusGroup(const MemberStatus& status);
            
            inline unsigned int size() const {
                return count;
            }
            inline bool empty() const {
                return count == 0;
            }
        protected:
            void constructBaseTaskGroups();
            
            std::mutex iteratingMtx;
            unsigned int count;
            unsigned int indexOf(SafeRef<TaskGroup> taskGroup);
            unsigned int firstTaskGroup();
        };
    };
    
    class Scheduler : public SafeRef<Obj::Scheduler> {
    public:
        typedef SafeRef<Obj::Scheduler> Parent;
        inline Scheduler() : Parent() {};
        using Parent::Parent;
    };
    SPINSTER_SAFEREF_NULL(Scheduler);
};

#endif /* SCHEDULER_H */

