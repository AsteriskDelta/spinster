#include "MainThread.h"
#include "MemberID.h"
#include "MemberStatus.h"
#include "Spinster.h"

namespace Spinster {
    namespace Obj {

        MainThread::MainThread() {
            Member::raw.id = MemberID(MemberID::LocalGroup, MemberID::MainThread);
            Member::setStatus(MemberStatus::MainThread);
            Member::setTask(nullptr);
            Local->addMember(this);
        }

        MainThread::~MainThread() {
        }
        SafeRef<$::Obj::LocalGroup> MainThread::local() {
            return Local;
        }
        SafeRef<$::Obj::Group> MainThread::parent() {
            return &Local;
        }
    }
}


