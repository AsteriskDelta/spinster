#include "LocalTask.h"


namespace Spinster {
    namespace Obj {

        LocalTask::LocalTask() {
        }

        LocalTask::LocalTask(const LocalTask& orig) : Task(orig) {
        }

        LocalTask::~LocalTask() {
            //DBG_EXCL(std::cout << "DELETE local task @ " << this << "\n";);
        }

        bool LocalTask::execute() {
            //DBG_EXCL(std::cout << "Executing local task @ " << this << "\n";);
            static thread_local unsigned char rawOutBuff[8192];
            void *outPtr = reinterpret_cast<void*>(rawOutBuff);
            (this->invocation)(outPtr);
            return true;
        }
    };
};
