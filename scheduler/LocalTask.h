#ifndef LOCALTASK_H
#define LOCALTASK_H
#include "SpinsterInclude.h"
#include "SafeRef.h"
#include "Task.h"

namespace Spinster {
    namespace Obj {

        class LocalTask : public Task {
        public:
            LocalTask();
            LocalTask(const LocalTask& orig);
            virtual ~LocalTask();
            
            virtual bool execute() override;
            std::function<void(void*)> invocation;
        private:

        };
    };
    class LocalTask : public SafeRef<Obj::LocalTask> {
    public:
        typedef SafeRef<Obj::LocalTask> Parent;
        inline LocalTask() : Parent() {};
        using Parent::Parent;
        
    };
};

#endif /* LOCALTASK_H */

