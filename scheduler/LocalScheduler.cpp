#include "LocalScheduler.h"
#include "Thread.h"
#include "Group.h"
#include "LocalGroup.h"
#include "SynGlobal.h"
#include <chrono>
#include <cstdlib>
#include <unistd.h>
using namespace std::chrono_literals;

namespace Spinster {
    unsigned int MaxActiveThreads = 24;
    unsigned int MaxTotalThreads = 64;
    unsigned int CurrentActiveThreads = 1;
    unsigned int CurrentBusyThreads = 1;
    unsigned int CurrentTotalThreads = 1;
    
    static std::mutex LclMetaMtx;
    
    _global(MaxActiveThreads, MaxTotalThreads, CurrentActiveThreads, CurrentBusyThreads, CurrentTotalThreads);
    
    namespace Obj {

        LocalScheduler::LocalScheduler(SafeRef<Group> grp) : Scheduler(grp) {
        }

        LocalScheduler::~LocalScheduler() {
            for(auto& thr : ownedThreads) {
                thr->destroy(true);
                std::lock_guard<std::mutex> lck(dispatchMtx);
                dispatchVar.notify_all();
            }
            //{
            //    std::lock_guard<std::mutex> lck(dispatchMtx);
            //    dispatchVar.notify_all();
            //}//Ensure lck falls out of scope before threads try to lock it again
            for(auto& thr : ownedThreads) {
                std::lock_guard<std::mutex> lck(dispatchMtx);
                dispatchVar.notify_all();
                delete thr;
            }
        }
        
        
        bool LocalScheduler::schedule(SafeRef<Task> task, SafeRef<TaskGroup> taskGroup) {
            if(!Scheduler::schedule(task, taskGroup)) return false;
            //DBG_EXCL(std::cout << "localscheduler got " << task << "\n");
            std::lock_guard<std::mutex> lck(dispatchMtx);
            dispatchVar.notify_one();
            //DBG_EXCL(std::cout << "notified...\n");
            return true;
        }
        
        void LocalScheduler::dispatch() {
            Scheduler::dispatch();
            //DBG_EXCL(std::cout<<"TRIGGER DISPATCH\n";);
            bool shouldRecurse = false;
            std::lock_guard<std::recursive_mutex> useLck(this->inUseMtx);
            
            /*DBG_EXCL(std::cout << "q sizes:\n";
            std::cout << "\tidle="<<idles.size() << "\n";
            std::cout << "\tworkers="<<workers.size() << "\n";
            std::cout << "\twakers="<<wakers.size() << "\n";
            std::cout << "\tsleepers="<<sleepers.size() << "\n";
            );*/
            
            CurrentTotalThreads = this->group->size();
            CurrentBusyThreads = CurrentTotalThreads - this->reserved.size() - this->sleepers.size() - this->idles.size();//this->workers.size();
            CurrentActiveThreads = this->workers.size()  + this->wakers.size() + this->idles.size();
            
            for(unsigned int i = 0; i < this->taskGroups.size(); i++) {
                SafeRef<TaskGroup> taskGroup = taskGroups[i];
                if(!(*taskGroup)) continue;
                
                SafeRef<Task> nextTask = taskGroup->top();
                
                if(idles.size() == 0 && CurrentActiveThreads < MaxActiveThreads) {
                    //DBG_EXCL(std::cout << "CREATE THREAD\n");
                    this->addThreads(std::min(MaxActiveThreads - CurrentActiveThreads, static_cast<unsigned int>(this->size())));
                } else if(idles.size() > 0) {
                    std::lock_guard<std::mutex> lck(dispatchMtx);
                    dispatchVar.notify_one();
                } else if(nextTask->priority.realtime() && CurrentTotalThreads < MaxTotalThreads) {//We *need* this to run right away, so fire up a thread and damn the consequences
                    this->addThreads(1);
                    std::lock_guard<std::mutex> lck(dispatchMtx);
                    dispatchVar.notify_one();
                    shouldRecurse = true;
                }
            }
            
            if(!wakers.empty()) {
                $::Member prevWaker = nullptr;
                //iteratingMtx.lock();
                for(auto it = wakers.begin(); it != wakers.end(); ++it) {
                    $::Member waker = *it;
                    if(waker == prevWaker) {
                        shouldRecurse = true;
                        break;//Iterators broken by other thread
                    }
                    waker->setActive(true);//Force a wakeup call, in case our original signal was missed (it happens...)
                    prevWaker = waker;
                }
                //iteratingMtx.unlock();
            }
            
            //If we have too many, put them to sleep
            if(CurrentActiveThreads > MaxActiveThreads) {
                std::unique_lock<std::mutex> lck(dispatchMtx);
                dispatchVar.notify_one();
            }
            
            _unused(shouldRecurse);
            //if(shouldRecurse) this->dispatch();

            /*for(auto it = this->queue.begin(); it != this->queue.end() ++it) {
                $::Task task = *it;
                
                break;
            }*/
        }
        
        bool LocalScheduler::addThreads(int cnt) {
            if(cnt == -1) cnt = $::MaxActiveThreads - $::CurrentActiveThreads;
            for(int i = 0; i < cnt; i++) {
                std::lock_guard<std::recursive_mutex> useLck(this->inUseMtx);
                if(sleepers.size() > 0) {//Wake existing thread
                    //DBG_EXCL(std::cout << "WAKING THREAD\n");
                    $::Member sleepingThread = *sleepers.begin();
                    sleepingThread->setStatus($::MemberStatus::WakingUp);
                    sleepingThread->setActive(true);
                    sleepers.erase(sleepingThread);
                    wakers.insert(sleepingThread);
                } else {//New thread
                    //DBG_EXCL(std::cout << "CONSTRUCTING THREAD\n");
                    CurrentTotalThreads++;
                    auto newThread = new Thread();
                    ownedThreads.push_back(newThread);
                    this->group->addMember($::Group(static_cast<Obj::Group*>(newThread)));
                }
            }
            return true;
        }
        
        Thread* LocalScheduler::createThread() {
            CurrentTotalThreads++;
            auto newThread = new Thread();
            ownedThreads.push_back(newThread);
            this->group->addMember($::Group(static_cast<Obj::Group*>(newThread)));
            return newThread;
        }
        
        void LocalScheduler::forceNotify() {
            //std::lock_guard<std::mutex> lck(dispatchMtx);
            //bool locked = dispatchMtx.try_lock();
            dispatchVar.notify_all();
            //if(locked) dispatchMtx.unlock();
        }

        bool LocalScheduler::waitFor($::Member member) {
            //DBG_EXCL(std::cout << "waiting for task on " << $::thread << "\n");
            std::unique_lock<std::mutex> lck(dispatchMtx);
            if (CurrentActiveThreads > MaxActiveThreads) {
                //Make sure we're not freezing a realtime task when we need to run right away
                if(this->empty() || (this->topTaskGroup() && this->topTaskGroup()->top()->priority.realtime())) {
                    DBG_EXCL(std::cout << "Too many threads, putting self to sleep\n");
                    member->setActive(false);
                    return false;
                }
            }
            
            std::cv_status stt = dispatchVar.wait_for(lck, 1000ms/*, []()->bool{usleep(1000*1000);return true; }*/);// == std::cv_status::no_timeout;
            return stt != std::cv_status::timeout;
            //DBG_EXCL(std::cout << member << " waited for new task, got " << member->task() << "\n");
            //return true;
        }
    };
};
