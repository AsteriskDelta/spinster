#ifndef REMOTESCHEDULER_H
#define REMOTESCHEDULER_H
#include "SpinsterInclude.h"
#include "SafeRef.h"
#include "Scheduler.h"
#include <mutex> 
#include <condition_variable>

namespace Spinster {
    namespace Obj {
        class Group;
        class Thread;
        
        class RemoteScheduler : public Scheduler {
        public:
            RemoteScheduler(SafeRef<Group> grp);
            virtual ~RemoteScheduler();
            
            virtual bool schedule(SafeRef<Task> task, SafeRef<TaskGroup> taskGroup = nullptr) override;
            virtual void dispatch() override;
        private:
        };
    };
    
    class RemoteScheduler : public SafeRef<Obj::RemoteScheduler> {
    public:
        typedef SafeRef<Obj::RemoteScheduler> Parent;
        inline RemoteScheduler() : Parent() {};
        using Parent::Parent;
    };
    
    SPINSTER_SAFEREF_NULL(RemoteScheduler);
};
#endif /* REMOTESCHEDULER_H */

