#ifndef MAINTHREAD_H
#define MAINTHREAD_H

#include "SpinsterInclude.h"
#include "Group.h"
#include "TaskGroup.h"
#include <pthread.h>
#include <mutex>
#include <condition_variable>

namespace Spinster {
    
    namespace Obj {
        class TaskGroup;
        void OnSignal(int sig);
        void SignalInit();
        class Task;

        class MainThread : public Group {
        public:
            MainThread();
            virtual ~MainThread();
            
            virtual SafeRef<$::Obj::LocalGroup> local();
            virtual SafeRef<$::Obj::Group> parent();
            inline virtual SafeRef<Obj::Task> task() {
                return nullptr;
            }
            
            inline virtual bool busy() const {
                return true;
            }
        };
    };
    
    
    typedef SafeRef<Obj::MainThread> MainThread;
};

#endif /* MAINTHREAD_H */

