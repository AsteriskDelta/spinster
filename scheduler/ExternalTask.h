#ifndef EXTERNALTASK_H
#define EXTERNALTASK_H
#include "SpinsterInclude.h"
#include "SafeRef.h"
#include "Task.h"
//#include "LocalTask.h"

namespace Spinster {
    class Message;
    namespace Obj {
        class SymbolEntry;

        class ExternalTask : public Task {
        public:
            ExternalTask();
            virtual ~ExternalTask();
            
            virtual bool execute() override;
            
            SafeRef<Message> message;
            SafeRef<SymbolEntry> entry;
        private:

        };
    }
    typedef SafeRef<Obj::ExternalTask> ExternalTask;
}

#endif /* EXTERNALTASK_H */

