#include "RemoteScheduler.h"
#include "Thread.h"
#include "Group.h"
#include "LocalGroup.h"
#include "SynGlobal.h"
#include <chrono>
#include <cstdlib>
#include <unistd.h>
using namespace std::chrono_literals;

namespace Spinster {
    namespace Obj {

        RemoteScheduler::RemoteScheduler(SafeRef<Group> grp) : Scheduler(grp) {
        }

        RemoteScheduler::~RemoteScheduler() {
            
        }
        
        
        bool RemoteScheduler::schedule(SafeRef<Task> task, SafeRef<TaskGroup> taskGroup) {
            if(!Scheduler::schedule(task, taskGroup)) return false;
            return true;
        }
        
        void RemoteScheduler::dispatch() {
            Scheduler::dispatch();
            bool shouldRecurse = false;
            std::lock_guard<std::recursive_mutex> useLck(this->inUseMtx);

            for(unsigned int i = 0; i < this->taskGroups.size(); i++) {
                SafeRef<TaskGroup> taskGroup = taskGroups[i];
                if(!(*taskGroup)) continue;
                
                SafeRef<Task> nextTask = taskGroup->top();
                
                while(idles.size() > 0 && nextTask) {
                    (*idles.begin())->assign(nextTask);
                    nextTask = taskGroup->take();
                }
            }
            _unused(shouldRecurse);
            //if(shouldRecurse) this->dispatch();

            /*for(auto it = this->queue.begin(); it != this->queue.end() ++it) {
                $::Task task = *it;
                
                break;
            }*/
        }
    };
};