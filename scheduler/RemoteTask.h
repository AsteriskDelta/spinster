#ifndef REMOTETASK_H
#define REMOTETASK_H
#include "SpinsterInclude.h"
#include "SafeRef.h"
#include "Task.h"
#include "MemBuffer.h"
#include "MemberID.h"

namespace Spinster {
    //class Message;
    namespace Obj {
        class MemberLink;
        
        class RemoteTask : public Task {
        public:
            RemoteTask();
            RemoteTask(const RemoteTask& orig);
            virtual ~RemoteTask();
            
            SafeRef<MemBuffer> data;
            MemberID target;
            std::function<void(void*)> onReturn;
            Message *cachedMessage;
            
            virtual Message* constructMessage() override;
            virtual MemberID& targetID() override;
            virtual bool sendMessage(SafeRef<MemberLink> link) override;
        private:
            bool msgConstructed;//Check if we should really delete data
        };
    };
    
    class RemoteTask : public SafeRef<Obj::RemoteTask> {
    public:
        typedef SafeRef<Obj::RemoteTask> Parent;
        inline RemoteTask() : Parent() {};
        using Parent::Parent;
        
    };
};

#endif /* REMOTETASK_H */

