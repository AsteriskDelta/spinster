#include "ExternalTask.h"
#include "Invocation.h"
#include "Message.h"
#include "MemberPipe.h"
#include "MemberLink.h"

namespace Spinster {
    namespace Obj {

        ExternalTask::ExternalTask() : message(nullptr), entry(nullptr) {
        }

        ExternalTask::~ExternalTask() {
        }
        
        bool ExternalTask::execute() {
            $::CurrentLink = this->message->owner->parent;
            $::CurrentMessage = this->message;
            bool ret = HandleSymInvocation(this->entry, this->message);
            return ret;
        }

    };
};