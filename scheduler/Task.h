#ifndef TASK_H
#define TASK_H
#include "SpinsterInclude.h"
#include "SafeRef.h"
#include "Priority.h"

namespace Spinster {
    namespace Tasks {
        namespace ID {
            enum Status {
                Invalid = 0,
                Queued,
                Active,
                ExclusiveActive,
                Failed,
                Succeeded,
                Special,
                SpecialWaiting,
                Dispatched,
                Cancelled,
                Constructed
            };
        };
        using namespace ID;
    }
    namespace Obj {
        class Member;
        class SymbolEntry;
        class TaskGroup;
        class MemberLink;

        class Task : public $::Common {
        public:
            Task();
            Task(const Task& orig);
            virtual ~Task();
            
            inline Task(const Tasks::ID::Status& stat) : sender(nullptr), priority(), status(stat) {};
            
            SafeRef<Member> sender;
            $::Priority priority;
            $::Tasks::ID::Status status;
            SafeRef<Member> worker;
            SafeRef<TaskGroup> taskGroup;
            SymIDT symbolID;
            
            std::string str() const;
            
            virtual void setWorker(SafeRef<Member> thr);
            virtual void setTaskGroup(SafeRef<TaskGroup> grp);
            
            virtual bool execute();
            virtual void complete(bool succeeded);
            virtual bool cancel(bool recursive = false);
            
            virtual Message* constructMessage();
            virtual bool sendMessage(SafeRef<MemberLink> link);
            
            void setStatus(const $::Tasks::ID::Status& stat);
            void setPriority(const $::Priority& pr);
            
            SafeRef<SymbolEntry> symbol() const;
            
            virtual MemberID& targetID();
            
            inline bool completed() const {
                return status == Tasks::ID::Invalid || status == Tasks::ID::Failed || status == Tasks::ID::Succeeded || status == Tasks::ID::Cancelled;
            }
            inline bool succeeded() const {
                return status == Tasks::ID::Succeeded;
            }
            inline bool valid() const {
                return status != Tasks::ID::Invalid && status != Tasks::ID::Failed && status != Tasks::ID::Cancelled;;
            }
            
            inline bool active() const {
                return status == Tasks::ID::Active || status == Tasks::ID::ExclusiveActive;
            }
            
            inline bool exclusive() const {
                return status == Tasks::ID::ExclusiveActive;
            }
            
            inline bool operator<(const Task& o) const {
                return priority < o.priority;
            }
            
            
        private:

        };
        
    };

    std::ostream& operator<<(std::ostream & os, const Tasks::ID::Status &status);
    
    class Task : public SafeRef<Obj::Task> {
    public:
        typedef SafeRef<Obj::Task> Parent;
        inline Task() : Parent() {};
        using Parent::Parent;
        
    };
    
    namespace Tasks {
        extern Task Waiting;
    }
};

#endif /* TASK_H */

