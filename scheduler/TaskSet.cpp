#include "TaskSet.h"
#include <chrono>
using namespace std::chrono_literals;

namespace Spinster {
    TaskSet::TaskSet() : mtx(), cv(), lclTaskCounter(0) {
        
    }
    TaskSet::~TaskSet() {
        return;
        for(const auto& cleanupFn : invokeeCleanup) {
            cleanupFn();
        }
    }
    
    void TaskSet::yield() {
        while(!this->complete()) {
            std::unique_lock<std::mutex> lck(mtx);
            cv.wait_for(lck, 10ms); 
        }
    }
}
