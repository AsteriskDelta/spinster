#include "RemoteTask.h"
#include "Message.h"
#include "Group.h"
#include "MemberLink.h"
#include "Protocol.h"
#include "LocalGroup.h"
#include "SymbolTable.h"

namespace Spinster {
    namespace Obj {

        RemoteTask::RemoteTask() : cachedMessage(nullptr) , msgConstructed(false){
        }

        RemoteTask::RemoteTask(const RemoteTask& orig) : Task(orig) {
        }

        RemoteTask::~RemoteTask() {
            if(!this->completed()) this->onReturn(nullptr);
            if(!msgConstructed && this->data) delete &data;//If we were constructed, the message would delete the data buffer
        }
        
        Message* RemoteTask::constructMessage() {
            if(this->cachedMessage != nullptr) return cachedMessage;
            
            //std::cout << "constructing message with dsz=" << data->size() << "\n";
            Message *ret = new Message(&this->data);
            ret->header.to = this->target;
            ret->header.symbol = this->symbolID;
            ret->header.priority = this->priority;
            ret->header.frameSize = $::Local->symbols()->at(this->symbolID)->argSize();
            ret->remoteTask = this;
            msgConstructed = true;
            this->cachedMessage = ret;
            return ret;
        }
        
        bool RemoteTask::sendMessage(SafeRef<MemberLink> link) {
            Message *msg = this->constructMessage();
            SafeRef<Group> group = this->target.resolve();
            if(!group || msg == nullptr) return false;
            
            if(msgConstructed) {
                //Data was already updated by invokee, just send it
                std::cout << "sending remote task message of sz=" << cachedMessage->data->size() << "\n";
                return link->send(cachedMessage);
            }
            
            std::cout << "looking for external symbol " << msg->symbolID() << ", msgSZ=" << msg->size() << "\n";
            if(group->symbols()->knows(msg->symbolID())) {
                std::cout << "using cached resolution, " << msg->symbolID();
                msg->symbolID() = group->symbols()->convert(msg->symbolID());
                if(msg->priority().immortal()) msg->header.frameSize = $::Local->symbols()->at(msg->symbolID())->argSize();
                std::cout << " -> " << msg->symbolID() << "\n";
                return link->send(msg);
            } else {//Runtime resolve
                std::cout << "looking up symbol " << $::Local->symbols()->lookupOf(msg->symbolID()).name << "\n";
                return group->invoke(Protocol::Sym::SymLookup, $::Local->symbols()->lookupOf(msg->symbolID()))
                        ->returned([msg,group, link](SymIDT extID) mutable -> void {
                    group->symbols()->conversion(msg->symbolID(), extID);
                    std::cout << "remote resolve " << msg->symbolID() << " -> " << extID << "\n";
                    msg->symbolID() = extID;
                    
                    link->send(msg);
                });
            }
        }
        
        MemberID& RemoteTask::targetID() {
            return this->target;
        }

    };
};
