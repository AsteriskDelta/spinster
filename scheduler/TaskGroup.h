#ifndef TASKGROUP_H
#define TASKGROUP_H
#include "SpinsterInclude.h"
#include "SafeRef.h"
#include "Task.h"
#include <mutex>
#include <unordered_set>
#include <queue>
#include "PriorityQueue.h"

namespace Spinster {

    namespace Obj {
        
        class Scheduler;

        class TaskGroup : public $::Common {
        public:
            TaskGroup(SafeRef<Scheduler> schd, const std::string& iden);
            ~TaskGroup();
            
            bool matches($::Member member) const;
            void addAffinity($::Member member);
            
            bool schedule(SafeRef<Task> task);
            
            SafeRef<Task> top();
            SafeRef<Task> take();
            void complete(SafeRef<Task> task);
            void restart(SafeRef<Task> task);
            void cancel(SafeRef<Task> task);
            
            void barrier();
            bool checkBarrier();
            
            PriorityQueue<SafeRef<Task>> queue;
            typedef decltype(queue.begin()) iterator;
            
            inline bool barred() const {
                return raw.barred;
            }
            inline iterator begin() {
                return queue.begin();
            }
            inline iterator end() {
                return queue.end();
            }
            inline bool empty() const {
                return this->size() == 0;
            }
            inline unsigned int size() const {
                return raw.size;
            }
            inline std::size_t actives() const {
                return activeTasks.size();
            }
            inline std::size_t count() const {
                return this->size() + this->actives();
            }
            
            inline const std::string& name() const {
                return raw.name;
            }
            
            inline explicit operator bool() const {
                return !this->barred() && !this->empty();
            }
            
            inline const std::unordered_set<SafeRef<Task>>* getActives() const {
                return &activeTasks;
            }
            
            std::string str() const;
        protected:
            SafeRef<Scheduler> scheduler;
            
            std::vector<$::Member> members;
            
            std::recursive_mutex useMutex;
            std::unordered_set<SafeRef<Task>> activeTasks;
            struct {
                std::string name = "";
                bool barred = false;
                unsigned int size;
            } raw;
        };
    };
    
    class TaskGroup : public SafeRef<Obj::TaskGroup> {
    public:
        typedef SafeRef<Obj::TaskGroup> Parent;
        inline TaskGroup() : Parent() {};
        using Parent::Parent;
    };
    SPINSTER_SAFEREF_NULL(TaskGroup);
};

#endif /* TASKGROUP_H */

