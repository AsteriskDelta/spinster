#ifndef THREAD_H
#define THREAD_H
#include "SpinsterInclude.h"
#include "Group.h"
#include "TaskGroup.h"
#include <pthread.h>
#include <mutex>
#include <condition_variable>
#include <queue>

#define SPINSTER_RESERVED_THREAD_IDS 1

namespace Spinster {
    
    namespace Obj {
        class TaskGroup;
        class LocalScheduler;
        
        void OnSignal(int sig);
        void SignalInit();
        class Task;

        class Thread : public Group {
        public:
            friend class LocalScheduler;
            typedef unsigned int Idt;
            struct Options {
                std::size_t stackSize = 0;//System default 
            } options;
            
            Thread();
            virtual ~Thread();

            //virtual const $::MemberID& id() const;
            virtual SafeRef<$::Obj::Task> task();

            virtual SafeRef<$::Obj::LocalGroup> local();
            virtual SafeRef<Obj::Group> parent();

            virtual std::string str() const;

            inline Idt threadID() const {
                return raw.tid;
            };

            inline bool valid() const {
                return raw.valid;
            };

            inline explicit operator bool() const {
                return this->valid();
            };
            
            inline virtual SafeRef<Task> currentTask() const override { return raw.task; };
            virtual bool getTask() override;
            virtual bool getTaskBlock() override;
            virtual void setTask(SafeRef<Task> newTask) override;
            virtual bool active() const override;
            inline virtual SafeRef<TaskGroup> affinity() const override {
                return raw.affinity;
            }
            virtual bool assign(SafeRef<Task> task) override;
            
            virtual void setActive(bool st) override;
        protected:
            void create();
            void destroy(bool soft = false);

            struct {
                pthread_t thread;
                pthread_attr_t attr;
                Idt tid = 0;
                bool valid = false;
                
                void *jmpBuffer = nullptr;
                SafeRef<Task> task = nullptr;
                SafeRef<TaskGroup> affinity = nullptr;
                bool active = true;
            } raw;
            
            std::queue<SafeRef<Task>> taskQueue;
            
            std::mutex activeMtx;
            std::condition_variable activeVar;
        public:
            friend void* ThreadInit(void *thr);
            friend void ThreadCleanup(void *thr);
            friend void ThreadTerminate();
            friend void OnSignal(int sig);
            
            std::mutex guardMtx;
        };

    };
    
    class Thread : public SafeRef<Obj::Thread> {
    public:
        typedef SafeRef<Obj::Thread> Parent;
        using Parent::Parent;
    };
    
    extern thread_local Thread thread;
    SPINSTER_SAFEREF_NULL(Thread);
};

#endif /* THREAD_H */
