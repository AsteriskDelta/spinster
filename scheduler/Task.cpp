#include "Task.h"
#include <sstream>
#include "Member.h"
#include "SymbolTable.h"
#include "Spinster.h"
#include <cstdlib>
#include <unistd.h>

namespace Spinster {
    namespace Obj {

        Task::Task() : symbolID(SymIDT_None) {
        }

        Task::Task(const Task& orig) : $::Common(orig), sender(orig.sender), priority(orig.priority), status(orig.status), worker(orig.worker), symbolID(orig.symbolID) {

        }

        Task::~Task() {
        }

        SafeRef<SymbolEntry> Task::symbol() const {
            if (symbolID == SymIDT_None/* || !worker*/) return nullptr;
            else return LookupSymbol(Local->id(), symbolID);
        }

        Message* Task::constructMessage() {
            return nullptr;
        }
        bool Task::sendMessage(SafeRef<MemberLink> link) {
            _unused(link);
            return false;
        }

        MemberID& Task::targetID() {
            static MemberID fakeID;
            return fakeID;
        }

        std::string Task::str() const {
            using namespace $::Tasks::ID;
            std::stringstream ss;
            switch (status) {
                case SpecialWaiting:
                {
                    ss << "Task::Waiting";
                    break;
                };
                case Special:
                {
                    ss << "Task::Special";
                    break;
                };
                default:
                {
                    ss << "Task(" << priority << ", " << status << " " << this->symbol() << " from " << sender << " on " << worker << ")";
                };
            };
            return ss.str();
        }

        void Task::setWorker($::Member thr) {
            worker = thr;
            if (!thr && !this->completed() && this->valid()) {//Resubmit for later completion

            }
        }

        void Task::setTaskGroup(SafeRef<TaskGroup> grp) {
            taskGroup = grp;
            //DBG_EXCL(std::cout << "\tSETGRP " << this << " set taskgroup to " << &taskGroup << std::endl;);
        }

        bool Task::execute() {
            //DBG_EXCL(std::cout << "\tEXECUTE " << this->str() << std::endl;);
            return true;
        }

        void Task::complete(bool succeeded) {
            if (succeeded) this->setStatus(Tasks::Succeeded);
            else this->setStatus(Tasks::Failed);
        }

        bool Task::cancel(bool recursive) {
            if (recursive) return true;
            //if(!this->active()) return false;
            this->setStatus(Tasks::Cancelled);
            return true;
        }

        void Task::setStatus(const $::Tasks::ID::Status& stat) {
            status = stat;
            //DBG_EXCL(std::cout << "Task at " << this << " has taskGroup=" << &taskGroup << ", set status to " << stat << ", compl=" << this->completed() << "\n";);
            //if(this->completed()) sleep(5);
            
            
            if (this->completed() && taskGroup) {
                if (stat == Tasks::Cancelled) {//Cancel'd task- no retry
                    return this->setStatus(Tasks::Failed);
                }

                taskGroup->complete(this);
                //taskGroup = nullptr;
            } else if(this->completed()) {
                //DBG_EXCL(std::cerr << "Warning: task without taskgroup was completed and reaped...\n";);
                delete this;
            }
        }

        void Task::setPriority(const $::Priority& pr) {
            priority = pr;
        }
    };

    std::ostream& operator<<(std::ostream & os, const Tasks::ID::Status &status) {
        using namespace Tasks::ID;
        switch (status) {
            default:
            case Invalid:
                os << "Invalid";
                break;
            case Queued:
                os << "Queued";
                break;
            case Active:
                os << "Active";
                break;
            case ExclusiveActive:
                os << "Looping";
                break;
            case Failed:
                os << "Failed";
                break;
            case Succeeded:
                os << "Succeeded";
                break;
            case Special:
                os << "Special";
                break;
            case SpecialWaiting:
                os << "Waiting";
                break;
            case Dispatched:
                os << "Dispatched";
                break;
            case Constructed:
                os << "Constructed";
                break;
        }
        return os;
    }

    SPINSTER_SAFEREF_NULL(Task);
    namespace Tasks {
        static $::Obj::Task WaitingObject = $::Obj::Task(ID::SpecialWaiting);
        $::Task Waiting(WaitingObject);
    };
};
