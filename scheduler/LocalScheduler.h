#ifndef LOCALSCHEDULER_H
#define LOCALSCHEDULER_H
#include "SpinsterInclude.h"
#include "SafeRef.h"
#include "Scheduler.h"
#include <mutex> 
#include <condition_variable>

namespace Spinster {
    namespace Obj {
        class Group;
        class Thread;
        
        class LocalScheduler : public Scheduler {
        public:
            LocalScheduler(SafeRef<Group> grp);
            virtual ~LocalScheduler();
            
            virtual bool schedule(SafeRef<Task> task, SafeRef<TaskGroup> taskGroup = nullptr) override;
            virtual void dispatch() override;
            virtual bool waitFor($::Member member) override;
            virtual bool addThreads(int cnt) override;
            virtual void forceNotify() override;
            virtual Thread* createThread();
        private:
            std::mutex dispatchMtx;
            std::condition_variable dispatchVar;
            std::list<Thread*> ownedThreads;
        };
    };
    
    class LocalScheduler : public SafeRef<Obj::LocalScheduler> {
    public:
        typedef SafeRef<Obj::LocalScheduler> Parent;
        inline LocalScheduler() : Parent() {};
        using Parent::Parent;
    };
    
    SPINSTER_SAFEREF_NULL(LocalScheduler);
};

#endif /* LOCALSCHEDULER_H */

