#include "Scheduler.h"
#include "Task.h"
#include "TaskGroup.h"

namespace Spinster {
    namespace Obj {

        void Scheduler::constructBaseTaskGroups() {
            taskGroups.push_back(new TaskGroup(this, "Default"));
            taskGroups.push_back(new TaskGroup(this, "Reserved"));
            taskGroups.push_back(new TaskGroup(this, "Events"));
        }
        
        Scheduler::Scheduler() : group(nullptr) {
            this->constructBaseTaskGroups();
        }
        Scheduler::Scheduler(SafeRef<Group> grp) {
            group = grp;
            this->constructBaseTaskGroups();
        }

        Scheduler::~Scheduler() {
            for(SafeRef<TaskGroup> tg : taskGroups) delete &tg;
        }
        
        bool Scheduler::schedule(SafeRef<Task> task, SafeRef<TaskGroup> taskGroup) {
            if(taskGroups.empty()) return false;
            else if(!taskGroup) {
                if(task->taskGroup) taskGroup = task->taskGroup;
                else taskGroup = taskGroups[0];
            }
            std::lock_guard<std::recursive_mutex> useLck(this->inUseMtx);
            //inUseMtx.lock();
            //queue.push(task);
            //inUseMtx.unlock();
            const unsigned int tgIdx = this->indexOf(taskGroup);
            task->taskGroup = taskGroup;
            //DBG_EXCL(std::cout << "pushed " << task << " to queue on group #" << tgIdx << "<"<<taskGroups.size()<<" from ptr " << taskGroup <<"\n");
            if(tgIdx >= taskGroups.size()) return false;
            
            //std::cout << "\tsubmitted task has group " << task->taskGroup << "\n";
            bool success = taskGroups[tgIdx]->schedule(task);
            if(success && !taskGroups[tgIdx]->barred()) count++;
            return success;
        }
        
        void Scheduler::dispatch() {
            //DBG_EXCL(std::cerr << "Scheduler raw dispatch\n");
            std::lock_guard<std::recursive_mutex> useLck(this->inUseMtx);
            count = 0;
            for(unsigned int i = 0; i < taskGroups.size(); i++) {
                SafeRef<TaskGroup> taskGroup = taskGroups[i];
                if(!(*taskGroup)) continue;
                
                count += taskGroup->size();
            }
        }
        
        bool Scheduler::waitFor($::Member member) {
            
            return true;
        }
        /*
        void Scheduler::addIdle($::Member member) {
            idles.insert(member);
            this->dispatch();
        }
        void Scheduler::removeIdle($::Member member) {
            idles.erase(member);
        }*/
        
        std::unordered_set<$::Member>* Scheduler::getStatusGroup(const MemberStatus& status) {
            decltype(idles) *targetSet;
            //const MemberStatus status = member->status();
            
            switch(status.code) {
                case $::MemberStatus::MainThread:
                case $::MemberStatus::Reserved:
                    targetSet = &reserved;
                    break;
                case $::MemberStatus::Executing:
                    targetSet = &workers;
                    break;
                case $::MemberStatus::Waiting:
                    targetSet = &idles;
                    break;
                case $::MemberStatus::Sleeping:
                    targetSet = &sleepers;
                    break;
                case $::MemberStatus::Yielded:
                    targetSet = &yielders;
                    break;
                case $::MemberStatus::WakingUp:
                    targetSet = &wakers;
                    break;
                default:
                    targetSet = &unqualified;
                    break;
            }
            return targetSet;
        }
        
        void Scheduler::updateMemberStatus($::Member member, const MemberStatus &status, const MemberStatus& oldStatus) {
            if(!member) return;
            
            //iteratingMtx.lock();
            //DBG_EXCL(std::cout << "set thread" << member->id() << " to status " << status << "\n";);
            std::lock_guard<std::recursive_mutex> useLck(this->inUseMtx);
            auto prevGroup = this->getStatusGroup(oldStatus), newGroup = this->getStatusGroup(status);
            prevGroup->erase(member);
            newGroup->insert(member);
            /*for(decltype(idles)* set = &idles; true; set++) {
                if(set == targetSet) set->insert(member);
                else set->erase(member);
                //DBG_EXCL(std::cout << "\tset size="<<set->size() << "\n";);
                if(set == &unqualified) break;
            }*/
            //DBG_EXCL(std::cout << "set thread" << member->id() << " gave set size="<<targetSet->size() << "\n";);
            //iteratingMtx.unlock();
        }
        
        SafeRef<TaskGroup> Scheduler::topTaskGroup() {
            if(this->empty()) return nullptr;
            
            const unsigned int idx = this->firstTaskGroup();
            if(idx >= taskGroups.size()) return nullptr;
            else return taskGroups[idx];
        }
        
        SafeRef<Task> Scheduler::take($::Member member) {
            //std::lock_guard<std::mutex> glck(iteratingMtx);
            std::lock_guard<std::recursive_mutex> useLck(this->inUseMtx);
            /*if(queue.empty()) return nullptr;
            
            if(!member) {//Force a return value if we can
                
            }
            
            SafeRef<Task> ret = queue.top();
            queue.pop();*/
            for(unsigned int i = 0; i < taskGroups.size(); i++) {
                SafeRef<TaskGroup> taskGroup = taskGroups[i];
                if(!(*taskGroup)) continue;
                
                SafeRef<Task> ret = taskGroup->take();
                if(ret) return ret;
            }
            return nullptr;
        }
        
        void Scheduler::forceNotify() {
            return;
        }
        
        bool Scheduler::addThreads(int cnt) {
            _unused(cnt);
            std::cerr << "Scheduler raw addThreads\n";
            return false;
        }
        
        unsigned int Scheduler::indexOf(SafeRef<TaskGroup> taskGroup) {
            for(unsigned int i = 0; i < taskGroups.size(); i++) {
                if(taskGroups[i] == taskGroup) return i;
            }
            return taskGroups.size();
        }
        unsigned int Scheduler::firstTaskGroup() {
            for(unsigned int i = 0; i < taskGroups.size(); i++) {
                if(*taskGroups[i]) return i;
            }
            return taskGroups.size();
        }
        
        SafeRef<TaskGroup> Scheduler::defaultTaskGroup() {
            if(taskGroups.size() < 1) return nullptr;
            else return taskGroups[0];
        }
        SafeRef<TaskGroup> Scheduler::reservedTaskGroup() {
            if(taskGroups.size() < 2) return nullptr;
            else return taskGroups[1];
        }
        SafeRef<TaskGroup> Scheduler::eventTaskGroup() {
            if(taskGroups.size() < 3) return nullptr;
            else return taskGroups[2];
        }
            
    };

};
