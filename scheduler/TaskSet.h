#ifndef TASKSET_H
#define TASKSET_H
#include "SpinsterInclude.h"
#include "SafeRef.h"
#include "Task.h"
#include <mutex>
#include <condition_variable>
#include <functional>
#include <vector>
#include "Invokee.h"

namespace Spinster {
    class TaskSet : public std::vector<Task> {
    public:
        TaskSet();
        ~TaskSet();
        
        std::vector<std::function<void(void)>> invokeeCleanup;
        
        /*inline void defer(bool state) {
            deferInvoke = state;
        }*/
        
        inline bool complete() const {
            return this->empty();
        }
        
        void yield();
        
        template<typename FN, typename ...Args>
        inline TaskSet& operator+=(Obj::Invokee<FN, Args...>* &&inv) {
            std::lock_guard<std::mutex> lck(this->mtx);
            const size_t lclTaskID = lclTaskCounter++;
            //inv->persist();//We'll delete this manually
            inv->callback([this, lclTaskID](auto val) {
                _unused(val);
                //std::unique_lock<std::mutex> lck(this->mtx);
                std::lock_guard<std::mutex> cblck(this->mtx);
                bool found = false;
                for(size_t i = 0; i < this->size(); i++) {
                    if(this->lclTaskIDs[i] == lclTaskID) {
                        this->erase(this->begin() + i);
                        this->lclTaskIDs.erase(this->lclTaskIDs.begin() + i);
                        found = true;
                    }
                }
                
                if(!found) {
                    DBG_EXCL(std::cerr << "Warning: TaskSet completion callback recieved for missing lclTaskID #" + std::to_string(lclTaskID) + "\n");
                } else {
                    //DBG_EXCL(std::cout << "found task lclid " << lclTaskID << ", " << this->size() << " left\n");
                }
                
                this->cv.notify_all(); 
            });
            
            Task tmp = inv->submit(true);
            inv = nullptr;
            //inv->disable();//Invoke only once
            
            //invokeeCleanup.push_back([inv]() {
                //delete &inv;
            //});
            lclTaskIDs.push_back(lclTaskID);
            this->push_back(tmp);
            
            return *this;
        }
        
        template<typename V = void, typename FN, typename ...Args>
        inline TaskSet& operator+=(Invokee<FN, Args...> &&inv) {
            return this->operator+=(&inv);
        }
        
        //void taskCompleted(Task task);
    protected:
        //bool deferInvoke;
        std::mutex mtx;
        std::condition_variable cv;
        size_t lclTaskCounter;
        
        std::vector<size_t> lclTaskIDs;
    };
};

#endif
