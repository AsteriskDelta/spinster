#include "Thread.h"
#include <csetjmp>
#include <atomic>
#include <csignal>
#include <cstring>
#include <sstream>
#include "Task.h"
#include "Spinster.h"
#include <exception>
#include <unistd.h>
#include <mutex>

namespace Spinster {

    std::recursive_mutex dbgMtx;
    thread_local Thread thread = nullptr;
    extern std::mutex SpinningMutex;

    namespace Obj {
        static thread_local Thread* _thr = nullptr;
        static thread_local int lastSignal = 0, signalDup = 0;
        //static thread_local bool signalExceptThrown = false;
        static thread_local bool deferSignalException = false;
        std::atomic<Thread::Idt> nextThreadID;
        
        
        static bool triedExit = false, triedAbort = false;
        
        static thread_local std::jmp_buf exceptJmp, exitJmp;
        void clearSigExcept();
        void handleSigExcept();
        void deferSigExcept();
        void throwSigExcept(int sig);
        
         [[noreturn]] void ThreadTerminate();
        
        void clearSigExcept() {
            deferSignalException = false;
            if(lastSignal != 0) handleSigExcept();
            lastSignal = signalDup = 0;
        }
        void deferSigExcept() {
            if(deferSignalException) {
                //clearSigExcept();
                //std::cerr << "Multiple exception signals, superseding with #" << lastSignal << "\n";
                signalDup++;
                if(lastSignal == SIGTERM || (lastSignal != 0 && signalDup > 4)) ThreadTerminate();
            }
            deferSignalException = true;
            //if(lastSignal != 0) std::cout << "\tdeferring sig#" << lastSignal << "\n";
        }
        void handleSigExcept() {
            //if(deferSignalException) return;
            if(triedAbort) return;
            
            int jmpSig = 0;
            if(lastSignal != 0) {
                //std::cout << "\tHANDLE SIG#" << lastSignal << "\n";
                if((jmpSig = setjmp(exceptJmp)) != 0) {//We were in noexcept, defer the signal
                    deferSigExcept();
                } else if(!deferSignalException) {
                    //std::cout << "throwing std exception\n";
                    throw;// std::exception();
                }
            }
        }
        void throwSigExcept(int sig) {
            if(sig == 0 || triedAbort) return;
            
            signalDup++;
            if(lastSignal == SIGTERM || (lastSignal != 0 && signalDup > 4)) ThreadTerminate();
            lastSignal = sig;
            //std::cout << "THROW SIG#" << sig << "\n";
            handleSigExcept();
        }

        void SignalInit() {
            nextThreadID = SPINSTER_RESERVED_THREAD_IDS;
            static int allHandled[] = {/*SIGSEGV, SIGILL, */SIGABRT, SIGFPE, SIGINT, SIGHUP, SIGQUIT, SIGTERM};
            constexpr unsigned int handleCnt = sizeof (allHandled) / sizeof (int);
            static struct sigaction allSAs[handleCnt];
            memset(&allSAs, 0x0, sizeof (allSAs));

            for (unsigned int i = 0; i < handleCnt; i++) {
                struct sigaction *sa = &allSAs[i];
                const int& sigID = allHandled[i];
                sa->sa_handler = OnSignal;
                sa->sa_flags = SA_RESTART;
                sigaction(sigID, sa, nullptr);
            }
        }

        void OnSignal(int sig) {
            lastSignal = sig;
            
            {
                sigset_t ss;

                /* Make sure to unblock SIGFPE, according to POSIX it
                 * gets blocked when calling its signal handler.
                 * sigsetjmp()/siglongjmp would make this unnecessary.
                 */
                sigemptyset(&ss);
                sigaddset(&ss, sig);
                //sigprocmask(SIG_UNBLOCK, &ss, NULL);
                pthread_sigmask(SIG_UNBLOCK, &ss, NULL);
            }
            throwSigExcept(sig);
            
            /*if (_thr == nullptr) {//Main thread, begin abort sequence
                try {
                    if (lastSignal != SIGINT) {
                        std::cerr << "Main thread recieved sig #" << lastSignal << "\n";
                        exit(lastSignal);
                    } else exit(0);
                } catch(...) {//Prevent exception leaks
                    
                }
                return;
            }

            if(lastSignal == SIGINT) {
                pthread_kill(0, SIGINT);
                return;
            } else if (signalExceptThrown) {//Failed to unwind the stack, continue as best we can


                longjmp(*reinterpret_cast<std::jmp_buf*&> (_thr->raw.jmpBuffer), sig);
            } else {
                signalExceptThrown = true;
                std::cout << "throwing SIG , excepts=" << std::uncaught_exceptions() << "\n";
                /2*if(lastSignal == SIGHUP) {
                    goto 
                } else {*2/
                if(!std::uncaught_exception()) {//Don't cause a terminate, we want a clean exit
                    throw std::exception();//("SIG#" + std::to_string(sig));
                }
            }*/
        }

        [[noreturn]] void ThreadTerminate() {
            //if(triedAbort) return;//Prevent eternal abort() -> terminate() calls
            
            if(_thr == nullptr) {
                if(!triedExit) {
                    triedExit = true;
                    exit(0);
                } else {
                    std::cerr << "Terminate called twice in main thread, bailing out...\n";
                    triedAbort = true;
                    abort();
                }
            } else {
                if(signalDup > 2 || lastSignal == SIGTERM) longjmp(exitJmp, lastSignal);;//pthread_exit(nullptr);
                //return; 
                //std::cout << "terminate called, but I didn't feel like dying today...\n";
                longjmp(exceptJmp, lastSignal);
            }
        }


        void* ThreadInit(void *rthr) {
            //DBG_EXCL(std::cout << "\nthreadinit on " << rthr << "\n");
            {
                sigset_t set;
                sigemptyset(&set);
                sigaddset(&set, SIGINT);
                pthread_sigmask(SIG_BLOCK, &set, NULL);
            }
            std::set_terminate(&ThreadTerminate);

            Thread *thread = reinterpret_cast<Thread*> (rthr);
            thread->setActive(true);
            if (thread == nullptr) return NULL;
            _thr = thread;
            $::thread = thread;
            $::member = thread;
            
            usleep(1000);//Prevent data race on thr->valid()

            std::jmp_buf *& jmpbuff = reinterpret_cast<std::jmp_buf*&> (_thr->raw.jmpBuffer);
            std::jmp_buf *const norefJmpbuff = jmpbuff;
            jmpbuff = reinterpret_cast<std::jmp_buf*> (malloc(sizeof (std::jmp_buf)));
            bool useThr = true;
            int jmpSig = 0;
            
            if((jmpSig = setjmp(exitJmp)) != 0) goto threadExit; 
            
            threadException:
            try {
                _thr->raw.tid = nextThreadID++;
                _thr->$::Obj::Member::raw.id = MemberID(MemberID::LocalGroup, _thr->raw.tid); //nextThreadID++;
                //_thr->raw.valid = true;

                /*if ((jmpSig = setjmp(*jmpbuff)) != 0 || lastSignal != 0) {//Unhandled fault in last task, invalidate it
                    if (lastSignal == SIGHUP) useThr = false;
                    else {
                        if (_thr->currentTask()) _thr->currentTask()->setStatus($::Tasks::Failed);
                        DBG_EXCL(std::cout << "\n\nFAULT WITHIN TASK on thread #" << _thr->threadID() << ", signal=" << lastSignal << ": " << _thr->currentTask() << "\n\n");
                        _thr->setTask(nullptr);
                        //_thr->setStatus($::MemberStatus::Waiting);
                    }
                    signalExceptThrown = false;
                }*/
                //lastSignal = 0;
                //_thr->setTask($::Tasks::Waiting);

                if(useThr) _thr->setStatus($::MemberStatus::Waiting);

                //DBG_EXCL(std::cout << "\nbegin task loop...\n");
                while (useThr && _thr->valid()) {
                    if(::Spin::SpinningMutex.try_lock()) {
                        ::Spin::SpinningMutex.unlock();
                        useThr = false;
                        break;
                    }
                    if(!_thr->valid()) {
                        useThr = false;
                        break;
                    }
                    
                    if (!_thr->raw.active) {
                        _thr->setStatus($::MemberStatus::Sleeping);
                        //_thr->setTask(nullptr);
                        std::unique_lock<std::mutex> lck(_thr->activeMtx);
                        //DBG_EXCL(std::cout << "waiting on alarm clock...\n");
                        _thr->activeVar.wait(lck);
                        //DBG_EXCL(std::cout << "GOT WAKEUP CALL\n");
                        _thr->raw.active = true;
                    }

                    //try {
                    deferSigExcept();
                    bool skip = (!_thr->getTaskBlock() || !_thr->currentTask());
                    clearSigExcept();
                    
                    if(skip) continue;
                    /*    if()
                    } catch (...) {
                        if (lastSignal == SIGHUP) break;
                    }*/

                    //if(_thr->currentTask()->exclusive()) _thr->setStatus($::MemberStatus::SpecialExecuting);
                    //else _thr->setStatus($::MemberStatus::Executing);

                    //try {
                        _thr->currentTask()->setStatus($::Tasks::Active);
                        _thr->currentTask()->execute();
                        //Status (of success) is set by task's execute function
                        //if (_thr->currentTask()) _thr->currentTask()->setStatus($::Tasks::Succeeded);
                        _thr->raw.task = nullptr;//setTask(nullptr);
                    _thr->setStatus($::MemberStatus::Waiting);
                    //} catch (...) {
                        //std::cout << "exec caught " << lastSignal << "\n";
                        //signalExceptThrown = false;
                        //if (lastSignal == SIGHUP) break;
                        //if (_thr->currentTask()) _thr->currentTask()->setStatus($::Tasks::Failed);
                        //DBG_EXCL(std::cout << "\n\nFAULT WITHIN TASK on thread #" << _thr->threadID() << ", signal=" << lastSignal << ": " << _thr->currentTask() << "\n\n");
                        //_thr->setTask(nullptr);
                        
                        // _thr->setStatus($::MemberStatus::Waiting);
                    //}
                    //_thr->setStatus($::MemberStatus::Waiting);
                }
            } catch (...) {
                _thr->raw.task = nullptr;
                
                //_thr->setTask(nullptr);
                DBG_EXCL(std::cout << "\n\nFAULT WITHIN TASK on thread #" << _thr->threadID() << ", signal=" << lastSignal << ": " << _thr->currentTask() << "\n\n");
                if(!useThr) goto threadExit;
                
                //std::cout << "CAUGHT SIG #" << lastSignal << "\n";
                if(lastSignal == SIGHUP) useThr = false;
                else if(lastSignal == SIGSEGV) {
                    if (_thr->currentTask()) {
                        _thr->currentTask()->setStatus($::Tasks::Failed);
                    }
                    std::cerr << "Thread recieved SIGSEGV, terminating it.\n";
                    goto threadExit;
                } else if (_thr) {
                    if (_thr->currentTask()) {
                        _thr->currentTask()->setStatus($::Tasks::Failed);
                    }
                    
                    _thr->setStatus($::MemberStatus::Waiting);
                    goto threadException;
                }
            };
            
            //cleanupTask:
            pthread_attr_destroy(&thread->raw.attr);
            
            threadExit:
            //std::cout << "cleanup OK\n";
            free(norefJmpbuff);
            //jmpbuff = nullptr;
            

            return NULL;
        }

        void ThreadCleanup(void *rthr) {
            Thread *thread = reinterpret_cast<Thread*> (rthr);

            if (thread == nullptr) return;
            if (thread->currentTask() && thread->currentTask()->valid()) {//Incomplete, but not invalid- re-queue 
                thread->setTask(nullptr);
            }

            if (thread->raw.jmpBuffer != nullptr) free(thread->raw.jmpBuffer);

        }

        void Thread::create() {
            pthread_attr_init(&raw.attr);
            if (options.stackSize != 0) pthread_attr_setstacksize(&raw.attr, options.stackSize);

            //activeMtx.lock();
            this->setTask(nullptr);

            int status = pthread_create(&raw.thread, NULL, ThreadInit, reinterpret_cast<void*> (this));
            if (status != 0) {
                std::cerr << "thread creation failed...\n";
            } else raw.valid = true;
        }

        void Thread::destroy(bool soft) {
           // if (!this->valid() || _thr != nullptr) return;//Don't try to destroy from child thread

            if(soft) {
                try {
                    pthread_kill(raw.thread, SIGHUP);
                } catch (...) {
                };
                return;
            }
            //std::cout << "SIGTERM sent to process\n";
            pthread_kill(raw.thread, SIGTERM);
            if(!this->active()) this->setActive(true);
            
            if(!raw.active) activeVar.notify_all();
            //if(soft) return;//Just send the signal, don't wait/force term
            /*raw.valid = false;
            pthread_attr_destroy(&raw.attr);
            //pthread_cancel(raw.thread);*/
            //std::cout << "Joining thread...\n";
            //local()->scheduler->forceNotify();//If we're waiting on task assignment, force an exit to break out of noexcept() mutex
            /*for(int i = 0; i < 10; i++) {
                if(pthread_kill(raw.thread, 0) == ESRCH) break;
                else if(i == 9) pthread_kill(raw.thread, SIGTERM);
                usleep(1000*10);
            }*/
            //std::cout << "Thread terminated\n";
            pthread_join(raw.thread, nullptr);
            try {
                if (this->raw.jmpBuffer != nullptr) free(this->raw.jmpBuffer);
                //activeMtx.unlock();
            } catch (...) {
            };
            //pthread_detach(raw.thread);
        }

        Thread::Thread() : options(), raw() {
            this->setStatus($::MemberStatus::Connecting);
            this->create();
        }

        Thread::~Thread() {
            this->destroy();
            //if (this->currentTask()) delete &this->currentTask();
        }

                /*
                $::MemberID Thread::id() const {
                    return MemberID(MemberID::LocalGroup, this->threadID());
                }*/

        SafeRef<Obj::Task> Thread::task() {
            if (!this->busy()) return nullptr;
            return this->currentTask();
        }

        SafeRef<Obj::LocalGroup> Thread::local() {
            return *static_cast<Obj::LocalGroup*> (&$::Local);
        }

        SafeRef<Obj::Group> Thread::parent() {
            return *static_cast<Obj::Group*> (&$::Local);
        }

        bool Thread::active() const {
            return this->valid() && raw.active && this->status().code != $::MemberStatus::Yielded;
        }

        bool Thread::assign(SafeRef<Task> task) {
            //DBG_EXCL(std::cout << "thread #" << threadID() << "::assign " << task << "\n");
            if (true || this->currentTask()/* && (this->currentTask() != $::Tasks::Waiting || this->status().code != $::MemberStatus::Waiting)*/) {
                //DBG_EXCL(std::cerr << "assign called, but already on task " << this->currentTask());
                std::lock_guard<std::mutex> lck(this->guardMtx);
                taskQueue.push(task);
                return false;
            }

            this->setTask(task);
            return true;
        }

        bool Thread::getTask() {
            return false;
        }

        bool Thread::getTaskBlock() {
            raw.task = nullptr;
            //In case our object was deleted before the worker thread could be notified
            if( lastSignal == SIGHUP || lastSignal == SIGSEGV) {
                throwSigExcept(lastSignal);
                return false;
            }
            
            //sleep(1);
            //activeMtx.lock();//Wait for next task to be assigned
            //activeMtx.unlock();
            //this->setTask(nullptr);
            
            if(!this->taskQueue.empty()) {
                this->setTask(taskQueue.front());
                std::lock_guard<std::mutex> lck(this->guardMtx);
                taskQueue.pop();
                return true;
            } else if (this->local()->scheduler->waitFor(this)) {
                auto newTask = local()->scheduler->take(this);
                if(newTask == nullptr || newTask->completed()) return false;
                
                this->setTask(newTask);
                return true;
            } else {
                //this->setTask(nullptr);
                return false;
            }
        }

        void Thread::setTask(SafeRef<Task> newTask) {
            //DBG_EXCL(std::cout << "\t" << threadID() << " set task to " << newTask << "\n");
            /*if (this->currentTask()) {
                currentTask()->setWorker(nullptr);
                delete &raw.task;
                raw.task = nullptr;
                //this->setStatus($::MemberStatus::Waiting);
            }*/

            raw.task = newTask;
            if (newTask && newTask != $::Tasks::Waiting) {
                newTask->setWorker(this);
                if (newTask->priority == Priority::Loop()) this->setStatus($::MemberStatus::Reserved);
                else this->setStatus($::MemberStatus::Executing);
                //this->local()->scheduler->removeIdle(this);
            } else {
                this->setStatus($::MemberStatus::Waiting);
                //this->local()->scheduler->addIdle(this);
                //this->local()->scheduler->dispatch();
            }
        }

        void Thread::setActive(bool st) {
            raw.active = st;
            if (st) {
                //DBG_EXCL(std::cout << "notifying for wake...\n");
                activeVar.notify_all();
            } else if (!st) {
                //auto prevStatus = this->status();

                //this->setStatus($::MemberStatus::Sleeping);
                //std::unique_lock<std::mutex> lck(activeMtx);
                //activeVar.wait(lck);

                //this->setStatus(prevStatus);
            }
        }

        std::string Thread::str() const {
            std::stringstream ss;
            ss << "Thread(#" << this->threadID() << ": " << this->status() << ")";
            return ss.str();
        }
    };
};
