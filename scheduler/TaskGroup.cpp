#include "TaskGroup.h"
#include "Scheduler.h"
#include <sstream>

namespace Spinster {
    namespace Obj {

        TaskGroup::TaskGroup(SafeRef<Scheduler> schd, const std::string& iden) : scheduler(schd), raw() {
            raw.name = iden;
        }

        TaskGroup::~TaskGroup() {
            for(auto it = this->begin(); it != this->end(); ++it) {
                delete &(*it);
            }
        }

        bool TaskGroup::schedule(SafeRef<Task> task) {
            if(!task) return false;
            std::lock_guard<std::recursive_mutex> lck(useMutex);
            
            task->setStatus(Tasks::Queued);
            task->setTaskGroup(this);
            queue.push(task);
            raw.size++;
            return true;
        }
        
        SafeRef<Task> TaskGroup::top() {
            if(this->empty()) return nullptr;
            return queue.top();
        }

        SafeRef<Task> TaskGroup::take() { 
            std::lock_guard<std::recursive_mutex> lck(useMutex);
            if(this->empty()) return nullptr;
            
            SafeRef<Task> ret = this->top();
            queue.pop();
            raw.size--;
            ret->setStatus(Tasks::Dispatched);
            activeTasks.insert(ret);
            return ret;
        }
        void TaskGroup::complete(SafeRef<Task> task) {
            std::lock_guard<std::recursive_mutex> lck(useMutex);
            //Placeholder
            //task->setTaskGroup(nullptr);
            activeTasks.erase(task);
            delete &task;
        }
        void TaskGroup::restart(SafeRef<Task> task) {
            std::lock_guard<std::recursive_mutex> lck(useMutex);
            if(activeTasks.find(task) == activeTasks.end()) {
                std::cerr << "Cancel called on group " << this->name() << ", but task doesn't exist! (" << task << ")\n";
                return;
            }
            
            activeTasks.erase(task);
            task->setWorker(nullptr);
            //task->setStatus(Tasks::Queued);
            //queue.push(task);
            this->schedule(task);
        }
        void TaskGroup::cancel(SafeRef<Task> task) {
            std::lock_guard<std::recursive_mutex> lck(useMutex);
            //Placeholder
            task->cancel();
        }

        void TaskGroup::barrier() {
            std::lock_guard<std::recursive_mutex> lck(useMutex);
            raw.barred = true;
        }
        bool TaskGroup::checkBarrier() {
            std::lock_guard<std::recursive_mutex> lck(useMutex);
            if(this->barred() && activeTasks.empty()) raw.barred = false;
            return raw.barred;
        }
        
        std::string TaskGroup::str() const {
            std::stringstream ss;
            ss << "TaskGroup(" << this->name() << ",count=" << this->count() << ", active="<<this->activeTasks.size() << ")";
            return ss.str();
        }
    }
}


